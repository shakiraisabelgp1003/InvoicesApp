goog.provide('com.app.model');
com.app.model.all_attributes = cljs.core.vec(cljs.core.concat.cljs$core$IFn$_invoke$arity$variadic(com.app.model.account.attributes,com.app.model.item.attributes,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([com.app.model.line_item.attributes,com.app.model.invoice.attributes,com.app.model.file.attributes], 0)));
com.app.model.all_attribute_validator = com.fulcrologic.rad.attributes.make_attribute_validator.cljs$core$IFn$_invoke$arity$1(com.app.model.all_attributes);

//# sourceMappingURL=com.app.model.js.map
