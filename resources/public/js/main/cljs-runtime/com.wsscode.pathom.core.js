goog.provide('com.wsscode.pathom.core');
goog.scope(function(){
  com.wsscode.pathom.core.goog$module$goog$object = goog.module.get('goog.object');
});
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","env","com.wsscode.pathom.core/env",602275378),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),cljs.core.map_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","attribute","com.wsscode.pathom.core/attribute",1954645846),new cljs.core.Keyword("edn-query-language.core","property","edn-query-language.core/property",-1479624874),new cljs.core.Keyword("edn-query-language.core","property","edn-query-language.core/property",-1479624874));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","reader-map","com.wsscode.pathom.core/reader-map",456673075),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","map-of","cljs.spec.alpha/map-of",153715093,null),new cljs.core.Symbol("cljs.core","keyword?","cljs.core/keyword?",713156450,null),new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410)),cljs.spec.alpha.every_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","tuple","cljs.spec.alpha/tuple",-415901908,null),new cljs.core.Symbol(null,"keyword?","keyword?",1917797069,null),new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410)),cljs.spec.alpha.tuple_impl.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","keyword?","cljs.core/keyword?",713156450,null),new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.keyword_QMARK_,new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410)], null)),new cljs.core.PersistentArrayMap(null, 7, [new cljs.core.Keyword("cljs.spec.alpha","kfn","cljs.spec.alpha/kfn",672643897),(function (i__20422__auto__,v__20423__auto__){
return cljs.core.nth.cljs$core$IFn$_invoke$arity$2(v__20423__auto__,(0));
}),new cljs.core.Keyword(null,"into","into",-150836029),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword("cljs.spec.alpha","conform-all","cljs.spec.alpha/conform-all",45201917),true,new cljs.core.Keyword(null,"kind","kind",-717265803),cljs.core.map_QMARK_,new cljs.core.Keyword("cljs.spec.alpha","kind-form","cljs.spec.alpha/kind-form",-1047104697),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Keyword("cljs.spec.alpha","describe","cljs.spec.alpha/describe",1883026911),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","map-of","cljs.spec.alpha/map-of",153715093,null),new cljs.core.Symbol("cljs.core","keyword?","cljs.core/keyword?",713156450,null),new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410)),new cljs.core.Keyword("cljs.spec.alpha","cpred","cljs.spec.alpha/cpred",-693471218),(function (G__61959){
return cljs.core.map_QMARK_(G__61959);
})], null),null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","reader-seq","com.wsscode.pathom.core/reader-seq",6913016),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","coll-of","cljs.spec.alpha/coll-of",1019430407,null),new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410),new cljs.core.Keyword(null,"kind","kind",-717265803),new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null),new cljs.core.Keyword(null,"into","into",-150836029),cljs.core.PersistentVector.EMPTY),cljs.spec.alpha.every_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410),new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410),new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword("cljs.spec.alpha","conform-all","cljs.spec.alpha/conform-all",45201917),true,new cljs.core.Keyword(null,"kind","kind",-717265803),cljs.core.vector_QMARK_,new cljs.core.Keyword(null,"into","into",-150836029),cljs.core.PersistentVector.EMPTY,new cljs.core.Keyword("cljs.spec.alpha","kind-form","cljs.spec.alpha/kind-form",-1047104697),new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null),new cljs.core.Keyword("cljs.spec.alpha","describe","cljs.spec.alpha/describe",1883026911),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","coll-of","cljs.spec.alpha/coll-of",1019430407,null),new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410),new cljs.core.Keyword(null,"kind","kind",-717265803),new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null),new cljs.core.Keyword(null,"into","into",-150836029),cljs.core.PersistentVector.EMPTY),new cljs.core.Keyword("cljs.spec.alpha","cpred","cljs.spec.alpha/cpred",-693471218),(function (G__61960){
return cljs.core.vector_QMARK_(G__61960);
})], null),null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","reader-fn","com.wsscode.pathom.core/reader-fn",1718614315),new cljs.core.Symbol("cljs.core","fn?","cljs.core/fn?",71876239,null),cljs.core.fn_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","optional?","com.wsscode.pathom.core/optional?",910087942),new cljs.core.Symbol("cljs.core","boolean?","cljs.core/boolean?",1400713761,null),cljs.core.boolean_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","or","cljs.spec.alpha/or",-831679639,null),new cljs.core.Keyword(null,"fn","fn",-1175266204),new cljs.core.Keyword("com.wsscode.pathom.core","reader-fn","com.wsscode.pathom.core/reader-fn",1718614315),new cljs.core.Keyword(null,"map","map",1371690461),new cljs.core.Keyword("com.wsscode.pathom.core","reader-map","com.wsscode.pathom.core/reader-map",456673075),new cljs.core.Keyword(null,"list","list",765357683),new cljs.core.Keyword("com.wsscode.pathom.core","reader-seq","com.wsscode.pathom.core/reader-seq",6913016)),cljs.spec.alpha.or_spec_impl(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"fn","fn",-1175266204),new cljs.core.Keyword(null,"map","map",1371690461),new cljs.core.Keyword(null,"list","list",765357683)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","reader-fn","com.wsscode.pathom.core/reader-fn",1718614315),new cljs.core.Keyword("com.wsscode.pathom.core","reader-map","com.wsscode.pathom.core/reader-map",456673075),new cljs.core.Keyword("com.wsscode.pathom.core","reader-seq","com.wsscode.pathom.core/reader-seq",6913016)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","reader-fn","com.wsscode.pathom.core/reader-fn",1718614315),new cljs.core.Keyword("com.wsscode.pathom.core","reader-map","com.wsscode.pathom.core/reader-map",456673075),new cljs.core.Keyword("com.wsscode.pathom.core","reader-seq","com.wsscode.pathom.core/reader-seq",6913016)], null),null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","process-reader","com.wsscode.pathom.core/process-reader",348867871),new cljs.core.Symbol("cljs.core","fn?","cljs.core/fn?",71876239,null),cljs.core.fn_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","process-error","com.wsscode.pathom.core/process-error",-2116719411),new cljs.core.Symbol("cljs.core","fn?","cljs.core/fn?",71876239,null),cljs.core.fn_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","error","com.wsscode.pathom.core/error",1967516491),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","spec","cljs.spec.alpha/spec",-707298191,null),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),new cljs.core.Keyword(null,"gen","gen",142575302),cljs.core.list(new cljs.core.Symbol(null,"fn*","fn*",-752876845,null),cljs.core.PersistentVector.EMPTY,cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","gen","cljs.spec.alpha/gen",147877780,null),new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 1, [cljs.core.list(new cljs.core.Symbol("cljs.core","ex-info","cljs.core/ex-info",-409744395,null),"Generated sample error",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"some","some",-1951079573),"data"], null)),"null"], null), null)))),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),cljs.core.any_QMARK_,(function (){
return cljs.spec.alpha.gen.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentHashSet.createAsIfByAssoc([cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("Generated sample error",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"some","some",-1951079573),"data"], null))]));
}),null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","errors","com.wsscode.pathom.core/errors",-1179549217),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","map-of","cljs.spec.alpha/map-of",153715093,null),new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)),cljs.spec.alpha.every_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","tuple","cljs.spec.alpha/tuple",-415901908,null),new cljs.core.Symbol(null,"vector?","vector?",-61367869,null),new cljs.core.Symbol(null,"any?","any?",-318999933,null)),cljs.spec.alpha.tuple_impl.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.vector_QMARK_,cljs.core.any_QMARK_], null)),new cljs.core.PersistentArrayMap(null, 7, [new cljs.core.Keyword("cljs.spec.alpha","kfn","cljs.spec.alpha/kfn",672643897),(function (i__20422__auto__,v__20423__auto__){
return cljs.core.nth.cljs$core$IFn$_invoke$arity$2(v__20423__auto__,(0));
}),new cljs.core.Keyword(null,"into","into",-150836029),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword("cljs.spec.alpha","conform-all","cljs.spec.alpha/conform-all",45201917),true,new cljs.core.Keyword(null,"kind","kind",-717265803),cljs.core.map_QMARK_,new cljs.core.Keyword("cljs.spec.alpha","kind-form","cljs.spec.alpha/kind-form",-1047104697),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Keyword("cljs.spec.alpha","describe","cljs.spec.alpha/describe",1883026911),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","map-of","cljs.spec.alpha/map-of",153715093,null),new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)),new cljs.core.Keyword("cljs.spec.alpha","cpred","cljs.spec.alpha/cpred",-693471218),(function (G__61961){
return cljs.core.map_QMARK_(G__61961);
})], null),null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","errors*","com.wsscode.pathom.core/errors*",337011276),cljs.core.list(new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"%","%",-950237169,null)], null),cljs.core.list(new cljs.core.Symbol("com.wsscode.pathom.core","atom?","com.wsscode.pathom.core/atom?",332525267,null),new cljs.core.Symbol(null,"%","%",-950237169,null))),(function (p1__61962_SHARP_){
return (com.wsscode.pathom.core.atom_QMARK_.cljs$core$IFn$_invoke$arity$1 ? com.wsscode.pathom.core.atom_QMARK_.cljs$core$IFn$_invoke$arity$1(p1__61962_SHARP_) : com.wsscode.pathom.core.atom_QMARK_.call(null,p1__61962_SHARP_));
}));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","entity","com.wsscode.pathom.core/entity",884706031),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),cljs.core.any_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","entity-key","com.wsscode.pathom.core/entity-key",1494103249),new cljs.core.Symbol("cljs.core","keyword?","cljs.core/keyword?",713156450,null),cljs.core.keyword_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","fail-fast?","com.wsscode.pathom.core/fail-fast?",-272943465),new cljs.core.Symbol("cljs.core","boolean?","cljs.core/boolean?",1400713761,null),cljs.core.boolean_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","map-key-transform","com.wsscode.pathom.core/map-key-transform",-238565800),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","fspec","cljs.spec.alpha/fspec",-1289128341,null),new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Symbol("cljs.core","string?","cljs.core/string?",-2072921719,null)),cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"key","key",-1516042587)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.any_QMARK_], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Symbol("cljs.core","string?","cljs.core/string?",-2072921719,null),cljs.core.string_QMARK_,null,null),new cljs.core.Symbol("cljs.core","string?","cljs.core/string?",-2072921719,null),null,null,null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","map-value-transform","com.wsscode.pathom.core/map-value-transform",1252006952),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","fspec","cljs.spec.alpha/fspec",-1289128341,null),new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),new cljs.core.Keyword(null,"value","value",305978217),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)),cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),new cljs.core.Keyword(null,"value","value",305978217),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Keyword(null,"value","value",305978217)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.any_QMARK_,cljs.core.any_QMARK_], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),new cljs.core.Keyword(null,"value","value",305978217),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),cljs.core.any_QMARK_,null,null),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),null,null,null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","placeholder-prefixes","com.wsscode.pathom.core/placeholder-prefixes",-1362240644),new cljs.core.Symbol("cljs.core","set?","cljs.core/set?",-1176684971,null),cljs.core.set_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","js-key-transform","com.wsscode.pathom.core/js-key-transform",-1588372758),new cljs.core.Keyword("com.wsscode.pathom.core","map-key-transform","com.wsscode.pathom.core/map-key-transform",-238565800),new cljs.core.Keyword("com.wsscode.pathom.core","map-key-transform","com.wsscode.pathom.core/map-key-transform",-238565800));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","js-value-transform","com.wsscode.pathom.core/js-value-transform",1418749137),new cljs.core.Keyword("com.wsscode.pathom.core","map-value-transform","com.wsscode.pathom.core/map-value-transform",1252006952),new cljs.core.Keyword("com.wsscode.pathom.core","map-value-transform","com.wsscode.pathom.core/map-value-transform",1252006952));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","parser","com.wsscode.pathom.core/parser",-740326395),new cljs.core.Symbol("cljs.core","fn?","cljs.core/fn?",71876239,null),cljs.core.fn_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","wrap-read","com.wsscode.pathom.core/wrap-read",-904142261),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","fspec","cljs.spec.alpha/fspec",-1289128341,null),new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"reader","reader",169660853),new cljs.core.Keyword("com.wsscode.pathom.core","reader-fn","com.wsscode.pathom.core/reader-fn",1718614315)),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword("com.wsscode.pathom.core","reader-fn","com.wsscode.pathom.core/reader-fn",1718614315)),cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"reader","reader",169660853),new cljs.core.Keyword("com.wsscode.pathom.core","reader-fn","com.wsscode.pathom.core/reader-fn",1718614315)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reader","reader",169660853)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","reader-fn","com.wsscode.pathom.core/reader-fn",1718614315)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","reader-fn","com.wsscode.pathom.core/reader-fn",1718614315)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"reader","reader",169660853),new cljs.core.Keyword("com.wsscode.pathom.core","reader-fn","com.wsscode.pathom.core/reader-fn",1718614315)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Keyword("com.wsscode.pathom.core","reader-fn","com.wsscode.pathom.core/reader-fn",1718614315),new cljs.core.Keyword("com.wsscode.pathom.core","reader-fn","com.wsscode.pathom.core/reader-fn",1718614315),null,null),new cljs.core.Keyword("com.wsscode.pathom.core","reader-fn","com.wsscode.pathom.core/reader-fn",1718614315),null,null,null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","fspec","cljs.spec.alpha/fspec",-1289128341,null),new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"parser","parser",-1543495310),new cljs.core.Keyword("com.wsscode.pathom.core","parser","com.wsscode.pathom.core/parser",-740326395)),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword("com.wsscode.pathom.core","parser","com.wsscode.pathom.core/parser",-740326395)),cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"parser","parser",-1543495310),new cljs.core.Keyword("com.wsscode.pathom.core","parser","com.wsscode.pathom.core/parser",-740326395)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"parser","parser",-1543495310)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","parser","com.wsscode.pathom.core/parser",-740326395)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","parser","com.wsscode.pathom.core/parser",-740326395)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"parser","parser",-1543495310),new cljs.core.Keyword("com.wsscode.pathom.core","parser","com.wsscode.pathom.core/parser",-740326395)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Keyword("com.wsscode.pathom.core","parser","com.wsscode.pathom.core/parser",-740326395),new cljs.core.Keyword("com.wsscode.pathom.core","parser","com.wsscode.pathom.core/parser",-740326395),null,null),new cljs.core.Keyword("com.wsscode.pathom.core","parser","com.wsscode.pathom.core/parser",-740326395),null,null,null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","plugin","com.wsscode.pathom.core/plugin",-881556304),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null),new cljs.core.Keyword(null,"opt","opt",-794706369),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","wrap-read","com.wsscode.pathom.core/wrap-read",-904142261),new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793)], null)),cljs.spec.alpha.map_spec_impl(cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"req-un","req-un",1074571008),new cljs.core.Keyword(null,"opt-un","opt-un",883442496),new cljs.core.Keyword(null,"gfn","gfn",791517474),new cljs.core.Keyword(null,"pred-exprs","pred-exprs",1792271395),new cljs.core.Keyword(null,"keys-pred","keys-pred",858984739),new cljs.core.Keyword(null,"opt-keys","opt-keys",1262688261),new cljs.core.Keyword(null,"req-specs","req-specs",553962313),new cljs.core.Keyword(null,"req","req",-326448303),new cljs.core.Keyword(null,"req-keys","req-keys",514319221),new cljs.core.Keyword(null,"opt-specs","opt-specs",-384905450),new cljs.core.Keyword(null,"pred-forms","pred-forms",172611832),new cljs.core.Keyword(null,"opt","opt",-794706369)],[null,null,null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (G__61963){
return cljs.core.map_QMARK_(G__61963);
})], null),(function (G__61963){
return cljs.core.map_QMARK_(G__61963);
}),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","wrap-read","com.wsscode.pathom.core/wrap-read",-904142261),new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793)], null),cljs.core.PersistentVector.EMPTY,null,cljs.core.PersistentVector.EMPTY,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","wrap-read","com.wsscode.pathom.core/wrap-read",-904142261),new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.list(new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"%","%",-950237169,null)], null),cljs.core.list(new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Symbol(null,"%","%",-950237169,null)))], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","wrap-read","com.wsscode.pathom.core/wrap-read",-904142261),new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793)], null)])));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","parent-join-key","com.wsscode.pathom.core/parent-join-key",-289005491),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","or","cljs.spec.alpha/or",-831679639,null),new cljs.core.Keyword(null,"prop","prop",-515168332),new cljs.core.Keyword("edn-query-language.core","property","edn-query-language.core/property",-1479624874),new cljs.core.Keyword(null,"ident","ident",-742346),new cljs.core.Keyword("edn-query-language.core","ident","edn-query-language.core/ident",419196228),new cljs.core.Keyword(null,"call","call",-519999866),new cljs.core.Keyword("edn-query-language.core","mutation-key","edn-query-language.core/mutation-key",422562651)),cljs.spec.alpha.or_spec_impl(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"prop","prop",-515168332),new cljs.core.Keyword(null,"ident","ident",-742346),new cljs.core.Keyword(null,"call","call",-519999866)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("edn-query-language.core","property","edn-query-language.core/property",-1479624874),new cljs.core.Keyword("edn-query-language.core","ident","edn-query-language.core/ident",419196228),new cljs.core.Keyword("edn-query-language.core","mutation-key","edn-query-language.core/mutation-key",422562651)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("edn-query-language.core","property","edn-query-language.core/property",-1479624874),new cljs.core.Keyword("edn-query-language.core","ident","edn-query-language.core/ident",419196228),new cljs.core.Keyword("edn-query-language.core","mutation-key","edn-query-language.core/mutation-key",422562651)], null),null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","parent-query","com.wsscode.pathom.core/parent-query",1683392594),new cljs.core.Keyword("edn-query-language.core","join-query","edn-query-language.core/join-query",587629761),new cljs.core.Keyword("edn-query-language.core","join-query","edn-query-language.core/join-query",587629761));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","union-path","com.wsscode.pathom.core/union-path",-2083478095),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","or","cljs.spec.alpha/or",-831679639,null),new cljs.core.Keyword(null,"keyword","keyword",811389747),new cljs.core.Keyword("edn-query-language.core","property","edn-query-language.core/property",-1479624874),new cljs.core.Keyword(null,"fn","fn",-1175266204),new cljs.core.Symbol("cljs.core","fn?","cljs.core/fn?",71876239,null)),cljs.spec.alpha.or_spec_impl(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"keyword","keyword",811389747),new cljs.core.Keyword(null,"fn","fn",-1175266204)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("edn-query-language.core","property","edn-query-language.core/property",-1479624874),new cljs.core.Symbol("cljs.core","fn?","cljs.core/fn?",71876239,null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("edn-query-language.core","property","edn-query-language.core/property",-1479624874),cljs.core.fn_QMARK_], null),null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","async-request-cache-ch-size","com.wsscode.pathom.core/async-request-cache-ch-size",-437531159),new cljs.core.Symbol("cljs.core","pos-int?","cljs.core/pos-int?",-2115888030,null),cljs.core.pos_int_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","async-parser?","com.wsscode.pathom.core/async-parser?",920199905),new cljs.core.Symbol("cljs.core","boolean?","cljs.core/boolean?",1400713761,null),cljs.core.boolean_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","processing-sequence","com.wsscode.pathom.core/processing-sequence",1929448637),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","or","cljs.spec.alpha/or",-831679639,null),new cljs.core.Keyword(null,"coll","coll",1647737163),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","coll-of","cljs.spec.alpha/coll-of",1019430407,null),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),new cljs.core.Keyword(null,"map","map",1371690461),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","map-of","cljs.spec.alpha/map-of",153715093,null),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null))),cljs.spec.alpha.or_spec_impl(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"coll","coll",1647737163),new cljs.core.Keyword(null,"map","map",1371690461)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","coll-of","cljs.spec.alpha/coll-of",1019430407,null),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","map-of","cljs.spec.alpha/map-of",153715093,null),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null))], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.alpha.every_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Symbol(null,"map?","map?",-1780568534,null),cljs.core.map_QMARK_,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword("cljs.spec.alpha","conform-all","cljs.spec.alpha/conform-all",45201917),true,new cljs.core.Keyword("cljs.spec.alpha","kind-form","cljs.spec.alpha/kind-form",-1047104697),null,new cljs.core.Keyword("cljs.spec.alpha","describe","cljs.spec.alpha/describe",1883026911),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","coll-of","cljs.spec.alpha/coll-of",1019430407,null),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),new cljs.core.Keyword("cljs.spec.alpha","cpred","cljs.spec.alpha/cpred",-693471218),(function (G__61964){
return cljs.core.coll_QMARK_(G__61964);
})], null),null),cljs.spec.alpha.every_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","tuple","cljs.spec.alpha/tuple",-415901908,null),new cljs.core.Symbol(null,"any?","any?",-318999933,null),new cljs.core.Symbol(null,"map?","map?",-1780568534,null)),cljs.spec.alpha.tuple_impl.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.any_QMARK_,cljs.core.map_QMARK_], null)),new cljs.core.PersistentArrayMap(null, 7, [new cljs.core.Keyword("cljs.spec.alpha","kfn","cljs.spec.alpha/kfn",672643897),(function (i__20422__auto__,v__20423__auto__){
return cljs.core.nth.cljs$core$IFn$_invoke$arity$2(v__20423__auto__,(0));
}),new cljs.core.Keyword(null,"into","into",-150836029),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword("cljs.spec.alpha","conform-all","cljs.spec.alpha/conform-all",45201917),true,new cljs.core.Keyword(null,"kind","kind",-717265803),cljs.core.map_QMARK_,new cljs.core.Keyword("cljs.spec.alpha","kind-form","cljs.spec.alpha/kind-form",-1047104697),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Keyword("cljs.spec.alpha","describe","cljs.spec.alpha/describe",1883026911),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","map-of","cljs.spec.alpha/map-of",153715093,null),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),new cljs.core.Keyword("cljs.spec.alpha","cpred","cljs.spec.alpha/cpred",-693471218),(function (G__61965){
return cljs.core.map_QMARK_(G__61965);
})], null),null)], null),null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","coll-of","cljs.spec.alpha/coll-of",1019430407,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","or","cljs.spec.alpha/or",-831679639,null),new cljs.core.Keyword(null,"attr","attr",-604132353),new cljs.core.Keyword("com.wsscode.pathom.core","attribute","com.wsscode.pathom.core/attribute",1954645846),new cljs.core.Keyword(null,"ident","ident",-742346),new cljs.core.Keyword("edn-query-language.core","ident","edn-query-language.core/ident",419196228),new cljs.core.Keyword(null,"index","index",-1531685915),new cljs.core.Symbol("cljs.core","nat-int?","cljs.core/nat-int?",-164364171,null)),new cljs.core.Keyword(null,"kind","kind",-717265803),new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null)),cljs.spec.alpha.every_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("s","or","s/or",1876282981,null),new cljs.core.Keyword(null,"attr","attr",-604132353),new cljs.core.Keyword("com.wsscode.pathom.core","attribute","com.wsscode.pathom.core/attribute",1954645846),new cljs.core.Keyword(null,"ident","ident",-742346),new cljs.core.Keyword("edn-query-language.core","ident","edn-query-language.core/ident",419196228),new cljs.core.Keyword(null,"index","index",-1531685915),new cljs.core.Symbol(null,"nat-int?","nat-int?",-1879663400,null)),cljs.spec.alpha.or_spec_impl(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"attr","attr",-604132353),new cljs.core.Keyword(null,"ident","ident",-742346),new cljs.core.Keyword(null,"index","index",-1531685915)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","attribute","com.wsscode.pathom.core/attribute",1954645846),new cljs.core.Keyword("edn-query-language.core","ident","edn-query-language.core/ident",419196228),new cljs.core.Symbol("cljs.core","nat-int?","cljs.core/nat-int?",-164364171,null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","attribute","com.wsscode.pathom.core/attribute",1954645846),new cljs.core.Keyword("edn-query-language.core","ident","edn-query-language.core/ident",419196228),cljs.core.nat_int_QMARK_], null),null),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword("cljs.spec.alpha","conform-all","cljs.spec.alpha/conform-all",45201917),true,new cljs.core.Keyword(null,"kind","kind",-717265803),cljs.core.vector_QMARK_,new cljs.core.Keyword("cljs.spec.alpha","kind-form","cljs.spec.alpha/kind-form",-1047104697),new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null),new cljs.core.Keyword("cljs.spec.alpha","describe","cljs.spec.alpha/describe",1883026911),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","coll-of","cljs.spec.alpha/coll-of",1019430407,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","or","cljs.spec.alpha/or",-831679639,null),new cljs.core.Keyword(null,"attr","attr",-604132353),new cljs.core.Keyword("com.wsscode.pathom.core","attribute","com.wsscode.pathom.core/attribute",1954645846),new cljs.core.Keyword(null,"ident","ident",-742346),new cljs.core.Keyword("edn-query-language.core","ident","edn-query-language.core/ident",419196228),new cljs.core.Keyword(null,"index","index",-1531685915),new cljs.core.Symbol("cljs.core","nat-int?","cljs.core/nat-int?",-164364171,null)),new cljs.core.Keyword(null,"kind","kind",-717265803),new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null)),new cljs.core.Keyword("cljs.spec.alpha","cpred","cljs.spec.alpha/cpred",-693471218),(function (G__61966){
return cljs.core.vector_QMARK_(G__61966);
})], null),null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","map-of","cljs.spec.alpha/map-of",153715093,null),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681)),cljs.spec.alpha.every_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","tuple","cljs.spec.alpha/tuple",-415901908,null),new cljs.core.Symbol(null,"any?","any?",-318999933,null),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681)),cljs.spec.alpha.tuple_impl.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.any_QMARK_,new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681)], null)),new cljs.core.PersistentArrayMap(null, 7, [new cljs.core.Keyword("cljs.spec.alpha","kfn","cljs.spec.alpha/kfn",672643897),(function (i__20422__auto__,v__20423__auto__){
return cljs.core.nth.cljs$core$IFn$_invoke$arity$2(v__20423__auto__,(0));
}),new cljs.core.Keyword(null,"into","into",-150836029),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword("cljs.spec.alpha","conform-all","cljs.spec.alpha/conform-all",45201917),true,new cljs.core.Keyword(null,"kind","kind",-717265803),cljs.core.map_QMARK_,new cljs.core.Keyword("cljs.spec.alpha","kind-form","cljs.spec.alpha/kind-form",-1047104697),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Keyword("cljs.spec.alpha","describe","cljs.spec.alpha/describe",1883026911),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","map-of","cljs.spec.alpha/map-of",153715093,null),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681)),new cljs.core.Keyword("cljs.spec.alpha","cpred","cljs.spec.alpha/cpred",-693471218),(function (G__61967){
return cljs.core.map_QMARK_(G__61967);
})], null),null));
com.wsscode.pathom.core.break_values = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword("com.wsscode.pathom.core","not-found","com.wsscode.pathom.core/not-found",-1411330137),null,new cljs.core.Keyword("com.wsscode.pathom.core","reader-error","com.wsscode.pathom.core/reader-error",-803587882),null], null), null);
/**
 * Takes an AST and return a single set with all properties that appear in a query.
 * 
 *   Example:
 * 
 *   (-> [:foo {:bar [:baz]}] eql/query->ast pc/all-out-attributes)
 *   ; => #{:foo :bar :baz}
 */
com.wsscode.pathom.core.ast_properties = (function com$wsscode$pathom$core$ast_properties(p__61968){
var map__61969 = p__61968;
var map__61969__$1 = cljs.core.__destructure_map(map__61969);
var children = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__61969__$1,new cljs.core.Keyword(null,"children","children",-940561982));
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (attrs,p__61970){
var map__61971 = p__61970;
var map__61971__$1 = cljs.core.__destructure_map(map__61971);
var node = map__61971__$1;
var key = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__61971__$1,new cljs.core.Keyword(null,"key","key",-1516042587));
var children__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__61971__$1,new cljs.core.Keyword(null,"children","children",-940561982));
var G__61972 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(attrs,key);
if(cljs.core.truth_(children__$1)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(G__61972,(com.wsscode.pathom.core.ast_properties.cljs$core$IFn$_invoke$arity$1 ? com.wsscode.pathom.core.ast_properties.cljs$core$IFn$_invoke$arity$1(node) : com.wsscode.pathom.core.ast_properties.call(null,node)));
} else {
return G__61972;
}
}),cljs.core.PersistentHashSet.EMPTY,children);
});
/**
 * Merges nested maps without overwriting existing keys.
 */
com.wsscode.pathom.core.deep_merge = (function com$wsscode$pathom$core$deep_merge(var_args){
var args__5775__auto__ = [];
var len__5769__auto___63597 = arguments.length;
var i__5770__auto___63598 = (0);
while(true){
if((i__5770__auto___63598 < len__5769__auto___63597)){
args__5775__auto__.push((arguments[i__5770__auto___63598]));

var G__63599 = (i__5770__auto___63598 + (1));
i__5770__auto___63598 = G__63599;
continue;
} else {
}
break;
}

var argseq__5776__auto__ = ((((0) < args__5775__auto__.length))?(new cljs.core.IndexedSeq(args__5775__auto__.slice((0)),(0),null)):null);
return com.wsscode.pathom.core.deep_merge.cljs$core$IFn$_invoke$arity$variadic(argseq__5776__auto__);
});

(com.wsscode.pathom.core.deep_merge.cljs$core$IFn$_invoke$arity$variadic = (function (xs){
if(cljs.core.every_QMARK_((function (p1__61973_SHARP_){
return ((cljs.core.map_QMARK_(p1__61973_SHARP_)) || ((p1__61973_SHARP_ == null)));
}),xs)){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(cljs.core.merge_with,com.wsscode.pathom.core.deep_merge,xs);
} else {
return cljs.core.last(xs);
}
}));

(com.wsscode.pathom.core.deep_merge.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(com.wsscode.pathom.core.deep_merge.cljs$lang$applyTo = (function (seq61974){
var self__5755__auto__ = this;
return self__5755__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq61974));
}));

/**
 * Given a query expression convert it into an AST.
 */
com.wsscode.pathom.core.query__GT_ast = (function com$wsscode$pathom$core$query__GT_ast(query_expr){
return com.wsscode.pathom.parser.query__GT_ast(query_expr);
});
/**
 * Call query->ast and return the first children.
 */
com.wsscode.pathom.core.query__GT_ast1 = (function com$wsscode$pathom$core$query__GT_ast1(query_expr){
return cljs.core.first(new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(com.wsscode.pathom.core.query__GT_ast(query_expr)));
});
/**
 * Given an AST convert it back into a query expression.
 */
com.wsscode.pathom.core.ast__GT_query = (function com$wsscode$pathom$core$ast__GT_query(query_ast){
return com.wsscode.pathom.parser.ast__GT_expr.cljs$core$IFn$_invoke$arity$2(query_ast,true);
});
com.wsscode.pathom.core.filter_ast = (function com$wsscode$pathom$core$filter_ast(f,ast){
return clojure.walk.prewalk((function com$wsscode$pathom$core$filter_ast_$_filter_ast_walk(x){
if(((cljs.core.map_QMARK_(x)) && (cljs.core.contains_QMARK_(x,new cljs.core.Keyword(null,"children","children",-940561982))))){
return cljs.core.update.cljs$core$IFn$_invoke$arity$3(x,new cljs.core.Keyword(null,"children","children",-940561982),(function (p1__61975_SHARP_){
return cljs.core.filterv(f,p1__61975_SHARP_);
}));
} else {
return x;
}
}),ast);
});
/**
 * Get params from env, always returns a map.
 */
com.wsscode.pathom.core.params = (function com$wsscode$pathom$core$params(env){
var or__5045__auto__ = new cljs.core.Keyword(null,"params","params",710516235).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"ast","ast",-860334068).cljs$core$IFn$_invoke$arity$1(env));
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
return cljs.core.PersistentArrayMap.EMPTY;
}
});
/**
 * Add attribute param, eg:
 * 
 *   ```
 *   (p/update-attribute-param :keyword assoc :foo "bar") => (:keyword {:foo "bar"})
 *   (p/update-attribute-param '(:keyword {:param "prev"}) assoc :foo "bar") => (:keyword {:foo "bar" :param "prev"})
 *   ```
 *   
 */
com.wsscode.pathom.core.update_attribute_param = (function com$wsscode$pathom$core$update_attribute_param(var_args){
var args__5775__auto__ = [];
var len__5769__auto___63600 = arguments.length;
var i__5770__auto___63601 = (0);
while(true){
if((i__5770__auto___63601 < len__5769__auto___63600)){
args__5775__auto__.push((arguments[i__5770__auto___63601]));

var G__63602 = (i__5770__auto___63601 + (1));
i__5770__auto___63601 = G__63602;
continue;
} else {
}
break;
}

var argseq__5776__auto__ = ((((2) < args__5775__auto__.length))?(new cljs.core.IndexedSeq(args__5775__auto__.slice((2)),(0),null)):null);
return com.wsscode.pathom.core.update_attribute_param.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__5776__auto__);
});

(com.wsscode.pathom.core.update_attribute_param.cljs$core$IFn$_invoke$arity$variadic = (function (x,f,args){
if(cljs.core.seq_QMARK_(x)){
var vec__61979 = x;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__61979,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__61979,(1),null);
return (new cljs.core.List(null,k,(new cljs.core.List(null,cljs.core.apply.cljs$core$IFn$_invoke$arity$3(f,p,args),null,(1),null)),(2),null));
} else {
return (new cljs.core.List(null,x,(new cljs.core.List(null,cljs.core.apply.cljs$core$IFn$_invoke$arity$3(f,cljs.core.PersistentArrayMap.EMPTY,args),null,(1),null)),(2),null));
}
}));

(com.wsscode.pathom.core.update_attribute_param.cljs$lang$maxFixedArity = (2));

/** @this {Function} */
(com.wsscode.pathom.core.update_attribute_param.cljs$lang$applyTo = (function (seq61976){
var G__61977 = cljs.core.first(seq61976);
var seq61976__$1 = cljs.core.next(seq61976);
var G__61978 = cljs.core.first(seq61976__$1);
var seq61976__$2 = cljs.core.next(seq61976__$1);
var self__5754__auto__ = this;
return self__5754__auto__.cljs$core$IFn$_invoke$arity$variadic(G__61977,G__61978,seq61976__$2);
}));

com.wsscode.pathom.core.optional_attribute = (function com$wsscode$pathom$core$optional_attribute(x){
if(cljs.core.truth_((function (){var or__5045__auto__ = (x instanceof cljs.core.Keyword);
if(or__5045__auto__){
return or__5045__auto__;
} else {
return (new cljs.core.List(null,x,null,(1),null));
}
})())){
} else {
throw (new Error(["Assert failed: ","Optional value must be a keyword or a parameterized attribute","\n","(or (keyword? x) (list x))"].join('')));
}

return com.wsscode.pathom.core.update_attribute_param.cljs$core$IFn$_invoke$arity$variadic(x,cljs.core.assoc,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword("com.wsscode.pathom.core","optional?","com.wsscode.pathom.core/optional?",910087942),true], 0));
});
com.wsscode.pathom.core._QMARK_ = com.wsscode.pathom.core.optional_attribute;
/**
 * Given an AST point, check if the children is a union query type.
 */
com.wsscode.pathom.core.union_children_QMARK_ = (function com$wsscode$pathom$core$union_children_QMARK_(ast){
return cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"union","union",2142937499),(function (){var G__61982 = ast;
var G__61982__$1 = (((G__61982 == null))?null:new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(G__61982));
var G__61982__$2 = (((G__61982__$1 == null))?null:cljs.core.first(G__61982__$1));
if((G__61982__$2 == null)){
return null;
} else {
return new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(G__61982__$2);
}
})());
});
com.wsscode.pathom.core.maybe_merge_union_ast = (function com$wsscode$pathom$core$maybe_merge_union_ast(ast){
if(com.wsscode.pathom.core.union_children_QMARK_(ast)){
var merged_children = cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,cljs.core.mapcat.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"children","children",-940561982)),(function (){var G__61983 = ast;
var G__61983__$1 = (((G__61983 == null))?null:new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(G__61983));
var G__61983__$2 = (((G__61983__$1 == null))?null:cljs.core.first(G__61983__$1));
if((G__61983__$2 == null)){
return null;
} else {
return new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(G__61983__$2);
}
})());
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(ast,new cljs.core.Keyword(null,"children","children",-940561982),merged_children,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"query","query",-1288509510),edn_query_language.core.ast__GT_query(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"root","root",-448657453),new cljs.core.Keyword(null,"children","children",-940561982),merged_children], null))], 0));
} else {
return ast;
}
});
com.wsscode.pathom.core.merge_shapes = (function com$wsscode$pathom$core$merge_shapes(var_args){
var G__61985 = arguments.length;
switch (G__61985) {
case 1:
return com.wsscode.pathom.core.merge_shapes.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return com.wsscode.pathom.core.merge_shapes.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(com.wsscode.pathom.core.merge_shapes.cljs$core$IFn$_invoke$arity$1 = (function (a){
return a;
}));

(com.wsscode.pathom.core.merge_shapes.cljs$core$IFn$_invoke$arity$2 = (function (a,b){
if(((cljs.core.map_QMARK_(a)) && (cljs.core.map_QMARK_(b)))){
return cljs.core.merge_with.cljs$core$IFn$_invoke$arity$variadic(com.wsscode.pathom.core.merge_shapes,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([a,b], 0));
} else {
if(cljs.core.map_QMARK_(a)){
return a;
} else {
if(cljs.core.map_QMARK_(b)){
return b;
} else {
return b;

}
}
}
}));

(com.wsscode.pathom.core.merge_shapes.cljs$lang$maxFixedArity = 2);

cljs.spec.alpha.def_impl(new cljs.core.Symbol("com.wsscode.pathom.core","ast->shape-descriptor","com.wsscode.pathom.core/ast->shape-descriptor",880860245,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","fspec","cljs.spec.alpha/fspec",-1289128341,null),new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"ast","ast",-860334068),new cljs.core.Keyword("edn-query-language.ast","node","edn-query-language.ast/node",-1614840957)),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681)),cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"ast","ast",-860334068),new cljs.core.Keyword("edn-query-language.ast","node","edn-query-language.ast/node",-1614840957)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ast","ast",-860334068)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("edn-query-language.ast","node","edn-query-language.ast/node",-1614840957)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("edn-query-language.ast","node","edn-query-language.ast/node",-1614840957)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"ast","ast",-860334068),new cljs.core.Keyword("edn-query-language.ast","node","edn-query-language.ast/node",-1614840957)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681),null,null),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681),null,null,null));


/**
 * Convert AST to shape descriptor format
 * @type {function(*): *}
 */
com.wsscode.pathom.core.ast__GT_shape_descriptor = (function com$wsscode$pathom$core$ast__GT_shape_descriptor(ast){
var map__61990 = cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"ast","ast",-860334068),new cljs.core.Keyword("edn-query-language.ast","node","edn-query-language.ast/node",-1614840957)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ast","ast",-860334068)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("edn-query-language.ast","node","edn-query-language.ast/node",-1614840957)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("edn-query-language.ast","node","edn-query-language.ast/node",-1614840957)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"ast","ast",-860334068),new cljs.core.Keyword("edn-query-language.ast","node","edn-query-language.ast/node",-1614840957)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681),null,null),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681),null,null,null);
var map__61990__$1 = cljs.core.__destructure_map(map__61990);
var argspec61986 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__61990__$1,new cljs.core.Keyword(null,"args","args",1315556576));
var retspec61987 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__61990__$1,new cljs.core.Keyword(null,"ret","ret",-468222814));
if(cljs.core.truth_(argspec61986)){
com.fulcrologic.guardrails.core.run_check(new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/wsscode/pathom/core.cljc:221 ast->shape-descriptor's",new cljs.core.Keyword(null,"tap>?","tap>?",212454486),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false,new cljs.core.Keyword(null,"expound-opts","expound-opts",623087481),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"args?","args?",-1963723548),true], null),argspec61986,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [ast], null));
} else {
}

var f61989 = (function (ast__$1){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (m,p__61991){
var map__61992 = p__61991;
var map__61992__$1 = cljs.core.__destructure_map(map__61992);
var node = map__61992__$1;
var key = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__61992__$1,new cljs.core.Keyword(null,"key","key",-1516042587));
var type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__61992__$1,new cljs.core.Keyword(null,"type","type",1174270348));
var children = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__61992__$1,new cljs.core.Keyword(null,"children","children",-940561982));
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"union","union",2142937499),type)){
var unions = cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$1(com.wsscode.pathom.core.ast__GT_shape_descriptor),children);
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(com.wsscode.pathom.core.merge_shapes,m,unions);
} else {
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(m,key,(com.wsscode.pathom.core.ast__GT_shape_descriptor.cljs$core$IFn$_invoke$arity$1 ? com.wsscode.pathom.core.ast__GT_shape_descriptor.cljs$core$IFn$_invoke$arity$1(node) : com.wsscode.pathom.core.ast__GT_shape_descriptor.call(null,node)));
}
}),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(ast__$1));
});
var ret61988 = f61989(ast);
if(cljs.core.truth_(retspec61987)){
com.fulcrologic.guardrails.core.run_check(new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/wsscode/pathom/core.cljc:221 ast->shape-descriptor's",new cljs.core.Keyword(null,"tap>?","tap>?",212454486),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false,new cljs.core.Keyword(null,"expound-opts","expound-opts",623087481),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"args?","args?",-1963723548),false], null),retspec61987,ret61988);
} else {
}

return ret61988;
});
cljs.spec.alpha.def_impl(new cljs.core.Symbol("com.wsscode.pathom.core","map->shape-descriptor","com.wsscode.pathom.core/map->shape-descriptor",464146637,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","fspec","cljs.spec.alpha/fspec",-1289128341,null),new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"m","m",1632677161),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681)),cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"m","m",1632677161),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"m","m",1632677161)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.map_QMARK_], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"m","m",1632677161),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681),null,null),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681),null,null,null));


/**
 * Convert Map to shape descriptor format
 * @type {function(!cljs.core.IMap): *}
 */
com.wsscode.pathom.core.map__GT_shape_descriptor = (function com$wsscode$pathom$core$map__GT_shape_descriptor(m){
var map__61997 = cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"m","m",1632677161),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"m","m",1632677161)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.map_QMARK_], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"m","m",1632677161),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681),null,null),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681),null,null,null);
var map__61997__$1 = cljs.core.__destructure_map(map__61997);
var retspec61994 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__61997__$1,new cljs.core.Keyword(null,"ret","ret",-468222814));
var argspec61993 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__61997__$1,new cljs.core.Keyword(null,"args","args",1315556576));
if(cljs.core.truth_(argspec61993)){
com.fulcrologic.guardrails.core.run_check(new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/wsscode/pathom/core.cljc:234 map->shape-descriptor's",new cljs.core.Keyword(null,"tap>?","tap>?",212454486),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false,new cljs.core.Keyword(null,"expound-opts","expound-opts",623087481),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"args?","args?",-1963723548),true], null),argspec61993,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [m], null));
} else {
}

var f61996 = (function (m__$1){
return cljs.core.reduce_kv((function (m__$2,k,v){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(m__$2,k,((cljs.core.map_QMARK_(v))?(com.wsscode.pathom.core.map__GT_shape_descriptor.cljs$core$IFn$_invoke$arity$1 ? com.wsscode.pathom.core.map__GT_shape_descriptor.cljs$core$IFn$_invoke$arity$1(v) : com.wsscode.pathom.core.map__GT_shape_descriptor.call(null,v)):((cljs.core.sequential_QMARK_(v))?cljs.core.transduce.cljs$core$IFn$_invoke$arity$4(cljs.core.comp.cljs$core$IFn$_invoke$arity$2(cljs.core.filter.cljs$core$IFn$_invoke$arity$1(cljs.core.map_QMARK_),cljs.core.map.cljs$core$IFn$_invoke$arity$1(com.wsscode.pathom.core.map__GT_shape_descriptor)),com.wsscode.pathom.core.merge_shapes,cljs.core.PersistentArrayMap.EMPTY,v):cljs.core.PersistentArrayMap.EMPTY
)));
}),cljs.core.PersistentArrayMap.EMPTY,m__$1);
});
var ret61995 = f61996(m);
if(cljs.core.truth_(retspec61994)){
com.fulcrologic.guardrails.core.run_check(new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/wsscode/pathom/core.cljc:234 map->shape-descriptor's",new cljs.core.Keyword(null,"tap>?","tap>?",212454486),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false,new cljs.core.Keyword(null,"expound-opts","expound-opts",623087481),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"args?","args?",-1963723548),false], null),retspec61994,ret61995);
} else {
}

return ret61995;
});
com.wsscode.pathom.core.read_from_STAR_ = (function com$wsscode$pathom$core$read_from_STAR_(p__62000,reader){
var map__62001 = p__62000;
var map__62001__$1 = cljs.core.__destructure_map(map__62001);
var env = map__62001__$1;
var ast = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62001__$1,new cljs.core.Keyword(null,"ast","ast",-860334068));
if(cljs.core.map_QMARK_(reader)){
var k = new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(ast);
var temp__5802__auto__ = cljs.core.find(reader,k);
if(cljs.core.truth_(temp__5802__auto__)){
var vec__62002 = temp__5802__auto__;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__62002,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__62002,(1),null);
return (com.wsscode.pathom.core.read_from_STAR_.cljs$core$IFn$_invoke$arity$2 ? com.wsscode.pathom.core.read_from_STAR_.cljs$core$IFn$_invoke$arity$2(env,v) : com.wsscode.pathom.core.read_from_STAR_.call(null,env,v));
} else {
return new cljs.core.Keyword("com.wsscode.pathom.core","continue","com.wsscode.pathom.core/continue",591698194);
}
} else {
if(cljs.core.vector_QMARK_(reader)){
var res = cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,cljs.core.comp.cljs$core$IFn$_invoke$arity$3(cljs.core.map.cljs$core$IFn$_invoke$arity$1((function (p1__61998_SHARP_){
return (com.wsscode.pathom.core.read_from_STAR_.cljs$core$IFn$_invoke$arity$2 ? com.wsscode.pathom.core.read_from_STAR_.cljs$core$IFn$_invoke$arity$2(env,p1__61998_SHARP_) : com.wsscode.pathom.core.read_from_STAR_.call(null,env,p1__61998_SHARP_));
})),cljs.core.drop_while.cljs$core$IFn$_invoke$arity$1((function (p1__61999_SHARP_){
return cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(p1__61999_SHARP_,new cljs.core.Keyword("com.wsscode.pathom.core","continue","com.wsscode.pathom.core/continue",591698194));
})),cljs.core.take.cljs$core$IFn$_invoke$arity$1((1))),reader);
if(cljs.core.seq(res)){
return cljs.core.first(res);
} else {
return new cljs.core.Keyword("com.wsscode.pathom.core","continue","com.wsscode.pathom.core/continue",591698194);
}
} else {
if(cljs.core.ifn_QMARK_(reader)){
return (reader.cljs$core$IFn$_invoke$arity$1 ? reader.cljs$core$IFn$_invoke$arity$1(env) : reader.call(null,env));
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("Can't process reader",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"reader","reader",169660853),reader], null));

}
}
}
});
/**
 * Runs the read process for the reading, the reader can be a function, a vector or a map:
 * 
 *   function: will receive the environment as argument
 *   map: will dispatch from the ast key to a reader on the map value
 *   vector: will try to run each reader in sequence, when a reader returns ::p/continue it will try the next
 */
com.wsscode.pathom.core.read_from = (function com$wsscode$pathom$core$read_from(env,reader){
var res__53579__auto__ = com.wsscode.pathom.core.read_from_STAR_(env,reader);
if(com.wsscode.async.async_cljs.chan_QMARK_(res__53579__auto__)){
var c__42152__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__42153__auto__ = (function (){var switch__42120__auto__ = (function (state_62023){
var state_val_62024 = (state_62023[(1)]);
if((state_val_62024 === (7))){
var state_62023__$1 = state_62023;
var statearr_62025_63604 = state_62023__$1;
(statearr_62025_63604[(2)] = new cljs.core.Keyword("com.wsscode.pathom.core","not-found","com.wsscode.pathom.core/not-found",-1411330137));

(statearr_62025_63604[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62024 === (1))){
var state_62023__$1 = state_62023;
var statearr_62026_63605 = state_62023__$1;
(statearr_62026_63605[(2)] = null);

(statearr_62026_63605[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62024 === (4))){
var inst_62005 = (state_62023[(2)]);
var state_62023__$1 = state_62023;
var statearr_62027_63606 = state_62023__$1;
(statearr_62027_63606[(2)] = inst_62005);

(statearr_62027_63606[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62024 === (6))){
var inst_62013 = (state_62023[(7)]);
var inst_62012 = (state_62023[(2)]);
var inst_62013__$1 = com.wsscode.async.async_cljs.throw_err(inst_62012);
var inst_62014 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_62013__$1,new cljs.core.Keyword("com.wsscode.pathom.core","continue","com.wsscode.pathom.core/continue",591698194));
var state_62023__$1 = (function (){var statearr_62028 = state_62023;
(statearr_62028[(7)] = inst_62013__$1);

return statearr_62028;
})();
if(inst_62014){
var statearr_62029_63607 = state_62023__$1;
(statearr_62029_63607[(1)] = (7));

} else {
var statearr_62030_63608 = state_62023__$1;
(statearr_62030_63608[(1)] = (8));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62024 === (3))){
var inst_62021 = (state_62023[(2)]);
var state_62023__$1 = state_62023;
return cljs.core.async.impl.ioc_helpers.return_chan(state_62023__$1,inst_62021);
} else {
if((state_val_62024 === (2))){
var _ = (function (){var statearr_62032 = state_62023;
(statearr_62032[(4)] = cljs.core.cons((5),(state_62023[(4)])));

return statearr_62032;
})();
var state_62023__$1 = state_62023;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62023__$1,(6),res__53579__auto__);
} else {
if((state_val_62024 === (9))){
var inst_62018 = (state_62023[(2)]);
var _ = (function (){var statearr_62033 = state_62023;
(statearr_62033[(4)] = cljs.core.rest((state_62023[(4)])));

return statearr_62033;
})();
var state_62023__$1 = state_62023;
var statearr_62034_63609 = state_62023__$1;
(statearr_62034_63609[(2)] = inst_62018);

(statearr_62034_63609[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62024 === (5))){
var _ = (function (){var statearr_62035 = state_62023;
(statearr_62035[(4)] = cljs.core.rest((state_62023[(4)])));

return statearr_62035;
})();
var state_62023__$1 = state_62023;
var ex62031 = (state_62023__$1[(2)]);
var statearr_62036_63610 = state_62023__$1;
(statearr_62036_63610[(5)] = ex62031);


var statearr_62037_63611 = state_62023__$1;
(statearr_62037_63611[(1)] = (4));

(statearr_62037_63611[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62024 === (8))){
var inst_62013 = (state_62023[(7)]);
var state_62023__$1 = state_62023;
var statearr_62038_63612 = state_62023__$1;
(statearr_62038_63612[(2)] = inst_62013);

(statearr_62038_63612[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$read_from_$_state_machine__42121__auto__ = null;
var com$wsscode$pathom$core$read_from_$_state_machine__42121__auto____0 = (function (){
var statearr_62039 = [null,null,null,null,null,null,null,null];
(statearr_62039[(0)] = com$wsscode$pathom$core$read_from_$_state_machine__42121__auto__);

(statearr_62039[(1)] = (1));

return statearr_62039;
});
var com$wsscode$pathom$core$read_from_$_state_machine__42121__auto____1 = (function (state_62023){
while(true){
var ret_value__42122__auto__ = (function (){try{while(true){
var result__42123__auto__ = switch__42120__auto__(state_62023);
if(cljs.core.keyword_identical_QMARK_(result__42123__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__42123__auto__;
}
break;
}
}catch (e62040){var ex__42124__auto__ = e62040;
var statearr_62041_63613 = state_62023;
(statearr_62041_63613[(2)] = ex__42124__auto__);


if(cljs.core.seq((state_62023[(4)]))){
var statearr_62042_63614 = state_62023;
(statearr_62042_63614[(1)] = cljs.core.first((state_62023[(4)])));

} else {
throw ex__42124__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__42122__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__63615 = state_62023;
state_62023 = G__63615;
continue;
} else {
return ret_value__42122__auto__;
}
break;
}
});
com$wsscode$pathom$core$read_from_$_state_machine__42121__auto__ = function(state_62023){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$read_from_$_state_machine__42121__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$read_from_$_state_machine__42121__auto____1.call(this,state_62023);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$read_from_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$read_from_$_state_machine__42121__auto____0;
com$wsscode$pathom$core$read_from_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$read_from_$_state_machine__42121__auto____1;
return com$wsscode$pathom$core$read_from_$_state_machine__42121__auto__;
})()
})();
var state__42154__auto__ = (function (){var statearr_62043 = f__42153__auto__();
(statearr_62043[(6)] = c__42152__auto__);

return statearr_62043;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__42154__auto__);
}));

return c__42152__auto__;
} else {
var res = res__53579__auto__;
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(res,new cljs.core.Keyword("com.wsscode.pathom.core","continue","com.wsscode.pathom.core/continue",591698194))){
return new cljs.core.Keyword("com.wsscode.pathom.core","not-found","com.wsscode.pathom.core/not-found",-1411330137);
} else {
return res;
}
}
});
/**
 * Like read-from, pulling reader from environment.
 */
com.wsscode.pathom.core.reader = (function com$wsscode$pathom$core$reader(env){
return com.wsscode.pathom.core.read_from(env,new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410).cljs$core$IFn$_invoke$arity$1(env));
});
com.wsscode.pathom.core.native_map_QMARK_ = (function com$wsscode$pathom$core$native_map_QMARK_(x){
return ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(cljs.core.type(x),cljs.core.PersistentArrayMap)) || (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(cljs.core.type(x),cljs.core.PersistentHashMap)));
});
/**
 * Walk the structure and transduce every map with xform.
 */
com.wsscode.pathom.core.transduce_maps = (function com$wsscode$pathom$core$transduce_maps(xform,input){
return clojure.walk.prewalk((function com$wsscode$pathom$core$transduce_maps_$_elide_items_walk(x){
if(com.wsscode.pathom.core.native_map_QMARK_(x)){
return cljs.core.with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentArrayMap.EMPTY,xform,x),cljs.core.meta(x));
} else {
return x;
}
}),input);
});
/**
 * Recursively transduce children on the AST, you can use this to apply filter/transformations
 *   on a whole AST. Each iteration of the transducer will get a single AST node to process.
 * 
 *   ```
 *   (->> [:a {:b [:c :d]} :e]
 *     (p/query->ast)
 *     (p/transduce-children (remove (comp #{:a :c} :key)))
 *     (p/ast->query))
 *   ; => [{:b [:d]} :e]
 *   ```
 */
com.wsscode.pathom.core.transduce_children = (function com$wsscode$pathom$core$transduce_children(xform,p__62045){
var map__62046 = p__62045;
var map__62046__$1 = cljs.core.__destructure_map(map__62046);
var node = map__62046__$1;
var children = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62046__$1,new cljs.core.Keyword(null,"children","children",-940561982));
var G__62047 = node;
if(cljs.core.seq(children)){
return cljs.core.update.cljs$core$IFn$_invoke$arity$3(G__62047,new cljs.core.Keyword(null,"children","children",-940561982),(function (children__$1){
return cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,cljs.core.comp.cljs$core$IFn$_invoke$arity$2(xform,cljs.core.map.cljs$core$IFn$_invoke$arity$1((function (p1__62044_SHARP_){
return (com.wsscode.pathom.core.transduce_children.cljs$core$IFn$_invoke$arity$2 ? com.wsscode.pathom.core.transduce_children.cljs$core$IFn$_invoke$arity$2(xform,p1__62044_SHARP_) : com.wsscode.pathom.core.transduce_children.call(null,xform,p1__62044_SHARP_));
}))),children__$1);
}));
} else {
return G__62047;
}
});
com.wsscode.pathom.core.special_outputs = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword("com.wsscode.pathom.core","not-found","com.wsscode.pathom.core/not-found",-1411330137),null,new cljs.core.Keyword("com.wsscode.pathom.core","reader-error","com.wsscode.pathom.core/reader-error",-803587882),null], null), null);
/**
 * Removes any item on set item-set from the input
 */
com.wsscode.pathom.core.elide_items = (function com$wsscode$pathom$core$elide_items(item_set,input){
return cljs.core.with_meta(com.wsscode.pathom.core.transduce_maps(cljs.core.remove.cljs$core$IFn$_invoke$arity$1((function (p__62048){
var vec__62049 = p__62048;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__62049,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__62049,(1),null);
return cljs.core.contains_QMARK_(item_set,v);
})),input),cljs.core.meta(input));
});
/**
 * Convert all ::p/not-found values of maps to nil
 */
com.wsscode.pathom.core.elide_not_found = (function com$wsscode$pathom$core$elide_not_found(input){
return com.wsscode.pathom.core.elide_items(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("com.wsscode.pathom.core","not-found","com.wsscode.pathom.core/not-found",-1411330137),null], null), null),input);
});
/**
 * Convert all ::p/not-found values of maps to nil
 */
com.wsscode.pathom.core.elide_special_outputs = (function com$wsscode$pathom$core$elide_special_outputs(input){
return com.wsscode.pathom.core.elide_items(com.wsscode.pathom.core.special_outputs,input);
});
com.wsscode.pathom.core.focus_subquery = com.wsscode.pathom.parser.focus_subquery;
com.wsscode.pathom.core.atom_QMARK_ = (function com$wsscode$pathom$core$atom_QMARK_(x){
if((!((x == null)))){
if((((x.cljs$lang$protocol_mask$partition0$ & (32768))) || ((cljs.core.PROTOCOL_SENTINEL === x.cljs$core$IDeref$)))){
return true;
} else {
if((!x.cljs$lang$protocol_mask$partition0$)){
return cljs.core.native_satisfies_QMARK_(cljs.core.IDeref,x);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_(cljs.core.IDeref,x);
}
});
com.wsscode.pathom.core.normalize_atom = (function com$wsscode$pathom$core$normalize_atom(x){
if(com.wsscode.pathom.core.atom_QMARK_(x)){
return x;
} else {
return cljs.core.atom.cljs$core$IFn$_invoke$arity$1(x);
}
});
com.wsscode.pathom.core.raw_entity = (function com$wsscode$pathom$core$raw_entity(p__62053){
var map__62054 = p__62053;
var map__62054__$1 = cljs.core.__destructure_map(map__62054);
var env = map__62054__$1;
var entity_key = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62054__$1,new cljs.core.Keyword("com.wsscode.pathom.core","entity-key","com.wsscode.pathom.core/entity-key",1494103249));
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(env,(function (){var or__5045__auto__ = entity_key;
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
return new cljs.core.Keyword("com.wsscode.pathom.core","entity","com.wsscode.pathom.core/entity",884706031);
}
})());
});
com.wsscode.pathom.core.maybe_atom = (function com$wsscode$pathom$core$maybe_atom(x){
if(com.wsscode.pathom.core.atom_QMARK_(x)){
return cljs.core.deref(x);
} else {
return x;
}
});
/**
 * This is used for merging new parsed attributes from entity, works like regular merge but if the value from the right
 *   direction is not found, then the previous value will be kept.
 */
com.wsscode.pathom.core.entity_value_merge = (function com$wsscode$pathom$core$entity_value_merge(x,y){
if((y === new cljs.core.Keyword("com.wsscode.pathom.core","not-found","com.wsscode.pathom.core/not-found",-1411330137))){
return x;
} else {
return y;
}
});
/**
 * Fetch the entity according to the ::entity-key. If the entity is an IAtom, it will be derefed.
 * 
 *   If a second argument is sent, calls the parser against current element to guarantee that some fields are loaded. This
 *   is useful when you need to ensure some values are loaded in order to fetch some more complex data. NOTE: When using
 *   this call with an explicit vector of attributes the parser will not be invoked for attributes that already exist in
 *   the current value of the current entity.
 */
com.wsscode.pathom.core.entity = (function com$wsscode$pathom$core$entity(var_args){
var G__62056 = arguments.length;
switch (G__62056) {
case 1:
return com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$1 = (function (env){
var e = com.wsscode.pathom.core.raw_entity(env);
return com.wsscode.pathom.core.maybe_atom(e);
}));

(com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$2 = (function (p__62057,attributes){
var map__62058 = p__62057;
var map__62058__$1 = cljs.core.__destructure_map(map__62058);
var env = map__62058__$1;
var parser = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62058__$1,new cljs.core.Keyword(null,"parser","parser",-1543495310));
var e = com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$1(env);
var res__53579__auto__ = (function (){var G__62059 = env;
var G__62060 = cljs.core.filterv(cljs.core.complement(cljs.core.set(cljs.core.keys(e))),attributes);
return (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(G__62059,G__62060) : parser.call(null,G__62059,G__62060));
})();
if(com.wsscode.async.async_cljs.chan_QMARK_(res__53579__auto__)){
var c__42152__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__42153__auto__ = (function (){var switch__42120__auto__ = (function (state_62080){
var state_val_62081 = (state_62080[(1)]);
if((state_val_62081 === (7))){
var inst_62069 = (state_62080[(7)]);
var inst_62072 = cljs.core.merge_with.cljs$core$IFn$_invoke$arity$variadic(com.wsscode.pathom.core.entity_value_merge,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([e,inst_62069], 0));
var state_62080__$1 = state_62080;
var statearr_62082_63619 = state_62080__$1;
(statearr_62082_63619[(2)] = inst_62072);

(statearr_62082_63619[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62081 === (1))){
var state_62080__$1 = state_62080;
var statearr_62083_63620 = state_62080__$1;
(statearr_62083_63620[(2)] = null);

(statearr_62083_63620[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62081 === (4))){
var inst_62061 = (state_62080[(2)]);
var state_62080__$1 = state_62080;
var statearr_62084_63622 = state_62080__$1;
(statearr_62084_63622[(2)] = inst_62061);

(statearr_62084_63622[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62081 === (6))){
var inst_62069 = (state_62080[(7)]);
var inst_62068 = (state_62080[(2)]);
var inst_62069__$1 = com.wsscode.async.async_cljs.throw_err(inst_62068);
var inst_62070 = cljs.core.map_QMARK_(inst_62069__$1);
var state_62080__$1 = (function (){var statearr_62085 = state_62080;
(statearr_62085[(7)] = inst_62069__$1);

return statearr_62085;
})();
if(inst_62070){
var statearr_62086_63626 = state_62080__$1;
(statearr_62086_63626[(1)] = (7));

} else {
var statearr_62087_63627 = state_62080__$1;
(statearr_62087_63627[(1)] = (8));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62081 === (3))){
var inst_62078 = (state_62080[(2)]);
var state_62080__$1 = state_62080;
return cljs.core.async.impl.ioc_helpers.return_chan(state_62080__$1,inst_62078);
} else {
if((state_val_62081 === (2))){
var _ = (function (){var statearr_62089 = state_62080;
(statearr_62089[(4)] = cljs.core.cons((5),(state_62080[(4)])));

return statearr_62089;
})();
var state_62080__$1 = state_62080;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62080__$1,(6),res__53579__auto__);
} else {
if((state_val_62081 === (9))){
var inst_62075 = (state_62080[(2)]);
var _ = (function (){var statearr_62090 = state_62080;
(statearr_62090[(4)] = cljs.core.rest((state_62080[(4)])));

return statearr_62090;
})();
var state_62080__$1 = state_62080;
var statearr_62091_63628 = state_62080__$1;
(statearr_62091_63628[(2)] = inst_62075);

(statearr_62091_63628[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62081 === (5))){
var _ = (function (){var statearr_62092 = state_62080;
(statearr_62092[(4)] = cljs.core.rest((state_62080[(4)])));

return statearr_62092;
})();
var state_62080__$1 = state_62080;
var ex62088 = (state_62080__$1[(2)]);
var statearr_62093_63629 = state_62080__$1;
(statearr_62093_63629[(5)] = ex62088);


var statearr_62094_63630 = state_62080__$1;
(statearr_62094_63630[(1)] = (4));

(statearr_62094_63630[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62081 === (8))){
var state_62080__$1 = state_62080;
var statearr_62095_63632 = state_62080__$1;
(statearr_62095_63632[(2)] = e);

(statearr_62095_63632[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$state_machine__42121__auto__ = null;
var com$wsscode$pathom$core$state_machine__42121__auto____0 = (function (){
var statearr_62096 = [null,null,null,null,null,null,null,null];
(statearr_62096[(0)] = com$wsscode$pathom$core$state_machine__42121__auto__);

(statearr_62096[(1)] = (1));

return statearr_62096;
});
var com$wsscode$pathom$core$state_machine__42121__auto____1 = (function (state_62080){
while(true){
var ret_value__42122__auto__ = (function (){try{while(true){
var result__42123__auto__ = switch__42120__auto__(state_62080);
if(cljs.core.keyword_identical_QMARK_(result__42123__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__42123__auto__;
}
break;
}
}catch (e62097){var ex__42124__auto__ = e62097;
var statearr_62098_63634 = state_62080;
(statearr_62098_63634[(2)] = ex__42124__auto__);


if(cljs.core.seq((state_62080[(4)]))){
var statearr_62099_63635 = state_62080;
(statearr_62099_63635[(1)] = cljs.core.first((state_62080[(4)])));

} else {
throw ex__42124__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__42122__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__63636 = state_62080;
state_62080 = G__63636;
continue;
} else {
return ret_value__42122__auto__;
}
break;
}
});
com$wsscode$pathom$core$state_machine__42121__auto__ = function(state_62080){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$state_machine__42121__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$state_machine__42121__auto____1.call(this,state_62080);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$state_machine__42121__auto____0;
com$wsscode$pathom$core$state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$state_machine__42121__auto____1;
return com$wsscode$pathom$core$state_machine__42121__auto__;
})()
})();
var state__42154__auto__ = (function (){var statearr_62100 = f__42153__auto__();
(statearr_62100[(6)] = c__42152__auto__);

return statearr_62100;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__42154__auto__);
}));

return c__42152__auto__;
} else {
var res = res__53579__auto__;
if(cljs.core.map_QMARK_(res)){
return cljs.core.merge_with.cljs$core$IFn$_invoke$arity$variadic(com.wsscode.pathom.core.entity_value_merge,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([e,res], 0));
} else {
return e;
}
}
}));

(com.wsscode.pathom.core.entity.cljs$lang$maxFixedArity = 2);

/**
 * Helper function to fetch a single attribute from current entity.
 */
com.wsscode.pathom.core.entity_attr = (function com$wsscode$pathom$core$entity_attr(var_args){
var G__62102 = arguments.length;
switch (G__62102) {
case 2:
return com.wsscode.pathom.core.entity_attr.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return com.wsscode.pathom.core.entity_attr.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(com.wsscode.pathom.core.entity_attr.cljs$core$IFn$_invoke$arity$2 = (function (env,attr){
var res__53579__auto__ = com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$2(env,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [attr], null));
if(com.wsscode.async.async_cljs.chan_QMARK_(res__53579__auto__)){
var c__42152__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__42153__auto__ = (function (){var switch__42120__auto__ = (function (state_62117){
var state_val_62118 = (state_62117[(1)]);
if((state_val_62118 === (1))){
var state_62117__$1 = state_62117;
var statearr_62119_63650 = state_62117__$1;
(statearr_62119_63650[(2)] = null);

(statearr_62119_63650[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62118 === (2))){
var _ = (function (){var statearr_62120 = state_62117;
(statearr_62120[(4)] = cljs.core.cons((5),(state_62117[(4)])));

return statearr_62120;
})();
var state_62117__$1 = state_62117;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62117__$1,(6),res__53579__auto__);
} else {
if((state_val_62118 === (3))){
var inst_62115 = (state_62117[(2)]);
var state_62117__$1 = state_62117;
return cljs.core.async.impl.ioc_helpers.return_chan(state_62117__$1,inst_62115);
} else {
if((state_val_62118 === (4))){
var inst_62103 = (state_62117[(2)]);
var state_62117__$1 = state_62117;
var statearr_62122_63651 = state_62117__$1;
(statearr_62122_63651[(2)] = inst_62103);

(statearr_62122_63651[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62118 === (5))){
var _ = (function (){var statearr_62123 = state_62117;
(statearr_62123[(4)] = cljs.core.rest((state_62117[(4)])));

return statearr_62123;
})();
var state_62117__$1 = state_62117;
var ex62121 = (state_62117__$1[(2)]);
var statearr_62124_63652 = state_62117__$1;
(statearr_62124_63652[(5)] = ex62121);


var statearr_62125_63653 = state_62117__$1;
(statearr_62125_63653[(1)] = (4));

(statearr_62125_63653[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62118 === (6))){
var inst_62110 = (state_62117[(2)]);
var inst_62111 = com.wsscode.async.async_cljs.throw_err(inst_62110);
var inst_62112 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_62111,attr);
var _ = (function (){var statearr_62126 = state_62117;
(statearr_62126[(4)] = cljs.core.rest((state_62117[(4)])));

return statearr_62126;
})();
var state_62117__$1 = state_62117;
var statearr_62127_63658 = state_62117__$1;
(statearr_62127_63658[(2)] = inst_62112);

(statearr_62127_63658[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$state_machine__42121__auto__ = null;
var com$wsscode$pathom$core$state_machine__42121__auto____0 = (function (){
var statearr_62128 = [null,null,null,null,null,null,null];
(statearr_62128[(0)] = com$wsscode$pathom$core$state_machine__42121__auto__);

(statearr_62128[(1)] = (1));

return statearr_62128;
});
var com$wsscode$pathom$core$state_machine__42121__auto____1 = (function (state_62117){
while(true){
var ret_value__42122__auto__ = (function (){try{while(true){
var result__42123__auto__ = switch__42120__auto__(state_62117);
if(cljs.core.keyword_identical_QMARK_(result__42123__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__42123__auto__;
}
break;
}
}catch (e62129){var ex__42124__auto__ = e62129;
var statearr_62130_63662 = state_62117;
(statearr_62130_63662[(2)] = ex__42124__auto__);


if(cljs.core.seq((state_62117[(4)]))){
var statearr_62131_63663 = state_62117;
(statearr_62131_63663[(1)] = cljs.core.first((state_62117[(4)])));

} else {
throw ex__42124__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__42122__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__63664 = state_62117;
state_62117 = G__63664;
continue;
} else {
return ret_value__42122__auto__;
}
break;
}
});
com$wsscode$pathom$core$state_machine__42121__auto__ = function(state_62117){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$state_machine__42121__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$state_machine__42121__auto____1.call(this,state_62117);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$state_machine__42121__auto____0;
com$wsscode$pathom$core$state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$state_machine__42121__auto____1;
return com$wsscode$pathom$core$state_machine__42121__auto__;
})()
})();
var state__42154__auto__ = (function (){var statearr_62132 = f__42153__auto__();
(statearr_62132[(6)] = c__42152__auto__);

return statearr_62132;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__42154__auto__);
}));

return c__42152__auto__;
} else {
var e = res__53579__auto__;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(e,attr);
}
}));

(com.wsscode.pathom.core.entity_attr.cljs$core$IFn$_invoke$arity$3 = (function (env,attr,default$){
var res__53579__auto__ = com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$2(env,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [attr], null));
if(com.wsscode.async.async_cljs.chan_QMARK_(res__53579__auto__)){
var c__42152__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__42153__auto__ = (function (){var switch__42120__auto__ = (function (state_62155){
var state_val_62156 = (state_62155[(1)]);
if((state_val_62156 === (7))){
var state_62155__$1 = state_62155;
var statearr_62157_63668 = state_62155__$1;
(statearr_62157_63668[(2)] = default$);

(statearr_62157_63668[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62156 === (1))){
var state_62155__$1 = state_62155;
var statearr_62158_63669 = state_62155__$1;
(statearr_62158_63669[(2)] = null);

(statearr_62158_63669[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62156 === (4))){
var inst_62133 = (state_62155[(2)]);
var state_62155__$1 = state_62155;
var statearr_62159_63670 = state_62155__$1;
(statearr_62159_63670[(2)] = inst_62133);

(statearr_62159_63670[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62156 === (6))){
var inst_62142 = (state_62155[(7)]);
var inst_62140 = (state_62155[(2)]);
var inst_62141 = com.wsscode.async.async_cljs.throw_err(inst_62140);
var inst_62142__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_62141,attr);
var inst_62143 = [null,null,new cljs.core.Keyword("com.wsscode.pathom.core","not-found","com.wsscode.pathom.core/not-found",-1411330137),null];
var inst_62144 = (new cljs.core.PersistentArrayMap(null,2,inst_62143,null));
var inst_62145 = (new cljs.core.PersistentHashSet(null,inst_62144,null));
var inst_62146 = (inst_62145.cljs$core$IFn$_invoke$arity$1 ? inst_62145.cljs$core$IFn$_invoke$arity$1(inst_62142__$1) : inst_62145.call(null,inst_62142__$1));
var state_62155__$1 = (function (){var statearr_62160 = state_62155;
(statearr_62160[(7)] = inst_62142__$1);

return statearr_62160;
})();
if(cljs.core.truth_(inst_62146)){
var statearr_62161_63672 = state_62155__$1;
(statearr_62161_63672[(1)] = (7));

} else {
var statearr_62162_63673 = state_62155__$1;
(statearr_62162_63673[(1)] = (8));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62156 === (3))){
var inst_62153 = (state_62155[(2)]);
var state_62155__$1 = state_62155;
return cljs.core.async.impl.ioc_helpers.return_chan(state_62155__$1,inst_62153);
} else {
if((state_val_62156 === (2))){
var _ = (function (){var statearr_62164 = state_62155;
(statearr_62164[(4)] = cljs.core.cons((5),(state_62155[(4)])));

return statearr_62164;
})();
var state_62155__$1 = state_62155;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62155__$1,(6),res__53579__auto__);
} else {
if((state_val_62156 === (9))){
var inst_62150 = (state_62155[(2)]);
var _ = (function (){var statearr_62165 = state_62155;
(statearr_62165[(4)] = cljs.core.rest((state_62155[(4)])));

return statearr_62165;
})();
var state_62155__$1 = state_62155;
var statearr_62166_63675 = state_62155__$1;
(statearr_62166_63675[(2)] = inst_62150);

(statearr_62166_63675[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62156 === (5))){
var _ = (function (){var statearr_62167 = state_62155;
(statearr_62167[(4)] = cljs.core.rest((state_62155[(4)])));

return statearr_62167;
})();
var state_62155__$1 = state_62155;
var ex62163 = (state_62155__$1[(2)]);
var statearr_62168_63676 = state_62155__$1;
(statearr_62168_63676[(5)] = ex62163);


var statearr_62169_63677 = state_62155__$1;
(statearr_62169_63677[(1)] = (4));

(statearr_62169_63677[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62156 === (8))){
var inst_62142 = (state_62155[(7)]);
var state_62155__$1 = state_62155;
var statearr_62170_63680 = state_62155__$1;
(statearr_62170_63680[(2)] = inst_62142);

(statearr_62170_63680[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$state_machine__42121__auto__ = null;
var com$wsscode$pathom$core$state_machine__42121__auto____0 = (function (){
var statearr_62171 = [null,null,null,null,null,null,null,null];
(statearr_62171[(0)] = com$wsscode$pathom$core$state_machine__42121__auto__);

(statearr_62171[(1)] = (1));

return statearr_62171;
});
var com$wsscode$pathom$core$state_machine__42121__auto____1 = (function (state_62155){
while(true){
var ret_value__42122__auto__ = (function (){try{while(true){
var result__42123__auto__ = switch__42120__auto__(state_62155);
if(cljs.core.keyword_identical_QMARK_(result__42123__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__42123__auto__;
}
break;
}
}catch (e62172){var ex__42124__auto__ = e62172;
var statearr_62173_63686 = state_62155;
(statearr_62173_63686[(2)] = ex__42124__auto__);


if(cljs.core.seq((state_62155[(4)]))){
var statearr_62174_63687 = state_62155;
(statearr_62174_63687[(1)] = cljs.core.first((state_62155[(4)])));

} else {
throw ex__42124__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__42122__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__63688 = state_62155;
state_62155 = G__63688;
continue;
} else {
return ret_value__42122__auto__;
}
break;
}
});
com$wsscode$pathom$core$state_machine__42121__auto__ = function(state_62155){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$state_machine__42121__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$state_machine__42121__auto____1.call(this,state_62155);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$state_machine__42121__auto____0;
com$wsscode$pathom$core$state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$state_machine__42121__auto____1;
return com$wsscode$pathom$core$state_machine__42121__auto__;
})()
})();
var state__42154__auto__ = (function (){var statearr_62175 = f__42153__auto__();
(statearr_62175[(6)] = c__42152__auto__);

return statearr_62175;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__42154__auto__);
}));

return c__42152__auto__;
} else {
var e = res__53579__auto__;
var x = cljs.core.get.cljs$core$IFn$_invoke$arity$2(e,attr);
if(cljs.core.truth_((function (){var fexpr__62176 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [null,null,new cljs.core.Keyword("com.wsscode.pathom.core","not-found","com.wsscode.pathom.core/not-found",-1411330137),null], null), null);
return (fexpr__62176.cljs$core$IFn$_invoke$arity$1 ? fexpr__62176.cljs$core$IFn$_invoke$arity$1(x) : fexpr__62176.call(null,x));
})())){
return default$;
} else {
return x;
}
}
}));

(com.wsscode.pathom.core.entity_attr.cljs$lang$maxFixedArity = 3);

com.wsscode.pathom.core.entity_BANG_ = (function com$wsscode$pathom$core$entity_BANG_(p__62177,attributes){
var map__62178 = p__62177;
var map__62178__$1 = cljs.core.__destructure_map(map__62178);
var env = map__62178__$1;
var path = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62178__$1,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661));
var res__53579__auto__ = com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$2(env,attributes);
if(com.wsscode.async.async_cljs.chan_QMARK_(res__53579__auto__)){
var c__42152__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__42153__auto__ = (function (){var switch__42120__auto__ = (function (state_62209){
var state_val_62210 = (state_62209[(1)]);
if((state_val_62210 === (7))){
var inst_62192 = (state_62209[(7)]);
var inst_62187 = (state_62209[(8)]);
var inst_62195 = cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([inst_62192], 0));
var inst_62196 = ["Entity attributes ",inst_62195," could not be realized"].join('');
var inst_62197 = [new cljs.core.Keyword("com.wsscode.pathom.core","entity","com.wsscode.pathom.core/entity",884706031),new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),new cljs.core.Keyword("com.wsscode.pathom.core","missing-attributes","com.wsscode.pathom.core/missing-attributes",1114260849)];
var inst_62198 = [inst_62187,path,inst_62192];
var inst_62199 = cljs.core.PersistentHashMap.fromArrays(inst_62197,inst_62198);
var inst_62200 = cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2(inst_62196,inst_62199);
var inst_62201 = (function(){throw inst_62200})();
var state_62209__$1 = state_62209;
var statearr_62211_63689 = state_62209__$1;
(statearr_62211_63689[(2)] = inst_62201);

(statearr_62211_63689[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62210 === (1))){
var state_62209__$1 = state_62209;
var statearr_62212_63690 = state_62209__$1;
(statearr_62212_63690[(2)] = null);

(statearr_62212_63690[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62210 === (4))){
var inst_62179 = (state_62209[(2)]);
var state_62209__$1 = state_62209;
var statearr_62213_63691 = state_62209__$1;
(statearr_62213_63691[(2)] = inst_62179);

(statearr_62213_63691[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62210 === (6))){
var inst_62187 = (state_62209[(8)]);
var inst_62192 = (state_62209[(7)]);
var inst_62186 = (state_62209[(2)]);
var inst_62187__$1 = com.wsscode.async.async_cljs.throw_err(inst_62186);
var inst_62188 = cljs.core.set(attributes);
var inst_62189 = com.wsscode.pathom.core.elide_not_found(inst_62187__$1);
var inst_62190 = cljs.core.keys(inst_62189);
var inst_62191 = cljs.core.set(inst_62190);
var inst_62192__$1 = clojure.set.difference.cljs$core$IFn$_invoke$arity$2(inst_62188,inst_62191);
var inst_62193 = cljs.core.seq(inst_62192__$1);
var state_62209__$1 = (function (){var statearr_62214 = state_62209;
(statearr_62214[(8)] = inst_62187__$1);

(statearr_62214[(7)] = inst_62192__$1);

return statearr_62214;
})();
if(inst_62193){
var statearr_62215_63692 = state_62209__$1;
(statearr_62215_63692[(1)] = (7));

} else {
var statearr_62216_63693 = state_62209__$1;
(statearr_62216_63693[(1)] = (8));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62210 === (3))){
var inst_62207 = (state_62209[(2)]);
var state_62209__$1 = state_62209;
return cljs.core.async.impl.ioc_helpers.return_chan(state_62209__$1,inst_62207);
} else {
if((state_val_62210 === (2))){
var _ = (function (){var statearr_62218 = state_62209;
(statearr_62218[(4)] = cljs.core.cons((5),(state_62209[(4)])));

return statearr_62218;
})();
var state_62209__$1 = state_62209;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62209__$1,(6),res__53579__auto__);
} else {
if((state_val_62210 === (9))){
var inst_62187 = (state_62209[(8)]);
var inst_62204 = (state_62209[(2)]);
var _ = (function (){var statearr_62219 = state_62209;
(statearr_62219[(4)] = cljs.core.rest((state_62209[(4)])));

return statearr_62219;
})();
var state_62209__$1 = (function (){var statearr_62220 = state_62209;
(statearr_62220[(9)] = inst_62204);

return statearr_62220;
})();
var statearr_62221_63694 = state_62209__$1;
(statearr_62221_63694[(2)] = inst_62187);

(statearr_62221_63694[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62210 === (5))){
var _ = (function (){var statearr_62222 = state_62209;
(statearr_62222[(4)] = cljs.core.rest((state_62209[(4)])));

return statearr_62222;
})();
var state_62209__$1 = state_62209;
var ex62217 = (state_62209__$1[(2)]);
var statearr_62223_63695 = state_62209__$1;
(statearr_62223_63695[(5)] = ex62217);


var statearr_62224_63696 = state_62209__$1;
(statearr_62224_63696[(1)] = (4));

(statearr_62224_63696[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62210 === (8))){
var state_62209__$1 = state_62209;
var statearr_62225_63697 = state_62209__$1;
(statearr_62225_63697[(2)] = null);

(statearr_62225_63697[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$entity_BANG__$_state_machine__42121__auto__ = null;
var com$wsscode$pathom$core$entity_BANG__$_state_machine__42121__auto____0 = (function (){
var statearr_62226 = [null,null,null,null,null,null,null,null,null,null];
(statearr_62226[(0)] = com$wsscode$pathom$core$entity_BANG__$_state_machine__42121__auto__);

(statearr_62226[(1)] = (1));

return statearr_62226;
});
var com$wsscode$pathom$core$entity_BANG__$_state_machine__42121__auto____1 = (function (state_62209){
while(true){
var ret_value__42122__auto__ = (function (){try{while(true){
var result__42123__auto__ = switch__42120__auto__(state_62209);
if(cljs.core.keyword_identical_QMARK_(result__42123__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__42123__auto__;
}
break;
}
}catch (e62227){var ex__42124__auto__ = e62227;
var statearr_62228_63698 = state_62209;
(statearr_62228_63698[(2)] = ex__42124__auto__);


if(cljs.core.seq((state_62209[(4)]))){
var statearr_62229_63699 = state_62209;
(statearr_62229_63699[(1)] = cljs.core.first((state_62209[(4)])));

} else {
throw ex__42124__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__42122__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__63700 = state_62209;
state_62209 = G__63700;
continue;
} else {
return ret_value__42122__auto__;
}
break;
}
});
com$wsscode$pathom$core$entity_BANG__$_state_machine__42121__auto__ = function(state_62209){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$entity_BANG__$_state_machine__42121__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$entity_BANG__$_state_machine__42121__auto____1.call(this,state_62209);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$entity_BANG__$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$entity_BANG__$_state_machine__42121__auto____0;
com$wsscode$pathom$core$entity_BANG__$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$entity_BANG__$_state_machine__42121__auto____1;
return com$wsscode$pathom$core$entity_BANG__$_state_machine__42121__auto__;
})()
})();
var state__42154__auto__ = (function (){var statearr_62230 = f__42153__auto__();
(statearr_62230[(6)] = c__42152__auto__);

return statearr_62230;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__42154__auto__);
}));

return c__42152__auto__;
} else {
var e = res__53579__auto__;
var missing = clojure.set.difference.cljs$core$IFn$_invoke$arity$2(cljs.core.set(attributes),cljs.core.set(cljs.core.keys(com.wsscode.pathom.core.elide_not_found(e))));
if(cljs.core.seq(missing)){
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2(["Entity attributes ",cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([missing], 0))," could not be realized"].join(''),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword("com.wsscode.pathom.core","entity","com.wsscode.pathom.core/entity",884706031),e,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),path,new cljs.core.Keyword("com.wsscode.pathom.core","missing-attributes","com.wsscode.pathom.core/missing-attributes",1114260849),missing], null));
} else {
}

return e;
}
});
/**
 * Like entity-attr. Raises an exception if the property can't be retrieved.
 */
com.wsscode.pathom.core.entity_attr_BANG_ = (function com$wsscode$pathom$core$entity_attr_BANG_(env,attr){
var res__53579__auto__ = com.wsscode.pathom.core.entity_BANG_(env,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [attr], null));
if(com.wsscode.async.async_cljs.chan_QMARK_(res__53579__auto__)){
var c__42152__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__42153__auto__ = (function (){var switch__42120__auto__ = (function (state_62245){
var state_val_62246 = (state_62245[(1)]);
if((state_val_62246 === (1))){
var state_62245__$1 = state_62245;
var statearr_62247_63701 = state_62245__$1;
(statearr_62247_63701[(2)] = null);

(statearr_62247_63701[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62246 === (2))){
var _ = (function (){var statearr_62248 = state_62245;
(statearr_62248[(4)] = cljs.core.cons((5),(state_62245[(4)])));

return statearr_62248;
})();
var state_62245__$1 = state_62245;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62245__$1,(6),res__53579__auto__);
} else {
if((state_val_62246 === (3))){
var inst_62243 = (state_62245[(2)]);
var state_62245__$1 = state_62245;
return cljs.core.async.impl.ioc_helpers.return_chan(state_62245__$1,inst_62243);
} else {
if((state_val_62246 === (4))){
var inst_62231 = (state_62245[(2)]);
var state_62245__$1 = state_62245;
var statearr_62250_63702 = state_62245__$1;
(statearr_62250_63702[(2)] = inst_62231);

(statearr_62250_63702[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62246 === (5))){
var _ = (function (){var statearr_62251 = state_62245;
(statearr_62251[(4)] = cljs.core.rest((state_62245[(4)])));

return statearr_62251;
})();
var state_62245__$1 = state_62245;
var ex62249 = (state_62245__$1[(2)]);
var statearr_62252_63703 = state_62245__$1;
(statearr_62252_63703[(5)] = ex62249);


var statearr_62253_63704 = state_62245__$1;
(statearr_62253_63704[(1)] = (4));

(statearr_62253_63704[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62246 === (6))){
var inst_62238 = (state_62245[(2)]);
var inst_62239 = com.wsscode.async.async_cljs.throw_err(inst_62238);
var inst_62240 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_62239,attr);
var _ = (function (){var statearr_62254 = state_62245;
(statearr_62254[(4)] = cljs.core.rest((state_62245[(4)])));

return statearr_62254;
})();
var state_62245__$1 = state_62245;
var statearr_62255_63705 = state_62245__$1;
(statearr_62255_63705[(2)] = inst_62240);

(statearr_62255_63705[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$entity_attr_BANG__$_state_machine__42121__auto__ = null;
var com$wsscode$pathom$core$entity_attr_BANG__$_state_machine__42121__auto____0 = (function (){
var statearr_62256 = [null,null,null,null,null,null,null];
(statearr_62256[(0)] = com$wsscode$pathom$core$entity_attr_BANG__$_state_machine__42121__auto__);

(statearr_62256[(1)] = (1));

return statearr_62256;
});
var com$wsscode$pathom$core$entity_attr_BANG__$_state_machine__42121__auto____1 = (function (state_62245){
while(true){
var ret_value__42122__auto__ = (function (){try{while(true){
var result__42123__auto__ = switch__42120__auto__(state_62245);
if(cljs.core.keyword_identical_QMARK_(result__42123__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__42123__auto__;
}
break;
}
}catch (e62257){var ex__42124__auto__ = e62257;
var statearr_62258_63706 = state_62245;
(statearr_62258_63706[(2)] = ex__42124__auto__);


if(cljs.core.seq((state_62245[(4)]))){
var statearr_62259_63707 = state_62245;
(statearr_62259_63707[(1)] = cljs.core.first((state_62245[(4)])));

} else {
throw ex__42124__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__42122__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__63708 = state_62245;
state_62245 = G__63708;
continue;
} else {
return ret_value__42122__auto__;
}
break;
}
});
com$wsscode$pathom$core$entity_attr_BANG__$_state_machine__42121__auto__ = function(state_62245){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$entity_attr_BANG__$_state_machine__42121__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$entity_attr_BANG__$_state_machine__42121__auto____1.call(this,state_62245);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$entity_attr_BANG__$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$entity_attr_BANG__$_state_machine__42121__auto____0;
com$wsscode$pathom$core$entity_attr_BANG__$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$entity_attr_BANG__$_state_machine__42121__auto____1;
return com$wsscode$pathom$core$entity_attr_BANG__$_state_machine__42121__auto__;
})()
})();
var state__42154__auto__ = (function (){var statearr_62260 = f__42153__auto__();
(statearr_62260[(6)] = c__42152__auto__);

return statearr_62260;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__42154__auto__);
}));

return c__42152__auto__;
} else {
var e = res__53579__auto__;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(e,attr);
}
});
/**
 * Helper to swap the current entity value.
 */
com.wsscode.pathom.core.swap_entity_BANG_ = (function com$wsscode$pathom$core$swap_entity_BANG_(var_args){
var args__5775__auto__ = [];
var len__5769__auto___63709 = arguments.length;
var i__5770__auto___63710 = (0);
while(true){
if((i__5770__auto___63710 < len__5769__auto___63709)){
args__5775__auto__.push((arguments[i__5770__auto___63710]));

var G__63711 = (i__5770__auto___63710 + (1));
i__5770__auto___63710 = G__63711;
continue;
} else {
}
break;
}

var argseq__5776__auto__ = ((((2) < args__5775__auto__.length))?(new cljs.core.IndexedSeq(args__5775__auto__.slice((2)),(0),null)):null);
return com.wsscode.pathom.core.swap_entity_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__5776__auto__);
});

(com.wsscode.pathom.core.swap_entity_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (env,fn,args){
var e = com.wsscode.pathom.core.raw_entity(env);
if(com.wsscode.pathom.core.atom_QMARK_(e)){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$4(cljs.core.swap_BANG_,e,fn,args);
} else {
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(fn,e,args);
}
}));

(com.wsscode.pathom.core.swap_entity_BANG_.cljs$lang$maxFixedArity = (2));

/** @this {Function} */
(com.wsscode.pathom.core.swap_entity_BANG_.cljs$lang$applyTo = (function (seq62261){
var G__62262 = cljs.core.first(seq62261);
var seq62261__$1 = cljs.core.next(seq62261);
var G__62263 = cljs.core.first(seq62261__$1);
var seq62261__$2 = cljs.core.next(seq62261__$1);
var self__5754__auto__ = this;
return self__5754__auto__.cljs$core$IFn$_invoke$arity$variadic(G__62262,G__62263,seq62261__$2);
}));

/**
 * Given an AST, find the child with a given key and run update against it.
 */
com.wsscode.pathom.core.update_child = (function com$wsscode$pathom$core$update_child(var_args){
var args__5775__auto__ = [];
var len__5769__auto___63712 = arguments.length;
var i__5770__auto___63713 = (0);
while(true){
if((i__5770__auto___63713 < len__5769__auto___63712)){
args__5775__auto__.push((arguments[i__5770__auto___63713]));

var G__63714 = (i__5770__auto___63713 + (1));
i__5770__auto___63713 = G__63714;
continue;
} else {
}
break;
}

var argseq__5776__auto__ = ((((2) < args__5775__auto__.length))?(new cljs.core.IndexedSeq(args__5775__auto__.slice((2)),(0),null)):null);
return com.wsscode.pathom.core.update_child.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__5776__auto__);
});

(com.wsscode.pathom.core.update_child.cljs$core$IFn$_invoke$arity$variadic = (function (ast,key,args){
var temp__5802__auto__ = (function (){var G__62267 = new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(ast);
var G__62267__$1 = (((G__62267 == null))?null:cljs.core.map_indexed.cljs$core$IFn$_invoke$arity$2(cljs.core.vector,G__62267));
var G__62267__$2 = (((G__62267__$1 == null))?null:cljs.core.filter.cljs$core$IFn$_invoke$arity$2(cljs.core.comp.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentHashSet.createAsIfByAssoc([key]),new cljs.core.Keyword(null,"key","key",-1516042587),cljs.core.second),G__62267__$1));
if((G__62267__$2 == null)){
return null;
} else {
return cljs.core.ffirst(G__62267__$2);
}
})();
if(cljs.core.truth_(temp__5802__auto__)){
var idx = temp__5802__auto__;
return cljs.core.apply.cljs$core$IFn$_invoke$arity$4(cljs.core.update_in,ast,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"children","children",-940561982),idx], null),args);
} else {
return ast;
}
}));

(com.wsscode.pathom.core.update_child.cljs$lang$maxFixedArity = (2));

/** @this {Function} */
(com.wsscode.pathom.core.update_child.cljs$lang$applyTo = (function (seq62264){
var G__62265 = cljs.core.first(seq62264);
var seq62264__$1 = cljs.core.next(seq62264);
var G__62266 = cljs.core.first(seq62264__$1);
var seq62264__$2 = cljs.core.next(seq62264__$1);
var self__5754__auto__ = this;
return self__5754__auto__.cljs$core$IFn$_invoke$arity$variadic(G__62265,G__62266,seq62264__$2);
}));

/**
 * Given an AST, find the child with a given key and run update against it.
 */
com.wsscode.pathom.core.update_recursive_depth = (function com$wsscode$pathom$core$update_recursive_depth(var_args){
var args__5775__auto__ = [];
var len__5769__auto___63717 = arguments.length;
var i__5770__auto___63718 = (0);
while(true){
if((i__5770__auto___63718 < len__5769__auto___63717)){
args__5775__auto__.push((arguments[i__5770__auto___63718]));

var G__63719 = (i__5770__auto___63718 + (1));
i__5770__auto___63718 = G__63719;
continue;
} else {
}
break;
}

var argseq__5776__auto__ = ((((2) < args__5775__auto__.length))?(new cljs.core.IndexedSeq(args__5775__auto__.slice((2)),(0),null)):null);
return com.wsscode.pathom.core.update_recursive_depth.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__5776__auto__);
});

(com.wsscode.pathom.core.update_recursive_depth.cljs$core$IFn$_invoke$arity$variadic = (function (ast,key,args){
var temp__5802__auto__ = (function (){var G__62272 = new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(ast);
var G__62272__$1 = (((G__62272 == null))?null:cljs.core.map_indexed.cljs$core$IFn$_invoke$arity$2(cljs.core.vector,G__62272));
var G__62272__$2 = (((G__62272__$1 == null))?null:cljs.core.filter.cljs$core$IFn$_invoke$arity$2(cljs.core.comp.cljs$core$IFn$_invoke$arity$2((function (p1__62268_SHARP_){
return ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(key,new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(p1__62268_SHARP_))) && (cljs.core.pos_int_QMARK_(new cljs.core.Keyword(null,"query","query",-1288509510).cljs$core$IFn$_invoke$arity$1(p1__62268_SHARP_))));
}),cljs.core.second),G__62272__$1));
if((G__62272__$2 == null)){
return null;
} else {
return cljs.core.ffirst(G__62272__$2);
}
})();
if(cljs.core.truth_(temp__5802__auto__)){
var idx = temp__5802__auto__;
return cljs.core.apply.cljs$core$IFn$_invoke$arity$4(cljs.core.update_in,ast,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"children","children",-940561982),idx,new cljs.core.Keyword(null,"query","query",-1288509510)], null),args);
} else {
return ast;
}
}));

(com.wsscode.pathom.core.update_recursive_depth.cljs$lang$maxFixedArity = (2));

/** @this {Function} */
(com.wsscode.pathom.core.update_recursive_depth.cljs$lang$applyTo = (function (seq62269){
var G__62270 = cljs.core.first(seq62269);
var seq62269__$1 = cljs.core.next(seq62269);
var G__62271 = cljs.core.first(seq62269__$1);
var seq62269__$2 = cljs.core.next(seq62269__$1);
var self__5754__auto__ = this;
return self__5754__auto__.cljs$core$IFn$_invoke$arity$variadic(G__62270,G__62271,seq62269__$2);
}));

com.wsscode.pathom.core.remove_query_wildcard = (function com$wsscode$pathom$core$remove_query_wildcard(query){
return cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.with_meta(cljs.core.PersistentVector.EMPTY,cljs.core.meta(query)),cljs.core.remove.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Symbol(null,"*","*",345799209,null),null], null), null)),query);
});
com.wsscode.pathom.core.default_union_path = (function com$wsscode$pathom$core$default_union_path(p__62274){
var map__62275 = p__62274;
var map__62275__$1 = cljs.core.__destructure_map(map__62275);
var env = map__62275__$1;
var query = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62275__$1,new cljs.core.Keyword(null,"query","query",-1288509510));
var e = com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$1(env);
var temp__5802__auto__ = (function (){var G__62276 = cljs.core.keys(query);
var G__62276__$1 = (((G__62276 == null))?null:cljs.core.filter.cljs$core$IFn$_invoke$arity$2((function (p1__62273_SHARP_){
return ((cljs.core.contains_QMARK_(e,p1__62273_SHARP_)) && (cljs.core.not((function (){var G__62277 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(e,p1__62273_SHARP_);
return (com.wsscode.pathom.core.break_values.cljs$core$IFn$_invoke$arity$1 ? com.wsscode.pathom.core.break_values.cljs$core$IFn$_invoke$arity$1(G__62277) : com.wsscode.pathom.core.break_values.call(null,G__62277));
})())));
}),G__62276));
if((G__62276__$1 == null)){
return null;
} else {
return cljs.core.first(G__62276__$1);
}
})();
if(cljs.core.truth_(temp__5802__auto__)){
var path = temp__5802__auto__;
return path;
} else {
return null;
}
});
com.wsscode.pathom.core.placeholder_key_QMARK_ = (function com$wsscode$pathom$core$placeholder_key_QMARK_(p__62278,k){
var map__62279 = p__62278;
var map__62279__$1 = cljs.core.__destructure_map(map__62279);
var placeholder_prefixes = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62279__$1,new cljs.core.Keyword("com.wsscode.pathom.core","placeholder-prefixes","com.wsscode.pathom.core/placeholder-prefixes",-1362240644));
var placeholder_prefixes__$1 = (function (){var or__5045__auto__ = placeholder_prefixes;
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
return new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 1, [">",null], null), null);
}
})();
return (((k instanceof cljs.core.Keyword)) && (cljs.core.contains_QMARK_(placeholder_prefixes__$1,cljs.core.namespace(k))));
});
cljs.spec.alpha.def_impl(new cljs.core.Symbol("com.wsscode.pathom.core","path-without-placeholders","com.wsscode.pathom.core/path-without-placeholders",817934882,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","fspec","cljs.spec.alpha/fspec",-1289128341,null),new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"env","env",-1815813235),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null),new cljs.core.Keyword(null,"req","req",-326448303),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)], null))),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)),cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"env","env",-1815813235),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null),new cljs.core.Keyword(null,"req","req",-326448303),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)], null))),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"env","env",-1815813235)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.alpha.map_spec_impl(cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"req-un","req-un",1074571008),new cljs.core.Keyword(null,"opt-un","opt-un",883442496),new cljs.core.Keyword(null,"gfn","gfn",791517474),new cljs.core.Keyword(null,"pred-exprs","pred-exprs",1792271395),new cljs.core.Keyword(null,"keys-pred","keys-pred",858984739),new cljs.core.Keyword(null,"opt-keys","opt-keys",1262688261),new cljs.core.Keyword(null,"req-specs","req-specs",553962313),new cljs.core.Keyword(null,"req","req",-326448303),new cljs.core.Keyword(null,"req-keys","req-keys",514319221),new cljs.core.Keyword(null,"opt-specs","opt-specs",-384905450),new cljs.core.Keyword(null,"pred-forms","pred-forms",172611832),new cljs.core.Keyword(null,"opt","opt",-794706369)],[null,null,null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (G__62285){
return cljs.core.map_QMARK_(G__62285);
}),(function (G__62285){
return cljs.core.contains_QMARK_(G__62285,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661));
})], null),(function (G__62285){
return ((cljs.core.map_QMARK_(G__62285)) && (cljs.core.contains_QMARK_(G__62285,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661))));
}),cljs.core.PersistentVector.EMPTY,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)], null),cljs.core.PersistentVector.EMPTY,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.list(new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"%","%",-950237169,null)], null),cljs.core.list(new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Symbol(null,"%","%",-950237169,null))),cljs.core.list(new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"%","%",-950237169,null)], null),cljs.core.list(new cljs.core.Symbol("cljs.core","contains?","cljs.core/contains?",-976526835,null),new cljs.core.Symbol(null,"%","%",-950237169,null),new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)))], null),null]))], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null),new cljs.core.Keyword(null,"req","req",-326448303),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)], null))], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"env","env",-1815813235),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null),new cljs.core.Keyword(null,"req","req",-326448303),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)], null))),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),null,null),new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),null,null,null));


/**
 * @type {function(!cljs.core.IMap): *}
 */
com.wsscode.pathom.core.path_without_placeholders = (function com$wsscode$pathom$core$path_without_placeholders(p__62286){
var map__62287 = p__62286;
var map__62287__$1 = cljs.core.__destructure_map(map__62287);
var env = map__62287__$1;
var path = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62287__$1,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661));
var map__62288 = cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"env","env",-1815813235),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null),new cljs.core.Keyword(null,"req","req",-326448303),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)], null))),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"env","env",-1815813235)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.alpha.map_spec_impl(cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"req-un","req-un",1074571008),new cljs.core.Keyword(null,"opt-un","opt-un",883442496),new cljs.core.Keyword(null,"gfn","gfn",791517474),new cljs.core.Keyword(null,"pred-exprs","pred-exprs",1792271395),new cljs.core.Keyword(null,"keys-pred","keys-pred",858984739),new cljs.core.Keyword(null,"opt-keys","opt-keys",1262688261),new cljs.core.Keyword(null,"req-specs","req-specs",553962313),new cljs.core.Keyword(null,"req","req",-326448303),new cljs.core.Keyword(null,"req-keys","req-keys",514319221),new cljs.core.Keyword(null,"opt-specs","opt-specs",-384905450),new cljs.core.Keyword(null,"pred-forms","pred-forms",172611832),new cljs.core.Keyword(null,"opt","opt",-794706369)],[null,null,null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (G__62289){
return cljs.core.map_QMARK_(G__62289);
}),(function (G__62289){
return cljs.core.contains_QMARK_(G__62289,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661));
})], null),(function (G__62289){
return ((cljs.core.map_QMARK_(G__62289)) && (cljs.core.contains_QMARK_(G__62289,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661))));
}),cljs.core.PersistentVector.EMPTY,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)], null),cljs.core.PersistentVector.EMPTY,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.list(new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"%","%",-950237169,null)], null),cljs.core.list(new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Symbol(null,"%","%",-950237169,null))),cljs.core.list(new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"%","%",-950237169,null)], null),cljs.core.list(new cljs.core.Symbol("cljs.core","contains?","cljs.core/contains?",-976526835,null),new cljs.core.Symbol(null,"%","%",-950237169,null),new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)))], null),null]))], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null),new cljs.core.Keyword(null,"req","req",-326448303),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)], null))], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"env","env",-1815813235),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null),new cljs.core.Keyword(null,"req","req",-326448303),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)], null))),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),null,null),new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),null,null,null);
var map__62288__$1 = cljs.core.__destructure_map(map__62288);
var argspec62281 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62288__$1,new cljs.core.Keyword(null,"args","args",1315556576));
var retspec62282 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62288__$1,new cljs.core.Keyword(null,"ret","ret",-468222814));
if(cljs.core.truth_(argspec62281)){
com.fulcrologic.guardrails.core.run_check(new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/wsscode/pathom/core.cljc:456 path-without-placeholders's",new cljs.core.Keyword(null,"tap>?","tap>?",212454486),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false,new cljs.core.Keyword(null,"expound-opts","expound-opts",623087481),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"args?","args?",-1963723548),true], null),argspec62281,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [env], null));
} else {
}

var f62284 = (function (p__62290){
var map__62291 = p__62290;
var map__62291__$1 = cljs.core.__destructure_map(map__62291);
var env__$1 = map__62291__$1;
var path__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62291__$1,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661));
return cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$1((function (p1__62280_SHARP_){
return com.wsscode.pathom.core.placeholder_key_QMARK_(env__$1,p1__62280_SHARP_);
})),path__$1);
});
var ret62283 = f62284(env);
if(cljs.core.truth_(retspec62282)){
com.fulcrologic.guardrails.core.run_check(new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/wsscode/pathom/core.cljc:456 path-without-placeholders's",new cljs.core.Keyword(null,"tap>?","tap>?",212454486),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false,new cljs.core.Keyword(null,"expound-opts","expound-opts",623087481),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"args?","args?",-1963723548),false], null),retspec62282,ret62283);
} else {
}

return ret62283;
});
/**
 * Find the closest parent key that's not a placeholder key.
 */
com.wsscode.pathom.core.find_closest_non_placeholder_parent_join_key = (function com$wsscode$pathom$core$find_closest_non_placeholder_parent_join_key(p__62293){
var map__62294 = p__62293;
var map__62294__$1 = cljs.core.__destructure_map(map__62294);
var env = map__62294__$1;
var path = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62294__$1,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661));
return cljs.core.first(cljs.core.remove.cljs$core$IFn$_invoke$arity$2((function (p1__62292_SHARP_){
return com.wsscode.pathom.core.placeholder_key_QMARK_(env,p1__62292_SHARP_);
}),cljs.core.drop.cljs$core$IFn$_invoke$arity$2((1),cljs.core.rseq((function (){var or__5045__auto__ = path;
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
return cljs.core.PersistentVector.EMPTY;
}
})()))));
});
/**
 * Runs a parser with current sub-query. When run with an `entity` argument, that entity is set as the new environment
 * value of `::entity`, and the subquery is parsed with that new environment. When run without an `entity` it
 * parses the current subquery in the context of whatever entity was already in `::entity` of the env.
 */
com.wsscode.pathom.core.join = (function com$wsscode$pathom$core$join(var_args){
var G__62296 = arguments.length;
switch (G__62296) {
case 2:
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$2 = (function (entity,p__62297){
var map__62298 = p__62297;
var map__62298__$1 = cljs.core.__destructure_map(map__62298);
var env = map__62298__$1;
var ast = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62298__$1,new cljs.core.Keyword(null,"ast","ast",-860334068));
var query = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62298__$1,new cljs.core.Keyword(null,"query","query",-1288509510));
var entity_key = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62298__$1,new cljs.core.Keyword("com.wsscode.pathom.core","entity-key","com.wsscode.pathom.core/entity-key",1494103249));
if(com.wsscode.pathom.core.atom_QMARK_(entity)){
var temp__5802__auto__ = new cljs.core.Keyword("com.wsscode.pathom.core","env","com.wsscode.pathom.core/env",602275378).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(entity));
if(cljs.core.truth_(temp__5802__auto__)){
var env_SINGLEQUOTE_ = temp__5802__auto__;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(entity,cljs.core.dissoc,new cljs.core.Keyword("com.wsscode.pathom.core","env","com.wsscode.pathom.core/env",602275378));

return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$1(cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(env_SINGLEQUOTE_,new cljs.core.Keyword(null,"ast","ast",-860334068),ast,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"query","query",-1288509510),query,entity_key,entity], 0)));
} else {
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$1(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(env,entity_key,entity));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword("com.wsscode.pathom.core","env","com.wsscode.pathom.core/env",602275378).cljs$core$IFn$_invoke$arity$1(entity))){
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$1(cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(cljs.core.get.cljs$core$IFn$_invoke$arity$2(entity,new cljs.core.Keyword("com.wsscode.pathom.core","env","com.wsscode.pathom.core/env",602275378)),new cljs.core.Keyword(null,"ast","ast",-860334068),ast,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"query","query",-1288509510),query,entity_key,cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(entity,new cljs.core.Keyword("com.wsscode.pathom.core","env","com.wsscode.pathom.core/env",602275378)))], 0)));
} else {
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$1(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(env,entity_key,cljs.core.atom.cljs$core$IFn$_invoke$arity$1(entity)));
}
}
}));

(com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$1 = (function (p__62299){
var map__62300 = p__62299;
var map__62300__$1 = cljs.core.__destructure_map(map__62300);
var env = map__62300__$1;
var parser = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62300__$1,new cljs.core.Keyword(null,"parser","parser",-1543495310));
var ast = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62300__$1,new cljs.core.Keyword(null,"ast","ast",-860334068));
var query = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62300__$1,new cljs.core.Keyword(null,"query","query",-1288509510));
var union_path = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62300__$1,new cljs.core.Keyword("com.wsscode.pathom.core","union-path","com.wsscode.pathom.core/union-path",-2083478095));
var parent_query = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62300__$1,new cljs.core.Keyword("com.wsscode.pathom.core","parent-query","com.wsscode.pathom.core/parent-query",1683392594));
var processing_sequence = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62300__$1,new cljs.core.Keyword("com.wsscode.pathom.core","processing-sequence","com.wsscode.pathom.core/processing-sequence",1929448637));
var e = com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$1(env);
var placeholder_QMARK_ = com.wsscode.pathom.core.placeholder_key_QMARK_(env,new cljs.core.Keyword(null,"dispatch-key","dispatch-key",733619510).cljs$core$IFn$_invoke$arity$1(ast));
var union_path__$1 = ((com.wsscode.pathom.core.union_children_QMARK_(ast))?(function (){var union_path__$1 = (function (){var or__5045__auto__ = union_path;
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
return com.wsscode.pathom.core.default_union_path;
}
})();
var path = ((cljs.core.fn_QMARK_(union_path__$1))?(union_path__$1.cljs$core$IFn$_invoke$arity$1 ? union_path__$1.cljs$core$IFn$_invoke$arity$1(env) : union_path__$1.call(null,env)):(((union_path__$1 instanceof cljs.core.Keyword))?cljs.core.get.cljs$core$IFn$_invoke$arity$2(com.wsscode.pathom.core.entity_BANG_(env,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [union_path__$1], null)),union_path__$1):null));
return path;
})():null);
var query__$1 = ((com.wsscode.pathom.core.union_children_QMARK_(ast))?(function (){var or__5045__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(query,union_path__$1);
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
return new cljs.core.Keyword("com.wsscode.pathom.core","blank-union","com.wsscode.pathom.core/blank-union",1901666614);
}
})():query);
var env_SINGLEQUOTE_ = (function (){var G__62301 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(env,new cljs.core.Keyword("com.wsscode.pathom.core","parent-query","com.wsscode.pathom.core/parent-query",1683392594),query__$1,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword("com.wsscode.pathom.core","parent-join-key","com.wsscode.pathom.core/parent-join-key",-289005491),new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(ast)], 0));
if((!(placeholder_QMARK_))){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__62301,new cljs.core.Keyword("com.wsscode.pathom.parser","waiting","com.wsscode.pathom.parser/waiting",-798662278),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword("com.wsscode.pathom.parser","key-watchers","com.wsscode.pathom.parser/key-watchers",-1670257404)], 0));
} else {
return G__62301;
}
})();
var env_SINGLEQUOTE___$1 = (cljs.core.truth_(processing_sequence)?(cljs.core.truth_((function (){var and__5043__auto__ = new cljs.core.Keyword("com.wsscode.pathom.core","stop-sequence?","com.wsscode.pathom.core/stop-sequence?",-1854144982).cljs$core$IFn$_invoke$arity$1(cljs.core.meta(processing_sequence));
if(cljs.core.truth_(and__5043__auto__)){
return (!(placeholder_QMARK_));
} else {
return and__5043__auto__;
}
})())?cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(env_SINGLEQUOTE_,new cljs.core.Keyword("com.wsscode.pathom.core","processing-sequence","com.wsscode.pathom.core/processing-sequence",1929448637)):cljs.core.update.cljs$core$IFn$_invoke$arity$6(env_SINGLEQUOTE_,new cljs.core.Keyword("com.wsscode.pathom.core","processing-sequence","com.wsscode.pathom.core/processing-sequence",1929448637),cljs.core.vary_meta,cljs.core.assoc,new cljs.core.Keyword("com.wsscode.pathom.core","stop-sequence?","com.wsscode.pathom.core/stop-sequence?",-1854144982),true)):env_SINGLEQUOTE_);
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("com.wsscode.pathom.core","blank-union","com.wsscode.pathom.core/blank-union",1901666614),query__$1)){
return cljs.core.PersistentArrayMap.EMPTY;
} else {
if((query__$1 == null)){
return e;
} else {
if(cljs.core.nat_int_QMARK_(query__$1)){
if((query__$1 === (0))){
return null;
} else {
var parent_query_SINGLEQUOTE_ = com.wsscode.pathom.core.ast__GT_query(com.wsscode.pathom.core.update_recursive_depth.cljs$core$IFn$_invoke$arity$variadic(com.wsscode.pathom.core.query__GT_ast(parent_query),new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(ast),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.dec], 0)));
var G__62302 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(env_SINGLEQUOTE___$1,new cljs.core.Keyword("com.wsscode.pathom.core","parent-query","com.wsscode.pathom.core/parent-query",1683392594),parent_query_SINGLEQUOTE_);
var G__62303 = com.wsscode.pathom.core.remove_query_wildcard(parent_query_SINGLEQUOTE_);
return (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(G__62302,G__62303) : parser.call(null,G__62302,G__62303));
}
} else {
if(cljs.core.truth_(cljs.core.some(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Symbol(null,"*","*",345799209,null),null], null), null),query__$1))){
var res__53579__auto__ = (function (){var G__62304 = env_SINGLEQUOTE___$1;
var G__62305 = com.wsscode.pathom.core.remove_query_wildcard(query__$1);
return (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(G__62304,G__62305) : parser.call(null,G__62304,G__62305));
})();
if(com.wsscode.async.async_cljs.chan_QMARK_(res__53579__auto__)){
var c__42152__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__42153__auto__ = (function (){var switch__42120__auto__ = (function (state_62321){
var state_val_62322 = (state_62321[(1)]);
if((state_val_62322 === (1))){
var state_62321__$1 = state_62321;
var statearr_62323_63742 = state_62321__$1;
(statearr_62323_63742[(2)] = null);

(statearr_62323_63742[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62322 === (2))){
var _ = (function (){var statearr_62324 = state_62321;
(statearr_62324[(4)] = cljs.core.cons((5),(state_62321[(4)])));

return statearr_62324;
})();
var state_62321__$1 = state_62321;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62321__$1,(6),res__53579__auto__);
} else {
if((state_val_62322 === (3))){
var inst_62319 = (state_62321[(2)]);
var state_62321__$1 = state_62321;
return cljs.core.async.impl.ioc_helpers.return_chan(state_62321__$1,inst_62319);
} else {
if((state_val_62322 === (4))){
var inst_62306 = (state_62321[(2)]);
var state_62321__$1 = state_62321;
var statearr_62326_63743 = state_62321__$1;
(statearr_62326_63743[(2)] = inst_62306);

(statearr_62326_63743[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62322 === (5))){
var _ = (function (){var statearr_62327 = state_62321;
(statearr_62327[(4)] = cljs.core.rest((state_62321[(4)])));

return statearr_62327;
})();
var state_62321__$1 = state_62321;
var ex62325 = (state_62321__$1[(2)]);
var statearr_62328_63744 = state_62321__$1;
(statearr_62328_63744[(5)] = ex62325);


var statearr_62329_63745 = state_62321__$1;
(statearr_62329_63745[(1)] = (4));

(statearr_62329_63745[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62322 === (6))){
var inst_62313 = (state_62321[(2)]);
var inst_62314 = com.wsscode.async.async_cljs.throw_err(inst_62313);
var inst_62315 = com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$1(env_SINGLEQUOTE___$1);
var inst_62316 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([inst_62315,inst_62314], 0));
var _ = (function (){var statearr_62330 = state_62321;
(statearr_62330[(4)] = cljs.core.rest((state_62321[(4)])));

return statearr_62330;
})();
var state_62321__$1 = state_62321;
var statearr_62331_63746 = state_62321__$1;
(statearr_62331_63746[(2)] = inst_62316);

(statearr_62331_63746[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$state_machine__42121__auto__ = null;
var com$wsscode$pathom$core$state_machine__42121__auto____0 = (function (){
var statearr_62332 = [null,null,null,null,null,null,null];
(statearr_62332[(0)] = com$wsscode$pathom$core$state_machine__42121__auto__);

(statearr_62332[(1)] = (1));

return statearr_62332;
});
var com$wsscode$pathom$core$state_machine__42121__auto____1 = (function (state_62321){
while(true){
var ret_value__42122__auto__ = (function (){try{while(true){
var result__42123__auto__ = switch__42120__auto__(state_62321);
if(cljs.core.keyword_identical_QMARK_(result__42123__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__42123__auto__;
}
break;
}
}catch (e62333){var ex__42124__auto__ = e62333;
var statearr_62334_63747 = state_62321;
(statearr_62334_63747[(2)] = ex__42124__auto__);


if(cljs.core.seq((state_62321[(4)]))){
var statearr_62335_63748 = state_62321;
(statearr_62335_63748[(1)] = cljs.core.first((state_62321[(4)])));

} else {
throw ex__42124__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__42122__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__63749 = state_62321;
state_62321 = G__63749;
continue;
} else {
return ret_value__42122__auto__;
}
break;
}
});
com$wsscode$pathom$core$state_machine__42121__auto__ = function(state_62321){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$state_machine__42121__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$state_machine__42121__auto____1.call(this,state_62321);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$state_machine__42121__auto____0;
com$wsscode$pathom$core$state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$state_machine__42121__auto____1;
return com$wsscode$pathom$core$state_machine__42121__auto__;
})()
})();
var state__42154__auto__ = (function (){var statearr_62336 = f__42153__auto__();
(statearr_62336[(6)] = c__42152__auto__);

return statearr_62336;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__42154__auto__);
}));

return c__42152__auto__;
} else {
var computed_e = res__53579__auto__;
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$1(env_SINGLEQUOTE___$1),computed_e], 0));
}
} else {
return (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(env_SINGLEQUOTE___$1,query__$1) : parser.call(null,env_SINGLEQUOTE___$1,query__$1));

}
}
}
}
}));

(com.wsscode.pathom.core.join.cljs$lang$maxFixedArity = 2);

com.wsscode.pathom.core.join_seq_pipeline_fn = (function com$wsscode$pathom$core$join_seq_pipeline_fn(env,entity_path_cache,join_item){
return (function com$wsscode$pathom$core$join_seq_pipeline_fn_$_join_seq_pipeline(p__62337,res_ch){
var vec__62338 = p__62337;
var ent = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__62338,(0),null);
var i = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__62338,(1),null);
var c__42152__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__42153__auto__ = (function (){var switch__42120__auto__ = (function (state_62362){
var state_val_62363 = (state_62362[(1)]);
if((state_val_62363 === (1))){
var inst_62350 = (state_62362[(7)]);
var inst_62342 = (i + (1));
var inst_62343 = cljs.core.update.cljs$core$IFn$_invoke$arity$4(env,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),cljs.core.conj,inst_62342);
var inst_62344 = cljs.core.__destructure_map(inst_62343);
var inst_62345 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_62344,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661));
var inst_62346 = cljs.core.deref(entity_path_cache);
var inst_62347 = cljs.core.PersistentHashMap.EMPTY;
var inst_62348 = cljs.core.get.cljs$core$IFn$_invoke$arity$3(inst_62346,inst_62345,inst_62347);
var inst_62349 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([inst_62348,ent], 0));
var inst_62350__$1 = (join_item.cljs$core$IFn$_invoke$arity$2 ? join_item.cljs$core$IFn$_invoke$arity$2(inst_62344,inst_62349) : join_item.call(null,inst_62344,inst_62349));
var inst_62351 = com.wsscode.async.async_cljs.chan_QMARK_(inst_62350__$1);
var state_62362__$1 = (function (){var statearr_62364 = state_62362;
(statearr_62364[(7)] = inst_62350__$1);

return statearr_62364;
})();
if(inst_62351){
var statearr_62365_63751 = state_62362__$1;
(statearr_62365_63751[(1)] = (2));

} else {
var statearr_62366_63752 = state_62362__$1;
(statearr_62366_63752[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62363 === (2))){
var inst_62350 = (state_62362[(7)]);
var state_62362__$1 = state_62362;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62362__$1,(5),inst_62350);
} else {
if((state_val_62363 === (3))){
var inst_62350 = (state_62362[(7)]);
var state_62362__$1 = state_62362;
var statearr_62367_63753 = state_62362__$1;
(statearr_62367_63753[(2)] = inst_62350);

(statearr_62367_63753[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62363 === (4))){
var inst_62357 = (state_62362[(2)]);
var state_62362__$1 = state_62362;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_62362__$1,(6),res_ch,inst_62357);
} else {
if((state_val_62363 === (5))){
var inst_62354 = (state_62362[(2)]);
var state_62362__$1 = state_62362;
var statearr_62368_63754 = state_62362__$1;
(statearr_62368_63754[(2)] = inst_62354);

(statearr_62368_63754[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62363 === (6))){
var inst_62359 = (state_62362[(2)]);
var inst_62360 = cljs.core.async.close_BANG_(res_ch);
var state_62362__$1 = (function (){var statearr_62369 = state_62362;
(statearr_62369[(8)] = inst_62359);

return statearr_62369;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_62362__$1,inst_62360);
} else {
return null;
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$join_seq_pipeline_fn_$_join_seq_pipeline_$_state_machine__42121__auto__ = null;
var com$wsscode$pathom$core$join_seq_pipeline_fn_$_join_seq_pipeline_$_state_machine__42121__auto____0 = (function (){
var statearr_62370 = [null,null,null,null,null,null,null,null,null];
(statearr_62370[(0)] = com$wsscode$pathom$core$join_seq_pipeline_fn_$_join_seq_pipeline_$_state_machine__42121__auto__);

(statearr_62370[(1)] = (1));

return statearr_62370;
});
var com$wsscode$pathom$core$join_seq_pipeline_fn_$_join_seq_pipeline_$_state_machine__42121__auto____1 = (function (state_62362){
while(true){
var ret_value__42122__auto__ = (function (){try{while(true){
var result__42123__auto__ = switch__42120__auto__(state_62362);
if(cljs.core.keyword_identical_QMARK_(result__42123__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__42123__auto__;
}
break;
}
}catch (e62371){var ex__42124__auto__ = e62371;
var statearr_62372_63755 = state_62362;
(statearr_62372_63755[(2)] = ex__42124__auto__);


if(cljs.core.seq((state_62362[(4)]))){
var statearr_62373_63756 = state_62362;
(statearr_62373_63756[(1)] = cljs.core.first((state_62362[(4)])));

} else {
throw ex__42124__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__42122__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__63757 = state_62362;
state_62362 = G__63757;
continue;
} else {
return ret_value__42122__auto__;
}
break;
}
});
com$wsscode$pathom$core$join_seq_pipeline_fn_$_join_seq_pipeline_$_state_machine__42121__auto__ = function(state_62362){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$join_seq_pipeline_fn_$_join_seq_pipeline_$_state_machine__42121__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$join_seq_pipeline_fn_$_join_seq_pipeline_$_state_machine__42121__auto____1.call(this,state_62362);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$join_seq_pipeline_fn_$_join_seq_pipeline_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$join_seq_pipeline_fn_$_join_seq_pipeline_$_state_machine__42121__auto____0;
com$wsscode$pathom$core$join_seq_pipeline_fn_$_join_seq_pipeline_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$join_seq_pipeline_fn_$_join_seq_pipeline_$_state_machine__42121__auto____1;
return com$wsscode$pathom$core$join_seq_pipeline_fn_$_join_seq_pipeline_$_state_machine__42121__auto__;
})()
})();
var state__42154__auto__ = (function (){var statearr_62374 = f__42153__auto__();
(statearr_62374[(6)] = c__42152__auto__);

return statearr_62374;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__42154__auto__);
}));

return c__42152__auto__;
});
});
com.wsscode.pathom.core.join_seq_parallel = (function com$wsscode$pathom$core$join_seq_parallel(p__62376,coll){
var map__62377 = p__62376;
var map__62377__$1 = cljs.core.__destructure_map(map__62377);
var env = map__62377__$1;
var query = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62377__$1,new cljs.core.Keyword(null,"query","query",-1288509510));
var entity_path_cache = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62377__$1,new cljs.core.Keyword("com.wsscode.pathom.core","entity-path-cache","com.wsscode.pathom.core/entity-path-cache",-1017384397));
var parent_query = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62377__$1,new cljs.core.Keyword("com.wsscode.pathom.core","parent-query","com.wsscode.pathom.core/parent-query",1683392594));
var query_SINGLEQUOTE_ = ((cljs.core.nat_int_QMARK_(query))?parent_query:query);
if(((cljs.core.seq(coll)) && (((cljs.core.vector_QMARK_(query)) || (((cljs.core.pos_int_QMARK_(query)) || (cljs.core.map_QMARK_(query)))))))){
var ch__53544__auto__ = cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();
var c__42152__auto___63759 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__42153__auto__ = (function (){var switch__42120__auto__ = (function (state_62512){
var state_val_62513 = (state_62512[(1)]);
if((state_val_62513 === (7))){
var inst_62458 = (state_62512[(7)]);
var inst_62457 = (state_62512[(8)]);
var inst_62464 = (state_62512[(9)]);
var inst_62453 = edn_query_language.core.query__GT_ast(query_SINGLEQUOTE_);
var inst_62454 = (function (){var ast = inst_62453;
return (function (p1__62375_SHARP_){
return cljs.core.not(new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(p1__62375_SHARP_));
});
})();
var inst_62455 = new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(inst_62453);
var inst_62456 = cljs.core.every_QMARK_(inst_62454,inst_62455);
var inst_62457__$1 = (function (){var ast = inst_62453;
var check_ast_opt_QMARK_ = inst_62456;
return (function com$wsscode$pathom$core$join_seq_parallel_$_join_item(env__$1,entity){
var or__5045__auto__ = (function (){var and__5043__auto__ = check_ast_opt_QMARK_;
if(and__5043__auto__){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ent,p__62514){
var map__62515 = p__62514;
var map__62515__$1 = cljs.core.__destructure_map(map__62515);
var key = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62515__$1,new cljs.core.Keyword(null,"key","key",-1516042587));
var params = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62515__$1,new cljs.core.Keyword(null,"params","params",710516235));
var temp__5802__auto__ = cljs.core.find(entity,key);
if(cljs.core.truth_(temp__5802__auto__)){
var vec__62516 = temp__5802__auto__;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__62516,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__62516,(1),null);
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(ent,cljs.core.get.cljs$core$IFn$_invoke$arity$3(params,new cljs.core.Keyword("pathom","as","pathom/as",-2134138450),key),v);
} else {
return cljs.core.reduced(null);
}
}),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(ast));
} else {
return and__5043__auto__;
}
})();
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$2(entity,env__$1);
}
});
})();
var inst_62458__$1 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(env,new cljs.core.Keyword("com.wsscode.pathom.core","processing-sequence","com.wsscode.pathom.core/processing-sequence",1929448637),coll);
var inst_62459 = coll;
var inst_62460 = cljs.core.seq(inst_62459);
var inst_62461 = cljs.core.first(inst_62460);
var inst_62462 = cljs.core.next(inst_62460);
var inst_62463 = cljs.core.update.cljs$core$IFn$_invoke$arity$4(inst_62458__$1,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),cljs.core.conj,(0));
var inst_62464__$1 = inst_62457__$1(inst_62463,inst_62461);
var inst_62465 = com.wsscode.async.async_cljs.chan_QMARK_(inst_62464__$1);
var state_62512__$1 = (function (){var statearr_62519 = state_62512;
(statearr_62519[(8)] = inst_62457__$1);

(statearr_62519[(7)] = inst_62458__$1);

(statearr_62519[(10)] = inst_62462);

(statearr_62519[(9)] = inst_62464__$1);

return statearr_62519;
})();
if(inst_62465){
var statearr_62520_63760 = state_62512__$1;
(statearr_62520_63760[(1)] = (21));

} else {
var statearr_62521_63761 = state_62512__$1;
(statearr_62521_63761[(1)] = (22));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62513 === (20))){
var inst_62392 = (state_62512[(11)]);
var inst_62441 = (state_62512[(2)]);
var inst_62442 = [new cljs.core.Keyword("com.wsscode.pathom.trace","event","com.wsscode.pathom.trace/event",1287398983),new cljs.core.Keyword("com.wsscode.pathom.trace","style","com.wsscode.pathom.trace/style",496837297)];
var inst_62443 = [new cljs.core.Keyword(null,"fill","fill",883462889),new cljs.core.Keyword(null,"opacity","opacity",397153780)];
var inst_62444 = ["#e0e3a4","0.8"];
var inst_62445 = cljs.core.PersistentHashMap.fromArrays(inst_62443,inst_62444);
var inst_62446 = [new cljs.core.Keyword("com.wsscode.pathom.core","parallel-sequence-loop","com.wsscode.pathom.core/parallel-sequence-loop",1692546245),inst_62445];
var inst_62447 = cljs.core.PersistentHashMap.fromArrays(inst_62442,inst_62446);
var inst_62448 = com.wsscode.pathom.trace.trace_leave.cljs$core$IFn$_invoke$arity$3(env,inst_62392,inst_62447);
var state_62512__$1 = (function (){var statearr_62522 = state_62512;
(statearr_62522[(12)] = inst_62448);

return statearr_62522;
})();
var statearr_62523_63762 = state_62512__$1;
(statearr_62523_63762[(2)] = inst_62441);

(statearr_62523_63762[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62513 === (27))){
var inst_62483 = (state_62512[(2)]);
var state_62512__$1 = state_62512;
var statearr_62524_63763 = state_62512__$1;
(statearr_62524_63763[(2)] = inst_62483);

(statearr_62524_63763[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62513 === (1))){
var state_62512__$1 = state_62512;
var statearr_62525_63764 = state_62512__$1;
(statearr_62525_63764[(2)] = null);

(statearr_62525_63764[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62513 === (24))){
var inst_62468 = (state_62512[(2)]);
var inst_62469 = com.wsscode.async.async_cljs.throw_err(inst_62468);
var state_62512__$1 = state_62512;
var statearr_62526_63765 = state_62512__$1;
(statearr_62526_63765[(2)] = inst_62469);

(statearr_62526_63765[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62513 === (4))){
var inst_62378 = (state_62512[(2)]);
var state_62512__$1 = state_62512;
var statearr_62527_63766 = state_62512__$1;
(statearr_62527_63766[(2)] = inst_62378);

(statearr_62527_63766[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62513 === (15))){
var inst_62426 = (state_62512[(2)]);
var state_62512__$1 = state_62512;
var statearr_62528_63767 = state_62512__$1;
(statearr_62528_63767[(2)] = inst_62426);

(statearr_62528_63767[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62513 === (21))){
var inst_62464 = (state_62512[(9)]);
var state_62512__$1 = state_62512;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62512__$1,(24),inst_62464);
} else {
if((state_val_62513 === (31))){
var inst_62481 = (state_62512[(2)]);
var state_62512__$1 = state_62512;
var statearr_62529_63768 = state_62512__$1;
(statearr_62529_63768[(2)] = inst_62481);

(statearr_62529_63768[(1)] = (27));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62513 === (32))){
var inst_62498 = (state_62512[(2)]);
var state_62512__$1 = state_62512;
var statearr_62530_63769 = state_62512__$1;
(statearr_62530_63769[(2)] = inst_62498);

(statearr_62530_63769[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62513 === (33))){
var inst_62506 = cljs.core.async.close_BANG_(ch__53544__auto__);
var state_62512__$1 = state_62512;
var statearr_62531_63770 = state_62512__$1;
(statearr_62531_63770[(2)] = inst_62506);

(statearr_62531_63770[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62513 === (13))){
var inst_62407 = (state_62512[(13)]);
var inst_62416 = com.wsscode.async.async_cljs.promise__GT_chan(inst_62407);
var state_62512__$1 = state_62512;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62512__$1,(16),inst_62416);
} else {
if((state_val_62513 === (22))){
var inst_62464 = (state_62512[(9)]);
var inst_62471 = com.wsscode.async.async_cljs.promise_QMARK_(inst_62464);
var state_62512__$1 = state_62512;
if(cljs.core.truth_(inst_62471)){
var statearr_62532_63771 = state_62512__$1;
(statearr_62532_63771[(1)] = (25));

} else {
var statearr_62533_63772 = state_62512__$1;
(statearr_62533_63772[(1)] = (26));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62513 === (29))){
var inst_62464 = (state_62512[(9)]);
var state_62512__$1 = state_62512;
var statearr_62534_63773 = state_62512__$1;
(statearr_62534_63773[(2)] = inst_62464);

(statearr_62534_63773[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62513 === (6))){
var inst_62392 = (state_62512[(11)]);
var inst_62401 = (state_62512[(14)]);
var inst_62400 = (state_62512[(15)]);
var inst_62407 = (state_62512[(13)]);
var inst_62386 = [new cljs.core.Keyword("com.wsscode.pathom.trace","event","com.wsscode.pathom.trace/event",1287398983),new cljs.core.Keyword("com.wsscode.pathom.trace","style","com.wsscode.pathom.trace/style",496837297)];
var inst_62387 = [new cljs.core.Keyword(null,"fill","fill",883462889),new cljs.core.Keyword(null,"opacity","opacity",397153780)];
var inst_62388 = ["#e0e3a4","0.8"];
var inst_62389 = cljs.core.PersistentHashMap.fromArrays(inst_62387,inst_62388);
var inst_62390 = [new cljs.core.Keyword("com.wsscode.pathom.core","parallel-sequence-loop","com.wsscode.pathom.core/parallel-sequence-loop",1692546245),inst_62389];
var inst_62391 = cljs.core.PersistentHashMap.fromArrays(inst_62386,inst_62390);
var inst_62392__$1 = com.wsscode.pathom.trace.trace_enter.cljs$core$IFn$_invoke$arity$2(env,inst_62391);
var inst_62396 = edn_query_language.core.query__GT_ast(query_SINGLEQUOTE_);
var inst_62397 = (function (){var trace_id__54379__auto__ = inst_62392__$1;
var ast = inst_62396;
return (function (p1__62375_SHARP_){
return cljs.core.not(new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(p1__62375_SHARP_));
});
})();
var inst_62398 = new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(inst_62396);
var inst_62399 = cljs.core.every_QMARK_(inst_62397,inst_62398);
var inst_62400__$1 = (function (){var trace_id__54379__auto__ = inst_62392__$1;
var ast = inst_62396;
var check_ast_opt_QMARK_ = inst_62399;
return (function com$wsscode$pathom$core$join_seq_parallel_$_join_item(env__$1,entity){
var or__5045__auto__ = (function (){var and__5043__auto__ = check_ast_opt_QMARK_;
if(and__5043__auto__){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ent,p__62535){
var map__62536 = p__62535;
var map__62536__$1 = cljs.core.__destructure_map(map__62536);
var key = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62536__$1,new cljs.core.Keyword(null,"key","key",-1516042587));
var params = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62536__$1,new cljs.core.Keyword(null,"params","params",710516235));
var temp__5802__auto__ = cljs.core.find(entity,key);
if(cljs.core.truth_(temp__5802__auto__)){
var vec__62537 = temp__5802__auto__;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__62537,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__62537,(1),null);
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(ent,cljs.core.get.cljs$core$IFn$_invoke$arity$3(params,new cljs.core.Keyword("pathom","as","pathom/as",-2134138450),key),v);
} else {
return cljs.core.reduced(null);
}
}),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(ast));
} else {
return and__5043__auto__;
}
})();
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$2(entity,env__$1);
}
});
})();
var inst_62401__$1 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(env,new cljs.core.Keyword("com.wsscode.pathom.core","processing-sequence","com.wsscode.pathom.core/processing-sequence",1929448637),coll);
var inst_62402 = coll;
var inst_62403 = cljs.core.seq(inst_62402);
var inst_62404 = cljs.core.first(inst_62403);
var inst_62405 = cljs.core.next(inst_62403);
var inst_62406 = cljs.core.update.cljs$core$IFn$_invoke$arity$4(inst_62401__$1,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),cljs.core.conj,(0));
var inst_62407__$1 = inst_62400__$1(inst_62406,inst_62404);
var inst_62408 = com.wsscode.async.async_cljs.chan_QMARK_(inst_62407__$1);
var state_62512__$1 = (function (){var statearr_62540 = state_62512;
(statearr_62540[(11)] = inst_62392__$1);

(statearr_62540[(15)] = inst_62400__$1);

(statearr_62540[(14)] = inst_62401__$1);

(statearr_62540[(16)] = inst_62405);

(statearr_62540[(13)] = inst_62407__$1);

return statearr_62540;
})();
if(inst_62408){
var statearr_62541_63774 = state_62512__$1;
(statearr_62541_63774[(1)] = (9));

} else {
var statearr_62542_63775 = state_62512__$1;
(statearr_62542_63775[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62513 === (28))){
var inst_62475 = (state_62512[(2)]);
var inst_62476 = com.wsscode.async.async_cljs.consumer_pair(inst_62475);
var state_62512__$1 = state_62512;
var statearr_62543_63776 = state_62512__$1;
(statearr_62543_63776[(2)] = inst_62476);

(statearr_62543_63776[(1)] = (27));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62513 === (25))){
var inst_62464 = (state_62512[(9)]);
var inst_62473 = com.wsscode.async.async_cljs.promise__GT_chan(inst_62464);
var state_62512__$1 = state_62512;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62512__$1,(28),inst_62473);
} else {
if((state_val_62513 === (34))){
var inst_62503 = (state_62512[(17)]);
var inst_62508 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(ch__53544__auto__,inst_62503);
var state_62512__$1 = state_62512;
var statearr_62544_63777 = state_62512__$1;
(statearr_62544_63777[(2)] = inst_62508);

(statearr_62544_63777[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62513 === (17))){
var inst_62407 = (state_62512[(13)]);
var state_62512__$1 = state_62512;
var statearr_62545_63778 = state_62512__$1;
(statearr_62545_63778[(2)] = inst_62407);

(statearr_62545_63778[(1)] = (19));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62513 === (3))){
var inst_62503 = (state_62512[(17)]);
var inst_62503__$1 = (state_62512[(2)]);
var inst_62504 = (inst_62503__$1 == null);
var state_62512__$1 = (function (){var statearr_62546 = state_62512;
(statearr_62546[(17)] = inst_62503__$1);

return statearr_62546;
})();
if(cljs.core.truth_(inst_62504)){
var statearr_62547_63779 = state_62512__$1;
(statearr_62547_63779[(1)] = (33));

} else {
var statearr_62548_63780 = state_62512__$1;
(statearr_62548_63780[(1)] = (34));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62513 === (12))){
var inst_62411 = (state_62512[(2)]);
var inst_62412 = com.wsscode.async.async_cljs.throw_err(inst_62411);
var state_62512__$1 = state_62512;
var statearr_62549_63781 = state_62512__$1;
(statearr_62549_63781[(2)] = inst_62412);

(statearr_62549_63781[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62513 === (2))){
var _ = (function (){var statearr_62550 = state_62512;
(statearr_62550[(4)] = cljs.core.cons((5),(state_62512[(4)])));

return statearr_62550;
})();
var inst_62384 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(env,new cljs.core.Keyword("com.wsscode.pathom.trace","trace*","com.wsscode.pathom.trace/trace*",-80191782));
var state_62512__$1 = state_62512;
if(cljs.core.truth_(inst_62384)){
var statearr_62551_63782 = state_62512__$1;
(statearr_62551_63782[(1)] = (6));

} else {
var statearr_62552_63783 = state_62512__$1;
(statearr_62552_63783[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62513 === (23))){
var inst_62462 = (state_62512[(10)]);
var inst_62458 = (state_62512[(7)]);
var inst_62457 = (state_62512[(8)]);
var inst_62485 = (state_62512[(2)]);
var inst_62486 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((10));
var inst_62487 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((10));
var inst_62488 = cljs.core.range.cljs$core$IFn$_invoke$arity$0();
var inst_62489 = cljs.core.map.cljs$core$IFn$_invoke$arity$3(cljs.core.vector,inst_62462,inst_62488);
var inst_62490 = cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2(inst_62486,inst_62489);
var inst_62491 = com.wsscode.pathom.core.join_seq_pipeline_fn(inst_62458,entity_path_cache,inst_62457);
var inst_62492 = cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((10),inst_62487,inst_62491,inst_62486);
var inst_62493 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_62494 = [inst_62485];
var inst_62495 = (new cljs.core.PersistentVector(null,1,(5),inst_62493,inst_62494,null));
var inst_62496 = cljs.core.async.into(inst_62495,inst_62487);
var state_62512__$1 = (function (){var statearr_62553 = state_62512;
(statearr_62553[(18)] = inst_62490);

(statearr_62553[(19)] = inst_62492);

return statearr_62553;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62512__$1,(32),inst_62496);
} else {
if((state_val_62513 === (35))){
var inst_62510 = (state_62512[(2)]);
var state_62512__$1 = state_62512;
return cljs.core.async.impl.ioc_helpers.return_chan(state_62512__$1,inst_62510);
} else {
if((state_val_62513 === (19))){
var inst_62424 = (state_62512[(2)]);
var state_62512__$1 = state_62512;
var statearr_62554_63785 = state_62512__$1;
(statearr_62554_63785[(2)] = inst_62424);

(statearr_62554_63785[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62513 === (11))){
var inst_62405 = (state_62512[(16)]);
var inst_62401 = (state_62512[(14)]);
var inst_62400 = (state_62512[(15)]);
var inst_62428 = (state_62512[(2)]);
var inst_62429 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((10));
var inst_62430 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((10));
var inst_62431 = cljs.core.range.cljs$core$IFn$_invoke$arity$0();
var inst_62432 = cljs.core.map.cljs$core$IFn$_invoke$arity$3(cljs.core.vector,inst_62405,inst_62431);
var inst_62433 = cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2(inst_62429,inst_62432);
var inst_62434 = com.wsscode.pathom.core.join_seq_pipeline_fn(inst_62401,entity_path_cache,inst_62400);
var inst_62435 = cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((10),inst_62430,inst_62434,inst_62429);
var inst_62436 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_62437 = [inst_62428];
var inst_62438 = (new cljs.core.PersistentVector(null,1,(5),inst_62436,inst_62437,null));
var inst_62439 = cljs.core.async.into(inst_62438,inst_62430);
var state_62512__$1 = (function (){var statearr_62556 = state_62512;
(statearr_62556[(20)] = inst_62433);

(statearr_62556[(21)] = inst_62435);

return statearr_62556;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62512__$1,(20),inst_62439);
} else {
if((state_val_62513 === (9))){
var inst_62407 = (state_62512[(13)]);
var state_62512__$1 = state_62512;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62512__$1,(12),inst_62407);
} else {
if((state_val_62513 === (5))){
var _ = (function (){var statearr_62557 = state_62512;
(statearr_62557[(4)] = cljs.core.rest((state_62512[(4)])));

return statearr_62557;
})();
var state_62512__$1 = state_62512;
var ex62555 = (state_62512__$1[(2)]);
var statearr_62558_63788 = state_62512__$1;
(statearr_62558_63788[(5)] = ex62555);


var statearr_62559_63789 = state_62512__$1;
(statearr_62559_63789[(1)] = (4));

(statearr_62559_63789[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62513 === (14))){
var state_62512__$1 = state_62512;
var statearr_62560_63790 = state_62512__$1;
(statearr_62560_63790[(1)] = (17));



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62513 === (26))){
var state_62512__$1 = state_62512;
var statearr_62562_63791 = state_62512__$1;
(statearr_62562_63791[(1)] = (29));



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62513 === (16))){
var inst_62418 = (state_62512[(2)]);
var inst_62419 = com.wsscode.async.async_cljs.consumer_pair(inst_62418);
var state_62512__$1 = state_62512;
var statearr_62564_63792 = state_62512__$1;
(statearr_62564_63792[(2)] = inst_62419);

(statearr_62564_63792[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62513 === (30))){
var state_62512__$1 = state_62512;
var statearr_62565_63794 = state_62512__$1;
(statearr_62565_63794[(2)] = null);

(statearr_62565_63794[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62513 === (10))){
var inst_62407 = (state_62512[(13)]);
var inst_62414 = com.wsscode.async.async_cljs.promise_QMARK_(inst_62407);
var state_62512__$1 = state_62512;
if(cljs.core.truth_(inst_62414)){
var statearr_62566_63795 = state_62512__$1;
(statearr_62566_63795[(1)] = (13));

} else {
var statearr_62567_63796 = state_62512__$1;
(statearr_62567_63796[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62513 === (18))){
var state_62512__$1 = state_62512;
var statearr_62568_63797 = state_62512__$1;
(statearr_62568_63797[(2)] = null);

(statearr_62568_63797[(1)] = (19));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62513 === (8))){
var inst_62500 = (state_62512[(2)]);
var _ = (function (){var statearr_62569 = state_62512;
(statearr_62569[(4)] = cljs.core.rest((state_62512[(4)])));

return statearr_62569;
})();
var state_62512__$1 = state_62512;
var statearr_62570_63798 = state_62512__$1;
(statearr_62570_63798[(2)] = inst_62500);

(statearr_62570_63798[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$join_seq_parallel_$_state_machine__42121__auto__ = null;
var com$wsscode$pathom$core$join_seq_parallel_$_state_machine__42121__auto____0 = (function (){
var statearr_62571 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_62571[(0)] = com$wsscode$pathom$core$join_seq_parallel_$_state_machine__42121__auto__);

(statearr_62571[(1)] = (1));

return statearr_62571;
});
var com$wsscode$pathom$core$join_seq_parallel_$_state_machine__42121__auto____1 = (function (state_62512){
while(true){
var ret_value__42122__auto__ = (function (){try{while(true){
var result__42123__auto__ = switch__42120__auto__(state_62512);
if(cljs.core.keyword_identical_QMARK_(result__42123__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__42123__auto__;
}
break;
}
}catch (e62572){var ex__42124__auto__ = e62572;
var statearr_62573_63800 = state_62512;
(statearr_62573_63800[(2)] = ex__42124__auto__);


if(cljs.core.seq((state_62512[(4)]))){
var statearr_62574_63801 = state_62512;
(statearr_62574_63801[(1)] = cljs.core.first((state_62512[(4)])));

} else {
throw ex__42124__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__42122__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__63802 = state_62512;
state_62512 = G__63802;
continue;
} else {
return ret_value__42122__auto__;
}
break;
}
});
com$wsscode$pathom$core$join_seq_parallel_$_state_machine__42121__auto__ = function(state_62512){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$join_seq_parallel_$_state_machine__42121__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$join_seq_parallel_$_state_machine__42121__auto____1.call(this,state_62512);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$join_seq_parallel_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$join_seq_parallel_$_state_machine__42121__auto____0;
com$wsscode$pathom$core$join_seq_parallel_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$join_seq_parallel_$_state_machine__42121__auto____1;
return com$wsscode$pathom$core$join_seq_parallel_$_state_machine__42121__auto__;
})()
})();
var state__42154__auto__ = (function (){var statearr_62575 = f__42153__auto__();
(statearr_62575[(6)] = c__42152__auto___63759);

return statearr_62575;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__42154__auto__);
}));


return ch__53544__auto__;
} else {
return cljs.core.PersistentVector.EMPTY;
}
});
/**
 * Runs the current subquery against the items of the given collection.
 */
com.wsscode.pathom.core.join_seq = (function com$wsscode$pathom$core$join_seq(p__62576,coll){
var map__62577 = p__62576;
var map__62577__$1 = cljs.core.__destructure_map(map__62577);
var env = map__62577__$1;
var parallel_QMARK_ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62577__$1,new cljs.core.Keyword("com.wsscode.pathom.parser","parallel?","com.wsscode.pathom.parser/parallel?",2071817193));
com.wsscode.pathom.trace.trace(env,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword("com.wsscode.pathom.trace","event","com.wsscode.pathom.trace/event",1287398983),new cljs.core.Keyword("com.wsscode.pathom.core","join-seq","com.wsscode.pathom.core/join-seq",523699901),new cljs.core.Keyword("com.wsscode.pathom.core","seq-count","com.wsscode.pathom.core/seq-count",-1671473836),cljs.core.count(coll)], null));

if(cljs.core.truth_(parallel_QMARK_)){
return com.wsscode.pathom.core.join_seq_parallel(env,coll);
} else {
var join_item = (function com$wsscode$pathom$core$join_seq_$_join_item(ent,out){
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$2(ent,cljs.core.update.cljs$core$IFn$_invoke$arity$4(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(env,new cljs.core.Keyword("com.wsscode.pathom.core","processing-sequence","com.wsscode.pathom.core/processing-sequence",1929448637),coll),new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),cljs.core.conj,cljs.core.count(out)));
});
var out = cljs.core.PersistentVector.EMPTY;
var G__62581 = coll;
var vec__62582 = G__62581;
var seq__62583 = cljs.core.seq(vec__62582);
var first__62584 = cljs.core.first(seq__62583);
var seq__62583__$1 = cljs.core.next(seq__62583);
var ent = first__62584;
var tail = seq__62583__$1;
var out__$1 = out;
var G__62581__$1 = G__62581;
while(true){
var out__$2 = out__$1;
var vec__62674 = G__62581__$1;
var seq__62675 = cljs.core.seq(vec__62674);
var first__62676 = cljs.core.first(seq__62675);
var seq__62675__$1 = cljs.core.next(seq__62675);
var ent__$1 = first__62676;
var tail__$1 = seq__62675__$1;
if(cljs.core.truth_(ent__$1)){
var res = join_item(ent__$1,out__$2);
if(com.wsscode.async.async_cljs.chan_QMARK_(res)){
var ch__53544__auto__ = cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();
var c__42152__auto___63803 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (out__$1,G__62581__$1,c__42152__auto___63803,ch__53544__auto__,res,out__$2,vec__62674,seq__62675,first__62676,seq__62675__$1,ent__$1,tail__$1,out,G__62581,vec__62582,seq__62583,first__62584,seq__62583__$1,ent,tail,map__62577,map__62577__$1,env,parallel_QMARK_){
return (function (){
var f__42153__auto__ = (function (){var switch__42120__auto__ = ((function (out__$1,G__62581__$1,c__42152__auto___63803,ch__53544__auto__,res,out__$2,vec__62674,seq__62675,first__62676,seq__62675__$1,ent__$1,tail__$1,out,G__62581,vec__62582,seq__62583,first__62584,seq__62583__$1,ent,tail,map__62577,map__62577__$1,env,parallel_QMARK_){
return (function (state_62732){
var state_val_62733 = (state_62732[(1)]);
if((state_val_62733 === (7))){
var inst_62701 = (state_62732[(7)]);
var inst_62707 = (state_62732[(8)]);
var inst_62706 = cljs.core.seq(inst_62701);
var inst_62707__$1 = cljs.core.first(inst_62706);
var inst_62708 = cljs.core.next(inst_62706);
var state_62732__$1 = (function (){var statearr_62734 = state_62732;
(statearr_62734[(8)] = inst_62707__$1);

(statearr_62734[(9)] = inst_62708);

return statearr_62734;
})();
if(cljs.core.truth_(inst_62707__$1)){
var statearr_62735_63804 = state_62732__$1;
(statearr_62735_63804[(1)] = (9));

} else {
var statearr_62736_63805 = state_62732__$1;
(statearr_62736_63805[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62733 === (1))){
var state_62732__$1 = state_62732;
var statearr_62737_63806 = state_62732__$1;
(statearr_62737_63806[(2)] = null);

(statearr_62737_63806[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62733 === (4))){
var inst_62677 = (state_62732[(2)]);
var state_62732__$1 = state_62732;
var statearr_62738_63807 = state_62732__$1;
(statearr_62738_63807[(2)] = inst_62677);

(statearr_62738_63807[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62733 === (15))){
var inst_62730 = (state_62732[(2)]);
var state_62732__$1 = state_62732;
return cljs.core.async.impl.ioc_helpers.return_chan(state_62732__$1,inst_62730);
} else {
if((state_val_62733 === (13))){
var inst_62726 = cljs.core.async.close_BANG_(ch__53544__auto__);
var state_62732__$1 = state_62732;
var statearr_62739_63808 = state_62732__$1;
(statearr_62739_63808[(2)] = inst_62726);

(statearr_62739_63808[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62733 === (6))){
var inst_62690 = (state_62732[(10)]);
var inst_62692 = (state_62732[(2)]);
var inst_62693 = com.wsscode.async.async_cljs.throw_err(inst_62692);
var inst_62694 = [inst_62693];
var inst_62695 = (new cljs.core.PersistentVector(null,1,(5),inst_62690,inst_62694,null));
var inst_62696 = tail__$1;
var inst_62697 = cljs.core.seq(inst_62696);
var inst_62698 = cljs.core.first(inst_62697);
var inst_62699 = cljs.core.next(inst_62697);
var inst_62700 = inst_62695;
var inst_62701 = inst_62696;
var state_62732__$1 = (function (){var statearr_62740 = state_62732;
(statearr_62740[(11)] = inst_62698);

(statearr_62740[(12)] = inst_62699);

(statearr_62740[(13)] = inst_62700);

(statearr_62740[(7)] = inst_62701);

return statearr_62740;
})();
var statearr_62741_63809 = state_62732__$1;
(statearr_62741_63809[(2)] = null);

(statearr_62741_63809[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62733 === (3))){
var inst_62723 = (state_62732[(14)]);
var inst_62723__$1 = (state_62732[(2)]);
var inst_62724 = (inst_62723__$1 == null);
var state_62732__$1 = (function (){var statearr_62742 = state_62732;
(statearr_62742[(14)] = inst_62723__$1);

return statearr_62742;
})();
if(cljs.core.truth_(inst_62724)){
var statearr_62743_63810 = state_62732__$1;
(statearr_62743_63810[(1)] = (13));

} else {
var statearr_62744_63811 = state_62732__$1;
(statearr_62744_63811[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62733 === (12))){
var inst_62700 = (state_62732[(13)]);
var inst_62708 = (state_62732[(9)]);
var inst_62712 = (state_62732[(2)]);
var inst_62713 = com.wsscode.async.async_cljs.throw_err(inst_62712);
var inst_62714 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(inst_62700,inst_62713);
var inst_62700__$1 = inst_62714;
var inst_62701 = inst_62708;
var state_62732__$1 = (function (){var statearr_62745 = state_62732;
(statearr_62745[(13)] = inst_62700__$1);

(statearr_62745[(7)] = inst_62701);

return statearr_62745;
})();
var statearr_62746_63812 = state_62732__$1;
(statearr_62746_63812[(2)] = null);

(statearr_62746_63812[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62733 === (2))){
var _ = (function (){var statearr_62747 = state_62732;
(statearr_62747[(4)] = cljs.core.cons((5),(state_62732[(4)])));

return statearr_62747;
})();
var inst_62690 = cljs.core.PersistentVector.EMPTY_NODE;
var state_62732__$1 = (function (){var statearr_62748 = state_62732;
(statearr_62748[(10)] = inst_62690);

return statearr_62748;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62732__$1,(6),res);
} else {
if((state_val_62733 === (11))){
var inst_62718 = (state_62732[(2)]);
var state_62732__$1 = state_62732;
var statearr_62750_63813 = state_62732__$1;
(statearr_62750_63813[(2)] = inst_62718);

(statearr_62750_63813[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62733 === (9))){
var inst_62707 = (state_62732[(8)]);
var inst_62700 = (state_62732[(13)]);
var inst_62710 = join_item(inst_62707,inst_62700);
var state_62732__$1 = state_62732;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62732__$1,(12),inst_62710);
} else {
if((state_val_62733 === (5))){
var _ = (function (){var statearr_62751 = state_62732;
(statearr_62751[(4)] = cljs.core.rest((state_62732[(4)])));

return statearr_62751;
})();
var state_62732__$1 = state_62732;
var ex62749 = (state_62732__$1[(2)]);
var statearr_62752_63814 = state_62732__$1;
(statearr_62752_63814[(5)] = ex62749);


var statearr_62753_63815 = state_62732__$1;
(statearr_62753_63815[(1)] = (4));

(statearr_62753_63815[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62733 === (14))){
var inst_62723 = (state_62732[(14)]);
var inst_62728 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(ch__53544__auto__,inst_62723);
var state_62732__$1 = state_62732;
var statearr_62754_63816 = state_62732__$1;
(statearr_62754_63816[(2)] = inst_62728);

(statearr_62754_63816[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62733 === (10))){
var inst_62700 = (state_62732[(13)]);
var state_62732__$1 = state_62732;
var statearr_62755_63817 = state_62732__$1;
(statearr_62755_63817[(2)] = inst_62700);

(statearr_62755_63817[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62733 === (8))){
var inst_62720 = (state_62732[(2)]);
var _ = (function (){var statearr_62756 = state_62732;
(statearr_62756[(4)] = cljs.core.rest((state_62732[(4)])));

return statearr_62756;
})();
var state_62732__$1 = state_62732;
var statearr_62757_63818 = state_62732__$1;
(statearr_62757_63818[(2)] = inst_62720);

(statearr_62757_63818[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(out__$1,G__62581__$1,c__42152__auto___63803,ch__53544__auto__,res,out__$2,vec__62674,seq__62675,first__62676,seq__62675__$1,ent__$1,tail__$1,out,G__62581,vec__62582,seq__62583,first__62584,seq__62583__$1,ent,tail,map__62577,map__62577__$1,env,parallel_QMARK_))
;
return ((function (out__$1,G__62581__$1,switch__42120__auto__,c__42152__auto___63803,ch__53544__auto__,res,out__$2,vec__62674,seq__62675,first__62676,seq__62675__$1,ent__$1,tail__$1,out,G__62581,vec__62582,seq__62583,first__62584,seq__62583__$1,ent,tail,map__62577,map__62577__$1,env,parallel_QMARK_){
return (function() {
var com$wsscode$pathom$core$join_seq_$_state_machine__42121__auto__ = null;
var com$wsscode$pathom$core$join_seq_$_state_machine__42121__auto____0 = (function (){
var statearr_62758 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_62758[(0)] = com$wsscode$pathom$core$join_seq_$_state_machine__42121__auto__);

(statearr_62758[(1)] = (1));

return statearr_62758;
});
var com$wsscode$pathom$core$join_seq_$_state_machine__42121__auto____1 = (function (state_62732){
while(true){
var ret_value__42122__auto__ = (function (){try{while(true){
var result__42123__auto__ = switch__42120__auto__(state_62732);
if(cljs.core.keyword_identical_QMARK_(result__42123__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__42123__auto__;
}
break;
}
}catch (e62759){var ex__42124__auto__ = e62759;
var statearr_62760_63819 = state_62732;
(statearr_62760_63819[(2)] = ex__42124__auto__);


if(cljs.core.seq((state_62732[(4)]))){
var statearr_62761_63820 = state_62732;
(statearr_62761_63820[(1)] = cljs.core.first((state_62732[(4)])));

} else {
throw ex__42124__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__42122__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__63822 = state_62732;
state_62732 = G__63822;
continue;
} else {
return ret_value__42122__auto__;
}
break;
}
});
com$wsscode$pathom$core$join_seq_$_state_machine__42121__auto__ = function(state_62732){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$join_seq_$_state_machine__42121__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$join_seq_$_state_machine__42121__auto____1.call(this,state_62732);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$join_seq_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$join_seq_$_state_machine__42121__auto____0;
com$wsscode$pathom$core$join_seq_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$join_seq_$_state_machine__42121__auto____1;
return com$wsscode$pathom$core$join_seq_$_state_machine__42121__auto__;
})()
;})(out__$1,G__62581__$1,switch__42120__auto__,c__42152__auto___63803,ch__53544__auto__,res,out__$2,vec__62674,seq__62675,first__62676,seq__62675__$1,ent__$1,tail__$1,out,G__62581,vec__62582,seq__62583,first__62584,seq__62583__$1,ent,tail,map__62577,map__62577__$1,env,parallel_QMARK_))
})();
var state__42154__auto__ = (function (){var statearr_62762 = f__42153__auto__();
(statearr_62762[(6)] = c__42152__auto___63803);

return statearr_62762;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__42154__auto__);
});})(out__$1,G__62581__$1,c__42152__auto___63803,ch__53544__auto__,res,out__$2,vec__62674,seq__62675,first__62676,seq__62675__$1,ent__$1,tail__$1,out,G__62581,vec__62582,seq__62583,first__62584,seq__62583__$1,ent,tail,map__62577,map__62577__$1,env,parallel_QMARK_))
);


return ch__53544__auto__;
} else {
var G__63823 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(out__$2,res);
var G__63824 = tail__$1;
out__$1 = G__63823;
G__62581__$1 = G__63824;
continue;
}
} else {
return out__$2;
}
break;
}
}
});
cljs.spec.alpha.def_impl(new cljs.core.Symbol("com.wsscode.pathom.core","join-map","com.wsscode.pathom.core/join-map",145426168,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","fspec","cljs.spec.alpha/fspec",-1289128341,null),new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"env","env",-1815813235),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null)),new cljs.core.Keyword(null,"m","m",1632677161),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),new cljs.core.Keyword(null,"ret","ret",-468222814),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","or","cljs.spec.alpha/or",-831679639,null),new cljs.core.Keyword(null,"map","map",1371690461),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Keyword(null,"map-chan","map-chan",-205889131),new cljs.core.Symbol("com.wsscode.async.async-cljs","chan?","com.wsscode.async.async-cljs/chan?",-1214839656,null))),cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"env","env",-1815813235),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null)),new cljs.core.Keyword(null,"m","m",1632677161),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"env","env",-1815813235),new cljs.core.Keyword(null,"m","m",1632677161)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.alpha.map_spec_impl(cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"req-un","req-un",1074571008),new cljs.core.Keyword(null,"opt-un","opt-un",883442496),new cljs.core.Keyword(null,"gfn","gfn",791517474),new cljs.core.Keyword(null,"pred-exprs","pred-exprs",1792271395),new cljs.core.Keyword(null,"keys-pred","keys-pred",858984739),new cljs.core.Keyword(null,"opt-keys","opt-keys",1262688261),new cljs.core.Keyword(null,"req-specs","req-specs",553962313),new cljs.core.Keyword(null,"req","req",-326448303),new cljs.core.Keyword(null,"req-keys","req-keys",514319221),new cljs.core.Keyword(null,"opt-specs","opt-specs",-384905450),new cljs.core.Keyword(null,"pred-forms","pred-forms",172611832),new cljs.core.Keyword(null,"opt","opt",-794706369)],[null,null,null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (G__62767){
return cljs.core.map_QMARK_(G__62767);
})], null),(function (G__62767){
return cljs.core.map_QMARK_(G__62767);
}),cljs.core.PersistentVector.EMPTY,cljs.core.PersistentVector.EMPTY,null,cljs.core.PersistentVector.EMPTY,cljs.core.PersistentVector.EMPTY,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.list(new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"%","%",-950237169,null)], null),cljs.core.list(new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Symbol(null,"%","%",-950237169,null)))], null),null])),cljs.core.map_QMARK_], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null)),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"env","env",-1815813235),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null)),new cljs.core.Keyword(null,"m","m",1632677161),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","or","cljs.spec.alpha/or",-831679639,null),new cljs.core.Keyword(null,"map","map",1371690461),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Keyword(null,"map-chan","map-chan",-205889131),new cljs.core.Symbol("com.wsscode.async.async-cljs","chan?","com.wsscode.async.async-cljs/chan?",-1214839656,null)),cljs.spec.alpha.or_spec_impl(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"map","map",1371690461),new cljs.core.Keyword(null,"map-chan","map-chan",-205889131)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Symbol("com.wsscode.async.async-cljs","chan?","com.wsscode.async.async-cljs/chan?",-1214839656,null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.map_QMARK_,com.wsscode.async.async_cljs.chan_QMARK_], null),null),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","or","cljs.spec.alpha/or",-831679639,null),new cljs.core.Keyword(null,"map","map",1371690461),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Keyword(null,"map-chan","map-chan",-205889131),new cljs.core.Symbol("com.wsscode.async.async-cljs","chan?","com.wsscode.async.async-cljs/chan?",-1214839656,null)),null,null,null));


/**
 * Runs the current subquery against the items of the given collection.
 * @type {function(!cljs.core.IMap, !cljs.core.IMap): *}
 */
com.wsscode.pathom.core.join_map = (function com$wsscode$pathom$core$join_map(env,m){
var map__62768 = cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"env","env",-1815813235),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null)),new cljs.core.Keyword(null,"m","m",1632677161),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"env","env",-1815813235),new cljs.core.Keyword(null,"m","m",1632677161)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.alpha.map_spec_impl(cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"req-un","req-un",1074571008),new cljs.core.Keyword(null,"opt-un","opt-un",883442496),new cljs.core.Keyword(null,"gfn","gfn",791517474),new cljs.core.Keyword(null,"pred-exprs","pred-exprs",1792271395),new cljs.core.Keyword(null,"keys-pred","keys-pred",858984739),new cljs.core.Keyword(null,"opt-keys","opt-keys",1262688261),new cljs.core.Keyword(null,"req-specs","req-specs",553962313),new cljs.core.Keyword(null,"req","req",-326448303),new cljs.core.Keyword(null,"req-keys","req-keys",514319221),new cljs.core.Keyword(null,"opt-specs","opt-specs",-384905450),new cljs.core.Keyword(null,"pred-forms","pred-forms",172611832),new cljs.core.Keyword(null,"opt","opt",-794706369)],[null,null,null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (G__62769){
return cljs.core.map_QMARK_(G__62769);
})], null),(function (G__62769){
return cljs.core.map_QMARK_(G__62769);
}),cljs.core.PersistentVector.EMPTY,cljs.core.PersistentVector.EMPTY,null,cljs.core.PersistentVector.EMPTY,cljs.core.PersistentVector.EMPTY,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.list(new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"%","%",-950237169,null)], null),cljs.core.list(new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Symbol(null,"%","%",-950237169,null)))], null),null])),cljs.core.map_QMARK_], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null)),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"env","env",-1815813235),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null)),new cljs.core.Keyword(null,"m","m",1632677161),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","or","cljs.spec.alpha/or",-831679639,null),new cljs.core.Keyword(null,"map","map",1371690461),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Keyword(null,"map-chan","map-chan",-205889131),new cljs.core.Symbol("com.wsscode.async.async-cljs","chan?","com.wsscode.async.async-cljs/chan?",-1214839656,null)),cljs.spec.alpha.or_spec_impl(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"map","map",1371690461),new cljs.core.Keyword(null,"map-chan","map-chan",-205889131)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Symbol("com.wsscode.async.async-cljs","chan?","com.wsscode.async.async-cljs/chan?",-1214839656,null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.map_QMARK_,com.wsscode.async.async_cljs.chan_QMARK_], null),null),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","or","cljs.spec.alpha/or",-831679639,null),new cljs.core.Keyword(null,"map","map",1371690461),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Keyword(null,"map-chan","map-chan",-205889131),new cljs.core.Symbol("com.wsscode.async.async-cljs","chan?","com.wsscode.async.async-cljs/chan?",-1214839656,null)),null,null,null);
var map__62768__$1 = cljs.core.__destructure_map(map__62768);
var retspec62764 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62768__$1,new cljs.core.Keyword(null,"ret","ret",-468222814));
var argspec62763 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62768__$1,new cljs.core.Keyword(null,"args","args",1315556576));
if(cljs.core.truth_(argspec62763)){
com.fulcrologic.guardrails.core.run_check(new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/wsscode/pathom/core.cljc:602 join-map's",new cljs.core.Keyword(null,"tap>?","tap>?",212454486),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false,new cljs.core.Keyword(null,"expound-opts","expound-opts",623087481),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"args?","args?",-1963723548),true], null),argspec62763,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [env,m], null));
} else {
}

var f62766 = (function (env__$1,m__$1){
com.wsscode.pathom.trace.trace(env__$1,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword("com.wsscode.pathom.trace","event","com.wsscode.pathom.trace/event",1287398983),new cljs.core.Keyword("com.wsscode.pathom.core","join-map","com.wsscode.pathom.core/join-map",-1495105359),new cljs.core.Keyword("com.wsscode.pathom.core","seq-count","com.wsscode.pathom.core/seq-count",-1671473836),cljs.core.count(m__$1)], null));

var join_item = (function com$wsscode$pathom$core$join_map_$_join_item(k,ent){
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$2(ent,cljs.core.update.cljs$core$IFn$_invoke$arity$4(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(env__$1,new cljs.core.Keyword("com.wsscode.pathom.core","processing-sequence","com.wsscode.pathom.core/processing-sequence",1929448637),m__$1),new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),cljs.core.conj,k));
});
var out = cljs.core.PersistentArrayMap.EMPTY;
var G__62773 = m__$1;
var vec__62774 = G__62773;
var seq__62775 = cljs.core.seq(vec__62774);
var first__62776 = cljs.core.first(seq__62775);
var seq__62775__$1 = cljs.core.next(seq__62775);
var pair = first__62776;
var tail = seq__62775__$1;
var out__$1 = out;
var G__62773__$1 = G__62773;
while(true){
var out__$2 = out__$1;
var vec__62875 = G__62773__$1;
var seq__62876 = cljs.core.seq(vec__62875);
var first__62877 = cljs.core.first(seq__62876);
var seq__62876__$1 = cljs.core.next(seq__62876);
var pair__$1 = first__62877;
var tail__$1 = seq__62876__$1;
if(cljs.core.truth_(pair__$1)){
var vec__62878 = pair__$1;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__62878,(0),null);
var ent = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__62878,(1),null);
var res = join_item(k,ent);
if(com.wsscode.async.async_cljs.chan_QMARK_(res)){
var ch__53544__auto__ = cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();
var c__42152__auto___63826 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (out__$1,G__62773__$1,c__42152__auto___63826,ch__53544__auto__,vec__62878,k,ent,res,out__$2,vec__62875,seq__62876,first__62877,seq__62876__$1,pair__$1,tail__$1,out,G__62773,vec__62774,seq__62775,first__62776,seq__62775__$1,pair,tail,map__62768,map__62768__$1,retspec62764,argspec62763){
return (function (){
var f__42153__auto__ = (function (){var switch__42120__auto__ = ((function (out__$1,G__62773__$1,c__42152__auto___63826,ch__53544__auto__,vec__62878,k,ent,res,out__$2,vec__62875,seq__62876,first__62877,seq__62876__$1,pair__$1,tail__$1,out,G__62773,vec__62774,seq__62775,first__62776,seq__62775__$1,pair,tail,map__62768,map__62768__$1,retspec62764,argspec62763){
return (function (state_62941){
var state_val_62942 = (state_62941[(1)]);
if((state_val_62942 === (7))){
var inst_62905 = (state_62941[(7)]);
var inst_62911 = (state_62941[(8)]);
var inst_62910 = cljs.core.seq(inst_62905);
var inst_62911__$1 = cljs.core.first(inst_62910);
var inst_62912 = cljs.core.next(inst_62910);
var state_62941__$1 = (function (){var statearr_62943 = state_62941;
(statearr_62943[(8)] = inst_62911__$1);

(statearr_62943[(9)] = inst_62912);

return statearr_62943;
})();
if(cljs.core.truth_(inst_62911__$1)){
var statearr_62944_63827 = state_62941__$1;
(statearr_62944_63827[(1)] = (9));

} else {
var statearr_62945_63828 = state_62941__$1;
(statearr_62945_63828[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62942 === (1))){
var state_62941__$1 = state_62941;
var statearr_62946_63829 = state_62941__$1;
(statearr_62946_63829[(2)] = null);

(statearr_62946_63829[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62942 === (4))){
var inst_62881 = (state_62941[(2)]);
var state_62941__$1 = state_62941;
var statearr_62947_63830 = state_62941__$1;
(statearr_62947_63830[(2)] = inst_62881);

(statearr_62947_63830[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62942 === (15))){
var inst_62939 = (state_62941[(2)]);
var state_62941__$1 = state_62941;
return cljs.core.async.impl.ioc_helpers.return_chan(state_62941__$1,inst_62939);
} else {
if((state_val_62942 === (13))){
var inst_62935 = cljs.core.async.close_BANG_(ch__53544__auto__);
var state_62941__$1 = state_62941;
var statearr_62948_63831 = state_62941__$1;
(statearr_62948_63831[(2)] = inst_62935);

(statearr_62948_63831[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62942 === (6))){
var inst_62894 = (state_62941[(10)]);
var inst_62896 = (state_62941[(2)]);
var inst_62897 = com.wsscode.async.async_cljs.throw_err(inst_62896);
var inst_62898 = [inst_62897];
var inst_62899 = cljs.core.PersistentHashMap.fromArrays(inst_62894,inst_62898);
var inst_62900 = tail__$1;
var inst_62901 = cljs.core.seq(inst_62900);
var inst_62902 = cljs.core.first(inst_62901);
var inst_62903 = cljs.core.next(inst_62901);
var inst_62904 = inst_62899;
var inst_62905 = inst_62900;
var state_62941__$1 = (function (){var statearr_62949 = state_62941;
(statearr_62949[(11)] = inst_62902);

(statearr_62949[(12)] = inst_62903);

(statearr_62949[(13)] = inst_62904);

(statearr_62949[(7)] = inst_62905);

return statearr_62949;
})();
var statearr_62950_63832 = state_62941__$1;
(statearr_62950_63832[(2)] = null);

(statearr_62950_63832[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62942 === (3))){
var inst_62932 = (state_62941[(14)]);
var inst_62932__$1 = (state_62941[(2)]);
var inst_62933 = (inst_62932__$1 == null);
var state_62941__$1 = (function (){var statearr_62951 = state_62941;
(statearr_62951[(14)] = inst_62932__$1);

return statearr_62951;
})();
if(cljs.core.truth_(inst_62933)){
var statearr_62952_63833 = state_62941__$1;
(statearr_62952_63833[(1)] = (13));

} else {
var statearr_62953_63834 = state_62941__$1;
(statearr_62953_63834[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62942 === (12))){
var inst_62904 = (state_62941[(13)]);
var inst_62917 = (state_62941[(15)]);
var inst_62912 = (state_62941[(9)]);
var inst_62921 = (state_62941[(2)]);
var inst_62922 = com.wsscode.async.async_cljs.throw_err(inst_62921);
var inst_62923 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(inst_62904,inst_62917,inst_62922);
var inst_62904__$1 = inst_62923;
var inst_62905 = inst_62912;
var state_62941__$1 = (function (){var statearr_62954 = state_62941;
(statearr_62954[(13)] = inst_62904__$1);

(statearr_62954[(7)] = inst_62905);

return statearr_62954;
})();
var statearr_62955_63835 = state_62941__$1;
(statearr_62955_63835[(2)] = null);

(statearr_62955_63835[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62942 === (2))){
var _ = (function (){var statearr_62956 = state_62941;
(statearr_62956[(4)] = cljs.core.cons((5),(state_62941[(4)])));

return statearr_62956;
})();
var inst_62894 = [k];
var state_62941__$1 = (function (){var statearr_62957 = state_62941;
(statearr_62957[(10)] = inst_62894);

return statearr_62957;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62941__$1,(6),res);
} else {
if((state_val_62942 === (11))){
var inst_62927 = (state_62941[(2)]);
var state_62941__$1 = state_62941;
var statearr_62959_63836 = state_62941__$1;
(statearr_62959_63836[(2)] = inst_62927);

(statearr_62959_63836[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62942 === (9))){
var inst_62911 = (state_62941[(8)]);
var inst_62917 = (state_62941[(15)]);
var inst_62917__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_62911,(0),null);
var inst_62918 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_62911,(1),null);
var inst_62919 = join_item(inst_62917__$1,inst_62918);
var state_62941__$1 = (function (){var statearr_62960 = state_62941;
(statearr_62960[(15)] = inst_62917__$1);

return statearr_62960;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62941__$1,(12),inst_62919);
} else {
if((state_val_62942 === (5))){
var _ = (function (){var statearr_62961 = state_62941;
(statearr_62961[(4)] = cljs.core.rest((state_62941[(4)])));

return statearr_62961;
})();
var state_62941__$1 = state_62941;
var ex62958 = (state_62941__$1[(2)]);
var statearr_62962_63837 = state_62941__$1;
(statearr_62962_63837[(5)] = ex62958);


var statearr_62963_63838 = state_62941__$1;
(statearr_62963_63838[(1)] = (4));

(statearr_62963_63838[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62942 === (14))){
var inst_62932 = (state_62941[(14)]);
var inst_62937 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(ch__53544__auto__,inst_62932);
var state_62941__$1 = state_62941;
var statearr_62964_63839 = state_62941__$1;
(statearr_62964_63839[(2)] = inst_62937);

(statearr_62964_63839[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62942 === (10))){
var inst_62904 = (state_62941[(13)]);
var state_62941__$1 = state_62941;
var statearr_62965_63840 = state_62941__$1;
(statearr_62965_63840[(2)] = inst_62904);

(statearr_62965_63840[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62942 === (8))){
var inst_62929 = (state_62941[(2)]);
var _ = (function (){var statearr_62966 = state_62941;
(statearr_62966[(4)] = cljs.core.rest((state_62941[(4)])));

return statearr_62966;
})();
var state_62941__$1 = state_62941;
var statearr_62967_63841 = state_62941__$1;
(statearr_62967_63841[(2)] = inst_62929);

(statearr_62967_63841[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(out__$1,G__62773__$1,c__42152__auto___63826,ch__53544__auto__,vec__62878,k,ent,res,out__$2,vec__62875,seq__62876,first__62877,seq__62876__$1,pair__$1,tail__$1,out,G__62773,vec__62774,seq__62775,first__62776,seq__62775__$1,pair,tail,map__62768,map__62768__$1,retspec62764,argspec62763))
;
return ((function (out__$1,G__62773__$1,switch__42120__auto__,c__42152__auto___63826,ch__53544__auto__,vec__62878,k,ent,res,out__$2,vec__62875,seq__62876,first__62877,seq__62876__$1,pair__$1,tail__$1,out,G__62773,vec__62774,seq__62775,first__62776,seq__62775__$1,pair,tail,map__62768,map__62768__$1,retspec62764,argspec62763){
return (function() {
var com$wsscode$pathom$core$join_map_$_state_machine__42121__auto__ = null;
var com$wsscode$pathom$core$join_map_$_state_machine__42121__auto____0 = (function (){
var statearr_62968 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_62968[(0)] = com$wsscode$pathom$core$join_map_$_state_machine__42121__auto__);

(statearr_62968[(1)] = (1));

return statearr_62968;
});
var com$wsscode$pathom$core$join_map_$_state_machine__42121__auto____1 = (function (state_62941){
while(true){
var ret_value__42122__auto__ = (function (){try{while(true){
var result__42123__auto__ = switch__42120__auto__(state_62941);
if(cljs.core.keyword_identical_QMARK_(result__42123__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__42123__auto__;
}
break;
}
}catch (e62969){var ex__42124__auto__ = e62969;
var statearr_62970_63842 = state_62941;
(statearr_62970_63842[(2)] = ex__42124__auto__);


if(cljs.core.seq((state_62941[(4)]))){
var statearr_62971_63843 = state_62941;
(statearr_62971_63843[(1)] = cljs.core.first((state_62941[(4)])));

} else {
throw ex__42124__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__42122__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__63844 = state_62941;
state_62941 = G__63844;
continue;
} else {
return ret_value__42122__auto__;
}
break;
}
});
com$wsscode$pathom$core$join_map_$_state_machine__42121__auto__ = function(state_62941){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$join_map_$_state_machine__42121__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$join_map_$_state_machine__42121__auto____1.call(this,state_62941);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$join_map_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$join_map_$_state_machine__42121__auto____0;
com$wsscode$pathom$core$join_map_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$join_map_$_state_machine__42121__auto____1;
return com$wsscode$pathom$core$join_map_$_state_machine__42121__auto__;
})()
;})(out__$1,G__62773__$1,switch__42120__auto__,c__42152__auto___63826,ch__53544__auto__,vec__62878,k,ent,res,out__$2,vec__62875,seq__62876,first__62877,seq__62876__$1,pair__$1,tail__$1,out,G__62773,vec__62774,seq__62775,first__62776,seq__62775__$1,pair,tail,map__62768,map__62768__$1,retspec62764,argspec62763))
})();
var state__42154__auto__ = (function (){var statearr_62972 = f__42153__auto__();
(statearr_62972[(6)] = c__42152__auto___63826);

return statearr_62972;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__42154__auto__);
});})(out__$1,G__62773__$1,c__42152__auto___63826,ch__53544__auto__,vec__62878,k,ent,res,out__$2,vec__62875,seq__62876,first__62877,seq__62876__$1,pair__$1,tail__$1,out,G__62773,vec__62774,seq__62775,first__62776,seq__62775__$1,pair,tail,map__62768,map__62768__$1,retspec62764,argspec62763))
);


return ch__53544__auto__;
} else {
var G__63845 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(out__$2,k,res);
var G__63846 = tail__$1;
out__$1 = G__63845;
G__62773__$1 = G__63846;
continue;
}
} else {
return out__$2;
}
break;
}
});
var ret62765 = f62766(env,m);
if(cljs.core.truth_(retspec62764)){
com.fulcrologic.guardrails.core.run_check(new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/wsscode/pathom/core.cljc:602 join-map's",new cljs.core.Keyword(null,"tap>?","tap>?",212454486),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false,new cljs.core.Keyword(null,"expound-opts","expound-opts",623087481),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"args?","args?",-1963723548),false], null),retspec62764,ret62765);
} else {
}

return ret62765;
});
com.wsscode.pathom.core.ident_QMARK_ = (function com$wsscode$pathom$core$ident_QMARK_(x){
return ((cljs.core.vector_QMARK_(x)) && ((((cljs.core.first(x) instanceof cljs.core.Keyword)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((2),cljs.core.count(x))))));
});
com.wsscode.pathom.core.ident_key_STAR_ = (function com$wsscode$pathom$core$ident_key_STAR_(key){
if(cljs.core.vector_QMARK_(key)){
return cljs.core.first(key);
} else {
return null;
}
});
/**
 * The first element of an ident.
 */
com.wsscode.pathom.core.ident_key = (function com$wsscode$pathom$core$ident_key(p__62973){
var map__62974 = p__62973;
var map__62974__$1 = cljs.core.__destructure_map(map__62974);
var ast = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62974__$1,new cljs.core.Keyword(null,"ast","ast",-860334068));
var key = (function (){var G__62975 = ast;
if((G__62975 == null)){
return null;
} else {
return new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(G__62975);
}
})();
if(cljs.core.vector_QMARK_(key)){
return cljs.core.first(key);
} else {
return null;
}
});
com.wsscode.pathom.core.ident_value_STAR_ = (function com$wsscode$pathom$core$ident_value_STAR_(key){
if(cljs.core.vector_QMARK_(key)){
return cljs.core.second(key);
} else {
return null;
}
});
/**
 * The second element of an ident
 */
com.wsscode.pathom.core.ident_value = (function com$wsscode$pathom$core$ident_value(p__62976){
var map__62977 = p__62976;
var map__62977__$1 = cljs.core.__destructure_map(map__62977);
var ast = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62977__$1,new cljs.core.Keyword(null,"ast","ast",-860334068));
var key = (function (){var G__62978 = ast;
if((G__62978 == null)){
return null;
} else {
return new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(G__62978);
}
})();
if(cljs.core.sequential_QMARK_(key)){
return cljs.core.second(key);
} else {
return null;
}
});
/**
 * Remove items from a query (AST) that have a key listed in the elision-set
 */
com.wsscode.pathom.core.elide_ast_nodes = (function com$wsscode$pathom$core$elide_ast_nodes(p__62980,elision_set){
var map__62981 = p__62980;
var map__62981__$1 = cljs.core.__destructure_map(map__62981);
var ast = map__62981__$1;
var key = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62981__$1,new cljs.core.Keyword(null,"key","key",-1516042587));
var union_key = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62981__$1,new cljs.core.Keyword(null,"union-key","union-key",1529707234));
var union_elision_QMARK_ = cljs.core.contains_QMARK_(elision_set,union_key);
if(((union_elision_QMARK_) || (cljs.core.contains_QMARK_(elision_set,key)))){
return null;
} else {
return cljs.core.update.cljs$core$IFn$_invoke$arity$3(ast,new cljs.core.Keyword(null,"children","children",-940561982),(function (c){
if(cljs.core.truth_(c)){
return cljs.core.vec(cljs.core.keep.cljs$core$IFn$_invoke$arity$2((function (p1__62979_SHARP_){
return (com.wsscode.pathom.core.elide_ast_nodes.cljs$core$IFn$_invoke$arity$2 ? com.wsscode.pathom.core.elide_ast_nodes.cljs$core$IFn$_invoke$arity$2(p1__62979_SHARP_,elision_set) : com.wsscode.pathom.core.elide_ast_nodes.call(null,p1__62979_SHARP_,elision_set));
}),c));
} else {
return null;
}
}));
}
});
com.wsscode.pathom.core.normalize_env = (function com$wsscode$pathom$core$normalize_env(p__62982){
var map__62983 = p__62982;
var map__62983__$1 = cljs.core.__destructure_map(map__62983);
var env = map__62983__$1;
var ast = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62983__$1,new cljs.core.Keyword(null,"ast","ast",-860334068));
var G__62984 = cljs.core.update.cljs$core$IFn$_invoke$arity$4(env,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),cljs.core.fnil.cljs$core$IFn$_invoke$arity$2(cljs.core.conj,cljs.core.PersistentVector.EMPTY),new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(ast));
if((new cljs.core.Keyword("com.wsscode.pathom.core","entity-key","com.wsscode.pathom.core/entity-key",1494103249).cljs$core$IFn$_invoke$arity$1(env) == null)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__62984,new cljs.core.Keyword("com.wsscode.pathom.core","entity-key","com.wsscode.pathom.core/entity-key",1494103249),new cljs.core.Keyword("com.wsscode.pathom.core","entity","com.wsscode.pathom.core/entity",884706031));
} else {
return G__62984;
}
});
com.wsscode.pathom.core.merge_queries_STAR_ = (function com$wsscode$pathom$core$merge_queries_STAR_(qa,qb){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ast,p__62987){
var map__62988 = p__62987;
var map__62988__$1 = cljs.core.__destructure_map(map__62988);
var item_b = map__62988__$1;
var key = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62988__$1,new cljs.core.Keyword(null,"key","key",-1516042587));
var type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62988__$1,new cljs.core.Keyword(null,"type","type",1174270348));
var params = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__62988__$1,new cljs.core.Keyword(null,"params","params",710516235));
var temp__5802__auto__ = cljs.core.first(cljs.core.keep_indexed.cljs$core$IFn$_invoke$arity$2((function (p1__62986_SHARP_,p2__62985_SHARP_){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(p2__62985_SHARP_),key)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__62986_SHARP_,p2__62985_SHARP_], null);
} else {
return null;
}
}),new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(ast)));
if(cljs.core.truth_(temp__5802__auto__)){
var vec__62989 = temp__5802__auto__;
var idx = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__62989,(0),null);
var item = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__62989,(1),null);
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"join","join",-758861890),new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(item),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([type], 0))) || (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"prop","prop",-515168332),new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(item),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([type], 0))))){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"params","params",710516235).cljs$core$IFn$_invoke$arity$1(item),params)){
return cljs.core.update_in.cljs$core$IFn$_invoke$arity$4(ast,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"children","children",-940561982),idx], null),com.wsscode.pathom.core.merge_queries_STAR_,item_b);
} else {
return cljs.core.reduced(null);
}
} else {
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"prop","prop",-515168332),new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(item))) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"join","join",-758861890),type)))){
return cljs.core.assoc_in(ast,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"children","children",-940561982),idx], null),item_b);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"call","call",-519999866),type)){
return cljs.core.reduced(null);
} else {
return ast;

}
}
}
} else {
return cljs.core.update.cljs$core$IFn$_invoke$arity$4(ast,new cljs.core.Keyword(null,"children","children",-940561982),cljs.core.conj,item_b);
}
}),qa,new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(qb));
});
com.wsscode.pathom.core.merge_queries = (function com$wsscode$pathom$core$merge_queries(qa,qb){
var G__62992 = com.wsscode.pathom.core.merge_queries_STAR_(com.wsscode.pathom.core.query__GT_ast(qa),com.wsscode.pathom.core.query__GT_ast(qb));
if((G__62992 == null)){
return null;
} else {
return com.wsscode.pathom.core.ast__GT_query(G__62992);
}
});
/**
 * Converts ident values and param values to ::p/var.
 */
com.wsscode.pathom.core.normalize_query_variables = (function com$wsscode$pathom$core$normalize_query_variables(query){
return com.wsscode.pathom.core.ast__GT_query(com.wsscode.pathom.core.transduce_children(cljs.core.map.cljs$core$IFn$_invoke$arity$1((function (x){
var G__62994 = x;
var G__62994__$1 = ((com.wsscode.pathom.core.ident_QMARK_(new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(x)))?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__62994,new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.first(new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(x)),new cljs.core.Keyword("com.wsscode.pathom.core","var","com.wsscode.pathom.core/var",-2126559442)], null)):G__62994);
if(cljs.core.truth_(new cljs.core.Keyword(null,"params","params",710516235).cljs$core$IFn$_invoke$arity$1(x))){
return cljs.core.update.cljs$core$IFn$_invoke$arity$3(G__62994__$1,new cljs.core.Keyword(null,"params","params",710516235),(function (p1__62993_SHARP_){
return cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentArrayMap.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$1((function (p__62995){
var vec__62996 = p__62995;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__62996,(0),null);
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__62996,(1),null);
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [k,new cljs.core.Keyword("com.wsscode.pathom.core","var","com.wsscode.pathom.core/var",-2126559442)], null);
})),p1__62993_SHARP_);
}));
} else {
return G__62994__$1;
}
})),com.wsscode.pathom.core.query__GT_ast(query)));
});
/**
 * Generates a consistent hash from the query. The query first goes to a process to remove any
 *   variables from idents and params, then we get the Clojure hash of it. You can use this to save
 *   information about a query that can be used to correlate with the query later.
 */
com.wsscode.pathom.core.query_id = (function com$wsscode$pathom$core$query_id(query){
return cljs.core.hash(com.wsscode.pathom.core.normalize_query_variables(query));
});
com.wsscode.pathom.core.key_dispatch = (function com$wsscode$pathom$core$key_dispatch(p__62999){
var map__63000 = p__62999;
var map__63000__$1 = cljs.core.__destructure_map(map__63000);
var ast = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63000__$1,new cljs.core.Keyword(null,"ast","ast",-860334068));
return new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(ast);
});
/**
 * Dispatch on the first element (type) of an incoming ident.
 */
com.wsscode.pathom.core.entity_dispatch = (function com$wsscode$pathom$core$entity_dispatch(p__63001){
var map__63002 = p__63001;
var map__63002__$1 = cljs.core.__destructure_map(map__63002);
var ast = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63002__$1,new cljs.core.Keyword(null,"ast","ast",-860334068));
if(cljs.core.vector_QMARK_(new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(ast))){
return cljs.core.first(new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(ast));
} else {
return null;
}
});
/**
 * Produces a reader that will respond to any keyword whose namespace
 *   is in the set `(::placeholder-prefixes env)`. The join node logical
 *   level stays the same as the parent where the placeholder node is
 *   requested.
 */
com.wsscode.pathom.core.env_placeholder_reader = (function com$wsscode$pathom$core$env_placeholder_reader(p__63003){
var map__63004 = p__63003;
var map__63004__$1 = cljs.core.__destructure_map(map__63004);
var env = map__63004__$1;
var placeholder_prefixes = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63004__$1,new cljs.core.Keyword("com.wsscode.pathom.core","placeholder-prefixes","com.wsscode.pathom.core/placeholder-prefixes",-1362240644));
if(cljs.core.truth_(placeholder_prefixes)){
} else {
throw (new Error(["Assert failed: ","To use env-placeholder-reader please add ::p/placeholder-prefixes to your environment.","\n","placeholder-prefixes"].join('')));
}

if(com.wsscode.pathom.core.placeholder_key_QMARK_(env,new cljs.core.Keyword(null,"dispatch-key","dispatch-key",733619510).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"ast","ast",-860334068).cljs$core$IFn$_invoke$arity$1(env)))){
var merge_data = new cljs.core.Keyword(null,"params","params",710516235).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"ast","ast",-860334068).cljs$core$IFn$_invoke$arity$1(env));
if(cljs.core.seq(merge_data)){
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$1(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(env,new cljs.core.Keyword("com.wsscode.pathom.core","entity","com.wsscode.pathom.core/entity",884706031),cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$1(env),merge_data], 0)))));
} else {
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$1(env);
}
} else {
return new cljs.core.Keyword("com.wsscode.pathom.core","continue","com.wsscode.pathom.core/continue",591698194);
}
});
/**
 * This will lift the AST from placeholders to the same level of the query, as if there was not placeholders in it.
 */
com.wsscode.pathom.core.lift_placeholders_ast = (function com$wsscode$pathom$core$lift_placeholders_ast(env,ast){
return clojure.walk.postwalk((function (x){
var temp__5802__auto__ = new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(x);
if(cljs.core.truth_(temp__5802__auto__)){
var children = temp__5802__auto__;
var map__63006 = cljs.core.group_by((function (p1__63005_SHARP_){
return ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"join","join",-758861890),new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(p1__63005_SHARP_))) && (com.wsscode.pathom.core.placeholder_key_QMARK_(env,new cljs.core.Keyword(null,"dispatch-key","dispatch-key",733619510).cljs$core$IFn$_invoke$arity$1(p1__63005_SHARP_))));
}),children);
var map__63006__$1 = cljs.core.__destructure_map(map__63006);
var placeholders = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63006__$1,true);
var regular = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63006__$1,false);
var _LT__GT_ = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(x,new cljs.core.Keyword(null,"children","children",-940561982),(function (){var or__5045__auto__ = regular;
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
return cljs.core.PersistentVector.EMPTY;
}
})());
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(com.wsscode.pathom.core.merge_queries_STAR_,_LT__GT_,placeholders);
} else {
return x;
}
}),ast);
});
/**
 * This will lift the queries from placeholders to the same level of the query, as if there was not placeholders in it.
 */
com.wsscode.pathom.core.lift_placeholders = (function com$wsscode$pathom$core$lift_placeholders(env,query){
return com.wsscode.pathom.core.ast__GT_query(com.wsscode.pathom.core.lift_placeholders_ast(env,com.wsscode.pathom.core.query__GT_ast(query)));
});
/**
 * Children should join when there is a query, unless the value is marked as final.
 */
com.wsscode.pathom.core.join_children_QMARK_ = (function com$wsscode$pathom$core$join_children_QMARK_(p__63007,v){
var map__63008 = p__63007;
var map__63008__$1 = cljs.core.__destructure_map(map__63008);
var query = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63008__$1,new cljs.core.Keyword(null,"query","query",-1288509510));
var and__5043__auto__ = query;
if(cljs.core.truth_(and__5043__auto__)){
return cljs.core.not(new cljs.core.Keyword("com.wsscode.pathom.core","final","com.wsscode.pathom.core/final",891454300).cljs$core$IFn$_invoke$arity$1(cljs.core.meta(v)));
} else {
return and__5043__auto__;
}
});
/**
 * Map reader will try to find the ast key on the current entity and output it. When the value is a map and a
 *   sub query is present, it will apply the sub query on that value (recursively). When the value is a sequence,
 *   map-reader will do a join on each of the items (and apply sub queries if it's present and values are maps.
 * 
 *   Map-reader will defer the read when the key is not present at entity.
 */
com.wsscode.pathom.core.map_reader = (function com$wsscode$pathom$core$map_reader(p__63009){
var map__63010 = p__63009;
var map__63010__$1 = cljs.core.__destructure_map(map__63010);
var env = map__63010__$1;
var ast = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63010__$1,new cljs.core.Keyword(null,"ast","ast",-860334068));
var query = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63010__$1,new cljs.core.Keyword(null,"query","query",-1288509510));
var key = new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(ast);
var entity = com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$1(env);
if(cljs.core.contains_QMARK_(entity,key)){
var v = cljs.core.get.cljs$core$IFn$_invoke$arity$2(entity,key);
if(cljs.core.sequential_QMARK_(v)){
if(cljs.core.truth_(com.wsscode.pathom.core.join_children_QMARK_(env,v))){
return com.wsscode.pathom.core.join_seq(env,v);
} else {
return v;
}
} else {
if(cljs.core.truth_((function (){var and__5043__auto__ = cljs.core.map_QMARK_(v);
if(and__5043__auto__){
var or__5045__auto__ = new cljs.core.Keyword("com.wsscode.pathom.core","map-of-maps","com.wsscode.pathom.core/map-of-maps",-1598019706).cljs$core$IFn$_invoke$arity$1(cljs.core.meta(v));
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
return new cljs.core.Keyword("com.wsscode.pathom.core","map-of-maps","com.wsscode.pathom.core/map-of-maps",-1598019706).cljs$core$IFn$_invoke$arity$1(cljs.core.meta(query));
}
} else {
return and__5043__auto__;
}
})())){
if(cljs.core.truth_(com.wsscode.pathom.core.join_children_QMARK_(env,v))){
return com.wsscode.pathom.core.join_map(env,v);
} else {
return v;
}
} else {
if(cljs.core.truth_((function (){var and__5043__auto__ = cljs.core.map_QMARK_(v);
if(and__5043__auto__){
return com.wsscode.pathom.core.join_children_QMARK_(env,v);
} else {
return and__5043__auto__;
}
})())){
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$2(v,env);
} else {
return v;
}

}
}
} else {
return new cljs.core.Keyword("com.wsscode.pathom.core","continue","com.wsscode.pathom.core/continue",591698194);
}
});
/**
 * Like map-reader, but it has extra options (read from the environment):
 *   map-key-transform: (fn [key]) will transform the key on the AST before trying to match with entity key
 *   map-value-transform: (fn [key value]) will transform the output value after reading from the entity.
 * 
 *   The reason to have a separated reader is so the plain version (map-reader) can be faster by avoiding checking
 *   the presence of transform functions.
 */
com.wsscode.pathom.core.map_reader_STAR_ = (function com$wsscode$pathom$core$map_reader_STAR_(p__63011){
var map__63012 = p__63011;
var map__63012__$1 = cljs.core.__destructure_map(map__63012);
var map_key_transform = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63012__$1,new cljs.core.Keyword("com.wsscode.pathom.core","map-key-transform","com.wsscode.pathom.core/map-key-transform",-238565800));
var map_value_transform = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63012__$1,new cljs.core.Keyword("com.wsscode.pathom.core","map-value-transform","com.wsscode.pathom.core/map-value-transform",1252006952));
return (function (p__63013){
var map__63014 = p__63013;
var map__63014__$1 = cljs.core.__destructure_map(map__63014);
var env = map__63014__$1;
var ast = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63014__$1,new cljs.core.Keyword(null,"ast","ast",-860334068));
var query = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63014__$1,new cljs.core.Keyword(null,"query","query",-1288509510));
var entity_key = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63014__$1,new cljs.core.Keyword("com.wsscode.pathom.core","entity-key","com.wsscode.pathom.core/entity-key",1494103249));
var key = (function (){var G__63015 = new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(ast);
if(cljs.core.truth_(map_key_transform)){
return (map_key_transform.cljs$core$IFn$_invoke$arity$1 ? map_key_transform.cljs$core$IFn$_invoke$arity$1(G__63015) : map_key_transform.call(null,G__63015));
} else {
return G__63015;
}
})();
var entity = com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$1(env);
if(cljs.core.contains_QMARK_(entity,key)){
var v = cljs.core.get.cljs$core$IFn$_invoke$arity$2(entity,key);
if(cljs.core.sequential_QMARK_(v)){
if(cljs.core.truth_(com.wsscode.pathom.core.join_children_QMARK_(env,v))){
return com.wsscode.pathom.core.join_seq(env,v);
} else {
return v;
}
} else {
if(cljs.core.truth_((function (){var and__5043__auto__ = cljs.core.map_QMARK_(v);
if(and__5043__auto__){
var or__5045__auto__ = new cljs.core.Keyword("com.wsscode.pathom.core","map-of-maps","com.wsscode.pathom.core/map-of-maps",-1598019706).cljs$core$IFn$_invoke$arity$1(cljs.core.meta(v));
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
return new cljs.core.Keyword("com.wsscode.pathom.core","map-of-maps","com.wsscode.pathom.core/map-of-maps",-1598019706).cljs$core$IFn$_invoke$arity$1(cljs.core.meta(query));
}
} else {
return and__5043__auto__;
}
})())){
if(cljs.core.truth_(com.wsscode.pathom.core.join_children_QMARK_(env,v))){
return com.wsscode.pathom.core.join_map(env,v);
} else {
return v;
}
} else {
if(cljs.core.truth_((function (){var and__5043__auto__ = cljs.core.map_QMARK_(v);
if(and__5043__auto__){
return com.wsscode.pathom.core.join_children_QMARK_(env,v);
} else {
return and__5043__auto__;
}
})())){
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$1(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(env,entity_key,v));
} else {
var G__63016 = v;
if(cljs.core.truth_(map_value_transform)){
var G__63017 = new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(ast);
var G__63018 = G__63016;
return (map_value_transform.cljs$core$IFn$_invoke$arity$2 ? map_value_transform.cljs$core$IFn$_invoke$arity$2(G__63017,G__63018) : map_value_transform.call(null,G__63017,G__63018));
} else {
return G__63016;
}
}

}
}
} else {
return new cljs.core.Keyword("com.wsscode.pathom.core","continue","com.wsscode.pathom.core/continue",591698194);
}
});
});
/**
 * Like map-reader*, but handles plain Javascript objects instead of Clojure maps.
 */
com.wsscode.pathom.core.js_obj_reader = (function com$wsscode$pathom$core$js_obj_reader(p__63019){
var map__63020 = p__63019;
var map__63020__$1 = cljs.core.__destructure_map(map__63020);
var env = map__63020__$1;
var query = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63020__$1,new cljs.core.Keyword(null,"query","query",-1288509510));
var ast = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63020__$1,new cljs.core.Keyword(null,"ast","ast",-860334068));
var js_key_transform = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__63020__$1,new cljs.core.Keyword("com.wsscode.pathom.core","js-key-transform","com.wsscode.pathom.core/js-key-transform",-1588372758),cljs.core.name);
var js_value_transform = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__63020__$1,new cljs.core.Keyword("com.wsscode.pathom.core","js-value-transform","com.wsscode.pathom.core/js-value-transform",1418749137),(function (_,v){
return v;
}));
var entity_key = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63020__$1,new cljs.core.Keyword("com.wsscode.pathom.core","entity-key","com.wsscode.pathom.core/entity-key",1494103249));
var js_key = (function (){var G__63021 = new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(ast);
return (js_key_transform.cljs$core$IFn$_invoke$arity$1 ? js_key_transform.cljs$core$IFn$_invoke$arity$1(G__63021) : js_key_transform.call(null,G__63021));
})();
var entity = com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$1(env);
if(cljs.core.truth_(com.wsscode.pathom.core.goog$module$goog$object.containsKey(entity,js_key))){
var v = com.wsscode.pathom.core.goog$module$goog$object.get(entity,js_key);
if(cljs.core.truth_(Array.isArray(v))){
if(cljs.core.truth_(query)){
return com.wsscode.pathom.core.join_seq(env,cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(v));
} else {
return v;
}
} else {
if(cljs.core.truth_((function (){var and__5043__auto__ = query;
if(cljs.core.truth_(and__5043__auto__)){
return cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(cljs.core.type(v),Object);
} else {
return and__5043__auto__;
}
})())){
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$1(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(env,entity_key,v));
} else {
var G__63022 = new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(ast);
var G__63023 = v;
return (js_value_transform.cljs$core$IFn$_invoke$arity$2 ? js_value_transform.cljs$core$IFn$_invoke$arity$2(G__63022,G__63023) : js_value_transform.call(null,G__63022,G__63023));
}
}
} else {
return new cljs.core.Keyword("com.wsscode.pathom.core","continue","com.wsscode.pathom.core/continue",591698194);
}
});
/**
 * This reader will join on any ident join, the entity for the join will be a map containing the same
 *   key and value expressed on the ident, eg: [{[:id 123] [:id]}], the join entry will be {:id 123}.
 */
com.wsscode.pathom.core.ident_join_reader = (function com$wsscode$pathom$core$ident_join_reader(env){
var temp__5802__auto__ = com.wsscode.pathom.core.ident_key(env);
if(cljs.core.truth_(temp__5802__auto__)){
var key = temp__5802__auto__;
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.createAsIfByAssoc([key,com.wsscode.pathom.core.ident_value(env)]),env);
} else {
return new cljs.core.Keyword("com.wsscode.pathom.core","continue","com.wsscode.pathom.core/continue",591698194);
}
});
/**
 * Helper to create a plugin that can view/modify the env/tx of a top-level request.
 *   f - (fn [{:keys [env tx]}] {:env new-env :tx new-tx})
 *   If the function returns no env or tx, then the parser will not be called (aborts the parse)
 */
com.wsscode.pathom.core.pre_process_parser_plugin = (function com$wsscode$pathom$core$pre_process_parser_plugin(f){
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793),(function com$wsscode$pathom$core$pre_process_parser_plugin_$_transform_parser_out_plugin_external(parser){
return (function com$wsscode$pathom$core$pre_process_parser_plugin_$_transform_parser_out_plugin_external_$_transform_parser_out_plugin_internal(env,tx){
var map__63024 = (function (){var G__63025 = new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"env","env",-1815813235),env,new cljs.core.Keyword(null,"tx","tx",466630418),tx], null);
return (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(G__63025) : f.call(null,G__63025));
})();
var map__63024__$1 = cljs.core.__destructure_map(map__63024);
var env__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63024__$1,new cljs.core.Keyword(null,"env","env",-1815813235));
var tx__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63024__$1,new cljs.core.Keyword(null,"tx","tx",466630418));
if(((cljs.core.map_QMARK_(env__$1)) && (cljs.core.seq(tx__$1)))){
return (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(env__$1,tx__$1) : parser.call(null,env__$1,tx__$1));
} else {
return cljs.core.PersistentArrayMap.EMPTY;
}
});
})], null);
});
com.wsscode.pathom.core.trans_parser_out_plugin_intn_fn = (function com$wsscode$pathom$core$trans_parser_out_plugin_intn_fn(f,parser){
return (function com$wsscode$pathom$core$trans_parser_out_plugin_intn_fn_$_transform_parser_out_plugin_internal(env,tx){
var res__53579__auto__ = (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(env,tx) : parser.call(null,env,tx));
if(com.wsscode.async.async_cljs.chan_QMARK_(res__53579__auto__)){
var c__42152__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__42153__auto__ = (function (){var switch__42120__auto__ = (function (state_63040){
var state_val_63041 = (state_63040[(1)]);
if((state_val_63041 === (1))){
var state_63040__$1 = state_63040;
var statearr_63042_63864 = state_63040__$1;
(statearr_63042_63864[(2)] = null);

(statearr_63042_63864[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63041 === (2))){
var _ = (function (){var statearr_63043 = state_63040;
(statearr_63043[(4)] = cljs.core.cons((5),(state_63040[(4)])));

return statearr_63043;
})();
var state_63040__$1 = state_63040;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_63040__$1,(6),res__53579__auto__);
} else {
if((state_val_63041 === (3))){
var inst_63038 = (state_63040[(2)]);
var state_63040__$1 = state_63040;
return cljs.core.async.impl.ioc_helpers.return_chan(state_63040__$1,inst_63038);
} else {
if((state_val_63041 === (4))){
var inst_63026 = (state_63040[(2)]);
var state_63040__$1 = state_63040;
var statearr_63045_63865 = state_63040__$1;
(statearr_63045_63865[(2)] = inst_63026);

(statearr_63045_63865[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63041 === (5))){
var _ = (function (){var statearr_63046 = state_63040;
(statearr_63046[(4)] = cljs.core.rest((state_63040[(4)])));

return statearr_63046;
})();
var state_63040__$1 = state_63040;
var ex63044 = (state_63040__$1[(2)]);
var statearr_63047_63866 = state_63040__$1;
(statearr_63047_63866[(5)] = ex63044);


var statearr_63048_63867 = state_63040__$1;
(statearr_63048_63867[(1)] = (4));

(statearr_63048_63867[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63041 === (6))){
var inst_63033 = (state_63040[(2)]);
var inst_63034 = com.wsscode.async.async_cljs.throw_err(inst_63033);
var inst_63035 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_63034) : f.call(null,inst_63034));
var _ = (function (){var statearr_63049 = state_63040;
(statearr_63049[(4)] = cljs.core.rest((state_63040[(4)])));

return statearr_63049;
})();
var state_63040__$1 = state_63040;
var statearr_63050_63868 = state_63040__$1;
(statearr_63050_63868[(2)] = inst_63035);

(statearr_63050_63868[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$trans_parser_out_plugin_intn_fn_$_transform_parser_out_plugin_internal_$_state_machine__42121__auto__ = null;
var com$wsscode$pathom$core$trans_parser_out_plugin_intn_fn_$_transform_parser_out_plugin_internal_$_state_machine__42121__auto____0 = (function (){
var statearr_63051 = [null,null,null,null,null,null,null];
(statearr_63051[(0)] = com$wsscode$pathom$core$trans_parser_out_plugin_intn_fn_$_transform_parser_out_plugin_internal_$_state_machine__42121__auto__);

(statearr_63051[(1)] = (1));

return statearr_63051;
});
var com$wsscode$pathom$core$trans_parser_out_plugin_intn_fn_$_transform_parser_out_plugin_internal_$_state_machine__42121__auto____1 = (function (state_63040){
while(true){
var ret_value__42122__auto__ = (function (){try{while(true){
var result__42123__auto__ = switch__42120__auto__(state_63040);
if(cljs.core.keyword_identical_QMARK_(result__42123__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__42123__auto__;
}
break;
}
}catch (e63052){var ex__42124__auto__ = e63052;
var statearr_63053_63869 = state_63040;
(statearr_63053_63869[(2)] = ex__42124__auto__);


if(cljs.core.seq((state_63040[(4)]))){
var statearr_63054_63870 = state_63040;
(statearr_63054_63870[(1)] = cljs.core.first((state_63040[(4)])));

} else {
throw ex__42124__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__42122__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__63871 = state_63040;
state_63040 = G__63871;
continue;
} else {
return ret_value__42122__auto__;
}
break;
}
});
com$wsscode$pathom$core$trans_parser_out_plugin_intn_fn_$_transform_parser_out_plugin_internal_$_state_machine__42121__auto__ = function(state_63040){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$trans_parser_out_plugin_intn_fn_$_transform_parser_out_plugin_internal_$_state_machine__42121__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$trans_parser_out_plugin_intn_fn_$_transform_parser_out_plugin_internal_$_state_machine__42121__auto____1.call(this,state_63040);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$trans_parser_out_plugin_intn_fn_$_transform_parser_out_plugin_internal_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$trans_parser_out_plugin_intn_fn_$_transform_parser_out_plugin_internal_$_state_machine__42121__auto____0;
com$wsscode$pathom$core$trans_parser_out_plugin_intn_fn_$_transform_parser_out_plugin_internal_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$trans_parser_out_plugin_intn_fn_$_transform_parser_out_plugin_internal_$_state_machine__42121__auto____1;
return com$wsscode$pathom$core$trans_parser_out_plugin_intn_fn_$_transform_parser_out_plugin_internal_$_state_machine__42121__auto__;
})()
})();
var state__42154__auto__ = (function (){var statearr_63055 = f__42153__auto__();
(statearr_63055[(6)] = c__42152__auto__);

return statearr_63055;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__42154__auto__);
}));

return c__42152__auto__;
} else {
var res = res__53579__auto__;
return (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(res) : f.call(null,res));
}
});
});
com.wsscode.pathom.core.trans_parser_out_plugin_ext_fn = (function com$wsscode$pathom$core$trans_parser_out_plugin_ext_fn(f){
return (function com$wsscode$pathom$core$trans_parser_out_plugin_ext_fn_$_transform_parser_out_plugin_external(parser){
return com.wsscode.pathom.core.trans_parser_out_plugin_intn_fn(f,parser);
});
});
/**
 * Helper to create a plugin to work on the parser output. `f` will run once with the parser final result.
 */
com.wsscode.pathom.core.post_process_parser_plugin = (function com$wsscode$pathom$core$post_process_parser_plugin(f){
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793),com.wsscode.pathom.core.trans_parser_out_plugin_ext_fn(f)], null);
});
com.wsscode.pathom.core.elide_special_outputs_plugin = com.wsscode.pathom.core.post_process_parser_plugin(com.wsscode.pathom.core.elide_special_outputs);
com.wsscode.pathom.core.error_message = (function com$wsscode$pathom$core$error_message(err){
return err.message;
});
com.wsscode.pathom.core.error_str = (function com$wsscode$pathom$core$error_str(err){
var msg = err.message;
var data = cljs.core.ex_data(err);
var G__63056 = msg;
if(cljs.core.truth_(data)){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__63056)," - ",cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([data], 0))].join('');
} else {
return G__63056;
}
});
/**
 * Helper function to update a mutation action.
 */
com.wsscode.pathom.core.update_action = (function com$wsscode$pathom$core$update_action(m,f){
if(cljs.core.contains_QMARK_(m,new cljs.core.Keyword(null,"action","action",-811238024))){
return cljs.core.update.cljs$core$IFn$_invoke$arity$3(m,new cljs.core.Keyword(null,"action","action",-811238024),f);
} else {
return m;
}
});
com.wsscode.pathom.core.process_error = (function com$wsscode$pathom$core$process_error(p__63057,e){
var map__63058 = p__63057;
var map__63058__$1 = cljs.core.__destructure_map(map__63058);
var env = map__63058__$1;
var process_error = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63058__$1,new cljs.core.Keyword("com.wsscode.pathom.core","process-error","com.wsscode.pathom.core/process-error",-2116719411));
if(cljs.core.truth_(process_error)){
return (process_error.cljs$core$IFn$_invoke$arity$2 ? process_error.cljs$core$IFn$_invoke$arity$2(env,e) : process_error.call(null,env,e));
} else {
return com.wsscode.pathom.core.error_str(e);
}
});
com.wsscode.pathom.core.add_error = (function com$wsscode$pathom$core$add_error(p__63059,e){
var map__63060 = p__63059;
var map__63060__$1 = cljs.core.__destructure_map(map__63060);
var env = map__63060__$1;
var errors_STAR_ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63060__$1,new cljs.core.Keyword("com.wsscode.pathom.core","errors*","com.wsscode.pathom.core/errors*",337011276));
var path = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63060__$1,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661));
if(cljs.core.truth_(errors_STAR_)){
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(errors_STAR_,cljs.core.assoc,path,com.wsscode.pathom.core.process_error(env,e));
} else {
}

return new cljs.core.Keyword("com.wsscode.pathom.core","reader-error","com.wsscode.pathom.core/reader-error",-803587882);
});
com.wsscode.pathom.core.wrap_handle_exception = (function com$wsscode$pathom$core$wrap_handle_exception(reader){
return (function com$wsscode$pathom$core$wrap_handle_exception_$_wrap_handle_exception_internal(p__63061){
var map__63062 = p__63061;
var map__63062__$1 = cljs.core.__destructure_map(map__63062);
var env = map__63062__$1;
var fail_fast_QMARK_ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63062__$1,new cljs.core.Keyword("com.wsscode.pathom.core","fail-fast?","com.wsscode.pathom.core/fail-fast?",-272943465));
if(cljs.core.truth_(fail_fast_QMARK_)){
return (reader.cljs$core$IFn$_invoke$arity$1 ? reader.cljs$core$IFn$_invoke$arity$1(env) : reader.call(null,env));
} else {
try{var x = (reader.cljs$core$IFn$_invoke$arity$1 ? reader.cljs$core$IFn$_invoke$arity$1(env) : reader.call(null,env));
if(com.wsscode.async.async_cljs.chan_QMARK_(x)){
var c__42152__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__42153__auto__ = (function (){var switch__42120__auto__ = (function (state_63078){
var state_val_63079 = (state_63078[(1)]);
if((state_val_63079 === (1))){
var state_63078__$1 = state_63078;
var statearr_63080_63873 = state_63078__$1;
(statearr_63080_63873[(2)] = null);

(statearr_63080_63873[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63079 === (2))){
var _ = (function (){var statearr_63081 = state_63078;
(statearr_63081[(4)] = cljs.core.cons((5),(state_63078[(4)])));

return statearr_63081;
})();
var state_63078__$1 = state_63078;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_63078__$1,(6),x);
} else {
if((state_val_63079 === (3))){
var inst_63076 = (state_63078[(2)]);
var state_63078__$1 = state_63078;
return cljs.core.async.impl.ioc_helpers.return_chan(state_63078__$1,inst_63076);
} else {
if((state_val_63079 === (4))){
var inst_63064 = (state_63078[(2)]);
var inst_63065 = com.wsscode.pathom.core.add_error(env,inst_63064);
var state_63078__$1 = state_63078;
var statearr_63083_63874 = state_63078__$1;
(statearr_63083_63874[(2)] = inst_63065);

(statearr_63083_63874[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63079 === (5))){
var _ = (function (){var statearr_63084 = state_63078;
(statearr_63084[(4)] = cljs.core.rest((state_63078[(4)])));

return statearr_63084;
})();
var state_63078__$1 = state_63078;
var ex63082 = (state_63078__$1[(2)]);
var statearr_63085_63875 = state_63078__$1;
(statearr_63085_63875[(5)] = ex63082);


var statearr_63086_63876 = state_63078__$1;
(statearr_63086_63876[(1)] = (4));

(statearr_63086_63876[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63079 === (6))){
var inst_63072 = (state_63078[(2)]);
var inst_63073 = com.wsscode.async.async_cljs.throw_err(inst_63072);
var _ = (function (){var statearr_63087 = state_63078;
(statearr_63087[(4)] = cljs.core.rest((state_63078[(4)])));

return statearr_63087;
})();
var state_63078__$1 = state_63078;
var statearr_63088_63877 = state_63078__$1;
(statearr_63088_63877[(2)] = inst_63073);

(statearr_63088_63877[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$wrap_handle_exception_$_wrap_handle_exception_internal_$_state_machine__42121__auto__ = null;
var com$wsscode$pathom$core$wrap_handle_exception_$_wrap_handle_exception_internal_$_state_machine__42121__auto____0 = (function (){
var statearr_63089 = [null,null,null,null,null,null,null];
(statearr_63089[(0)] = com$wsscode$pathom$core$wrap_handle_exception_$_wrap_handle_exception_internal_$_state_machine__42121__auto__);

(statearr_63089[(1)] = (1));

return statearr_63089;
});
var com$wsscode$pathom$core$wrap_handle_exception_$_wrap_handle_exception_internal_$_state_machine__42121__auto____1 = (function (state_63078){
while(true){
var ret_value__42122__auto__ = (function (){try{while(true){
var result__42123__auto__ = switch__42120__auto__(state_63078);
if(cljs.core.keyword_identical_QMARK_(result__42123__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__42123__auto__;
}
break;
}
}catch (e63090){var ex__42124__auto__ = e63090;
var statearr_63091_63878 = state_63078;
(statearr_63091_63878[(2)] = ex__42124__auto__);


if(cljs.core.seq((state_63078[(4)]))){
var statearr_63092_63879 = state_63078;
(statearr_63092_63879[(1)] = cljs.core.first((state_63078[(4)])));

} else {
throw ex__42124__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__42122__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__63880 = state_63078;
state_63078 = G__63880;
continue;
} else {
return ret_value__42122__auto__;
}
break;
}
});
com$wsscode$pathom$core$wrap_handle_exception_$_wrap_handle_exception_internal_$_state_machine__42121__auto__ = function(state_63078){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$wrap_handle_exception_$_wrap_handle_exception_internal_$_state_machine__42121__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$wrap_handle_exception_$_wrap_handle_exception_internal_$_state_machine__42121__auto____1.call(this,state_63078);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$wrap_handle_exception_$_wrap_handle_exception_internal_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$wrap_handle_exception_$_wrap_handle_exception_internal_$_state_machine__42121__auto____0;
com$wsscode$pathom$core$wrap_handle_exception_$_wrap_handle_exception_internal_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$wrap_handle_exception_$_wrap_handle_exception_internal_$_state_machine__42121__auto____1;
return com$wsscode$pathom$core$wrap_handle_exception_$_wrap_handle_exception_internal_$_state_machine__42121__auto__;
})()
})();
var state__42154__auto__ = (function (){var statearr_63093 = f__42153__auto__();
(statearr_63093[(6)] = c__42152__auto__);

return statearr_63093;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__42154__auto__);
}));

return c__42152__auto__;
} else {
return x;
}
}catch (e63063){var e = e63063;
return com.wsscode.pathom.core.add_error(env,e);
}}
});
});
com.wsscode.pathom.core.build_mutate_handle_exception_fn = (function com$wsscode$pathom$core$build_mutate_handle_exception_fn(env){
return (function (action){
return (function (){
try{var res = (action.cljs$core$IFn$_invoke$arity$0 ? action.cljs$core$IFn$_invoke$arity$0() : action.call(null));
if(com.wsscode.async.async_cljs.chan_QMARK_(res)){
var c__42152__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__42153__auto__ = (function (){var switch__42120__auto__ = (function (state_63117){
var state_val_63118 = (state_63117[(1)]);
if((state_val_63118 === (7))){
var inst_63104 = (state_63117[(2)]);
var state_63117__$1 = state_63117;
var statearr_63119_63881 = state_63117__$1;
(statearr_63119_63881[(2)] = inst_63104);

(statearr_63119_63881[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63118 === (1))){
var state_63117__$1 = state_63117;
var statearr_63120_63882 = state_63117__$1;
(statearr_63120_63882[(2)] = null);

(statearr_63120_63882[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63118 === (4))){
var inst_63095 = (state_63117[(2)]);
var state_63117__$1 = (function (){var statearr_63121 = state_63117;
(statearr_63121[(7)] = inst_63095);

return statearr_63121;
})();
if(cljs.core.truth_(com.wsscode.pathom.core.process_error)){
var statearr_63122_63883 = state_63117__$1;
(statearr_63122_63883[(1)] = (5));

} else {
var statearr_63123_63884 = state_63117__$1;
(statearr_63123_63884[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63118 === (6))){
var inst_63095 = (state_63117[(7)]);
var inst_63099 = [new cljs.core.Keyword("com.wsscode.pathom.core","reader-error","com.wsscode.pathom.core/reader-error",-803587882)];
var inst_63100 = com.wsscode.pathom.core.error_str(inst_63095);
var inst_63101 = [inst_63100];
var inst_63102 = cljs.core.PersistentHashMap.fromArrays(inst_63099,inst_63101);
var state_63117__$1 = state_63117;
var statearr_63124_63885 = state_63117__$1;
(statearr_63124_63885[(2)] = inst_63102);

(statearr_63124_63885[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63118 === (3))){
var inst_63115 = (state_63117[(2)]);
var state_63117__$1 = state_63117;
return cljs.core.async.impl.ioc_helpers.return_chan(state_63117__$1,inst_63115);
} else {
if((state_val_63118 === (2))){
var _ = (function (){var statearr_63125 = state_63117;
(statearr_63125[(4)] = cljs.core.cons((8),(state_63117[(4)])));

return statearr_63125;
})();
var state_63117__$1 = state_63117;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_63117__$1,(9),res);
} else {
if((state_val_63118 === (9))){
var inst_63111 = (state_63117[(2)]);
var inst_63112 = com.wsscode.async.async_cljs.throw_err(inst_63111);
var _ = (function (){var statearr_63127 = state_63117;
(statearr_63127[(4)] = cljs.core.rest((state_63117[(4)])));

return statearr_63127;
})();
var state_63117__$1 = state_63117;
var statearr_63128_63886 = state_63117__$1;
(statearr_63128_63886[(2)] = inst_63112);

(statearr_63128_63886[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63118 === (5))){
var inst_63095 = (state_63117[(7)]);
var inst_63097 = com.wsscode.pathom.core.process_error(env,inst_63095);
var state_63117__$1 = state_63117;
var statearr_63129_63887 = state_63117__$1;
(statearr_63129_63887[(2)] = inst_63097);

(statearr_63129_63887[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63118 === (8))){
var _ = (function (){var statearr_63130 = state_63117;
(statearr_63130[(4)] = cljs.core.rest((state_63117[(4)])));

return statearr_63130;
})();
var state_63117__$1 = state_63117;
var ex63126 = (state_63117__$1[(2)]);
var statearr_63131_63888 = state_63117__$1;
(statearr_63131_63888[(5)] = ex63126);


var statearr_63132_63889 = state_63117__$1;
(statearr_63132_63889[(1)] = (4));

(statearr_63132_63889[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$build_mutate_handle_exception_fn_$_state_machine__42121__auto__ = null;
var com$wsscode$pathom$core$build_mutate_handle_exception_fn_$_state_machine__42121__auto____0 = (function (){
var statearr_63133 = [null,null,null,null,null,null,null,null];
(statearr_63133[(0)] = com$wsscode$pathom$core$build_mutate_handle_exception_fn_$_state_machine__42121__auto__);

(statearr_63133[(1)] = (1));

return statearr_63133;
});
var com$wsscode$pathom$core$build_mutate_handle_exception_fn_$_state_machine__42121__auto____1 = (function (state_63117){
while(true){
var ret_value__42122__auto__ = (function (){try{while(true){
var result__42123__auto__ = switch__42120__auto__(state_63117);
if(cljs.core.keyword_identical_QMARK_(result__42123__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__42123__auto__;
}
break;
}
}catch (e63134){var ex__42124__auto__ = e63134;
var statearr_63135_63890 = state_63117;
(statearr_63135_63890[(2)] = ex__42124__auto__);


if(cljs.core.seq((state_63117[(4)]))){
var statearr_63136_63891 = state_63117;
(statearr_63136_63891[(1)] = cljs.core.first((state_63117[(4)])));

} else {
throw ex__42124__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__42122__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__63897 = state_63117;
state_63117 = G__63897;
continue;
} else {
return ret_value__42122__auto__;
}
break;
}
});
com$wsscode$pathom$core$build_mutate_handle_exception_fn_$_state_machine__42121__auto__ = function(state_63117){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$build_mutate_handle_exception_fn_$_state_machine__42121__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$build_mutate_handle_exception_fn_$_state_machine__42121__auto____1.call(this,state_63117);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$build_mutate_handle_exception_fn_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$build_mutate_handle_exception_fn_$_state_machine__42121__auto____0;
com$wsscode$pathom$core$build_mutate_handle_exception_fn_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$build_mutate_handle_exception_fn_$_state_machine__42121__auto____1;
return com$wsscode$pathom$core$build_mutate_handle_exception_fn_$_state_machine__42121__auto__;
})()
})();
var state__42154__auto__ = (function (){var statearr_63137 = f__42153__auto__();
(statearr_63137[(6)] = c__42152__auto__);

return statearr_63137;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__42154__auto__);
}));

return c__42152__auto__;
} else {
return res;
}
}catch (e63094){var e = e63094;
if(cljs.core.truth_(com.wsscode.pathom.core.process_error)){
return com.wsscode.pathom.core.process_error(env,e);
} else {
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("com.wsscode.pathom.core","reader-error","com.wsscode.pathom.core/reader-error",-803587882),com.wsscode.pathom.core.error_str(e)], null);
}
}});
});
});
com.wsscode.pathom.core.wrap_mutate_handle_exception = (function com$wsscode$pathom$core$wrap_mutate_handle_exception(mutate){
return (function com$wsscode$pathom$core$wrap_mutate_handle_exception_$_wrap_mutate_handle_exception_internal(p__63138,k,p){
var map__63139 = p__63138;
var map__63139__$1 = cljs.core.__destructure_map(map__63139);
var env = map__63139__$1;
var process_error = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63139__$1,new cljs.core.Keyword("com.wsscode.pathom.core","process-error","com.wsscode.pathom.core/process-error",-2116719411));
var fail_fast_QMARK_ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63139__$1,new cljs.core.Keyword("com.wsscode.pathom.core","fail-fast?","com.wsscode.pathom.core/fail-fast?",-272943465));
if(cljs.core.truth_(fail_fast_QMARK_)){
return (mutate.cljs$core$IFn$_invoke$arity$3 ? mutate.cljs$core$IFn$_invoke$arity$3(env,k,p) : mutate.call(null,env,k,p));
} else {
try{return com.wsscode.pathom.core.update_action((mutate.cljs$core$IFn$_invoke$arity$3 ? mutate.cljs$core$IFn$_invoke$arity$3(env,k,p) : mutate.call(null,env,k,p)),com.wsscode.pathom.core.build_mutate_handle_exception_fn(env));
}catch (e63140){var e = e63140;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"action","action",-811238024),(function (){
if(cljs.core.truth_(process_error)){
return (process_error.cljs$core$IFn$_invoke$arity$2 ? process_error.cljs$core$IFn$_invoke$arity$2(env,e) : process_error.call(null,env,e));
} else {
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("com.wsscode.pathom.core","reader-error","com.wsscode.pathom.core/reader-error",-803587882),com.wsscode.pathom.core.error_str(e)], null);
}
})], null);
}}
});
});
com.wsscode.pathom.core.wrap_parser_exception = (function com$wsscode$pathom$core$wrap_parser_exception(parser){
return (function com$wsscode$pathom$core$wrap_parser_exception_$_wrap_parser_exception_internal(env,tx){
var errors = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var res__53579__auto__ = (function (){var G__63141 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(env,new cljs.core.Keyword("com.wsscode.pathom.core","errors*","com.wsscode.pathom.core/errors*",337011276),errors);
var G__63142 = tx;
return (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(G__63141,G__63142) : parser.call(null,G__63141,G__63142));
})();
if(com.wsscode.async.async_cljs.chan_QMARK_(res__53579__auto__)){
var c__42152__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__42153__auto__ = (function (){var switch__42120__auto__ = (function (state_63165){
var state_val_63166 = (state_63165[(1)]);
if((state_val_63166 === (7))){
var inst_63151 = (state_63165[(7)]);
var inst_63156 = cljs.core.deref(errors);
var inst_63157 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(inst_63151,new cljs.core.Keyword("com.wsscode.pathom.core","errors","com.wsscode.pathom.core/errors",-1179549217),inst_63156);
var state_63165__$1 = state_63165;
var statearr_63167_63901 = state_63165__$1;
(statearr_63167_63901[(2)] = inst_63157);

(statearr_63167_63901[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63166 === (1))){
var state_63165__$1 = state_63165;
var statearr_63168_63902 = state_63165__$1;
(statearr_63168_63902[(2)] = null);

(statearr_63168_63902[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63166 === (4))){
var inst_63143 = (state_63165[(2)]);
var state_63165__$1 = state_63165;
var statearr_63169_63903 = state_63165__$1;
(statearr_63169_63903[(2)] = inst_63143);

(statearr_63169_63903[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63166 === (6))){
var inst_63150 = (state_63165[(2)]);
var inst_63151 = com.wsscode.async.async_cljs.throw_err(inst_63150);
var inst_63153 = cljs.core.deref(errors);
var inst_63154 = cljs.core.seq(inst_63153);
var state_63165__$1 = (function (){var statearr_63170 = state_63165;
(statearr_63170[(7)] = inst_63151);

return statearr_63170;
})();
if(inst_63154){
var statearr_63171_63904 = state_63165__$1;
(statearr_63171_63904[(1)] = (7));

} else {
var statearr_63172_63905 = state_63165__$1;
(statearr_63172_63905[(1)] = (8));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63166 === (3))){
var inst_63163 = (state_63165[(2)]);
var state_63165__$1 = state_63165;
return cljs.core.async.impl.ioc_helpers.return_chan(state_63165__$1,inst_63163);
} else {
if((state_val_63166 === (2))){
var _ = (function (){var statearr_63174 = state_63165;
(statearr_63174[(4)] = cljs.core.cons((5),(state_63165[(4)])));

return statearr_63174;
})();
var state_63165__$1 = state_63165;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_63165__$1,(6),res__53579__auto__);
} else {
if((state_val_63166 === (9))){
var inst_63160 = (state_63165[(2)]);
var _ = (function (){var statearr_63175 = state_63165;
(statearr_63175[(4)] = cljs.core.rest((state_63165[(4)])));

return statearr_63175;
})();
var state_63165__$1 = state_63165;
var statearr_63176_63907 = state_63165__$1;
(statearr_63176_63907[(2)] = inst_63160);

(statearr_63176_63907[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63166 === (5))){
var _ = (function (){var statearr_63177 = state_63165;
(statearr_63177[(4)] = cljs.core.rest((state_63165[(4)])));

return statearr_63177;
})();
var state_63165__$1 = state_63165;
var ex63173 = (state_63165__$1[(2)]);
var statearr_63178_63908 = state_63165__$1;
(statearr_63178_63908[(5)] = ex63173);


var statearr_63179_63909 = state_63165__$1;
(statearr_63179_63909[(1)] = (4));

(statearr_63179_63909[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63166 === (8))){
var inst_63151 = (state_63165[(7)]);
var state_63165__$1 = state_63165;
var statearr_63180_63910 = state_63165__$1;
(statearr_63180_63910[(2)] = inst_63151);

(statearr_63180_63910[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$wrap_parser_exception_$_wrap_parser_exception_internal_$_state_machine__42121__auto__ = null;
var com$wsscode$pathom$core$wrap_parser_exception_$_wrap_parser_exception_internal_$_state_machine__42121__auto____0 = (function (){
var statearr_63181 = [null,null,null,null,null,null,null,null];
(statearr_63181[(0)] = com$wsscode$pathom$core$wrap_parser_exception_$_wrap_parser_exception_internal_$_state_machine__42121__auto__);

(statearr_63181[(1)] = (1));

return statearr_63181;
});
var com$wsscode$pathom$core$wrap_parser_exception_$_wrap_parser_exception_internal_$_state_machine__42121__auto____1 = (function (state_63165){
while(true){
var ret_value__42122__auto__ = (function (){try{while(true){
var result__42123__auto__ = switch__42120__auto__(state_63165);
if(cljs.core.keyword_identical_QMARK_(result__42123__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__42123__auto__;
}
break;
}
}catch (e63182){var ex__42124__auto__ = e63182;
var statearr_63183_63911 = state_63165;
(statearr_63183_63911[(2)] = ex__42124__auto__);


if(cljs.core.seq((state_63165[(4)]))){
var statearr_63184_63912 = state_63165;
(statearr_63184_63912[(1)] = cljs.core.first((state_63165[(4)])));

} else {
throw ex__42124__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__42122__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__63913 = state_63165;
state_63165 = G__63913;
continue;
} else {
return ret_value__42122__auto__;
}
break;
}
});
com$wsscode$pathom$core$wrap_parser_exception_$_wrap_parser_exception_internal_$_state_machine__42121__auto__ = function(state_63165){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$wrap_parser_exception_$_wrap_parser_exception_internal_$_state_machine__42121__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$wrap_parser_exception_$_wrap_parser_exception_internal_$_state_machine__42121__auto____1.call(this,state_63165);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$wrap_parser_exception_$_wrap_parser_exception_internal_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$wrap_parser_exception_$_wrap_parser_exception_internal_$_state_machine__42121__auto____0;
com$wsscode$pathom$core$wrap_parser_exception_$_wrap_parser_exception_internal_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$wrap_parser_exception_$_wrap_parser_exception_internal_$_state_machine__42121__auto____1;
return com$wsscode$pathom$core$wrap_parser_exception_$_wrap_parser_exception_internal_$_state_machine__42121__auto__;
})()
})();
var state__42154__auto__ = (function (){var statearr_63185 = f__42153__auto__();
(statearr_63185[(6)] = c__42152__auto__);

return statearr_63185;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__42154__auto__);
}));

return c__42152__auto__;
} else {
var res = res__53579__auto__;
var G__63186 = res;
if(cljs.core.seq(cljs.core.deref(errors))){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__63186,new cljs.core.Keyword("com.wsscode.pathom.core","errors","com.wsscode.pathom.core/errors",-1179549217),cljs.core.deref(errors));
} else {
return G__63186;
}
}
});
});
/**
 * Wrap reads with try-catch and put any errors under `::p/errors` (including the path),
 * setting the value of the errored node to `::p/reader-error`.
 * 
 *   You can customize how the error is exported into the `::p/errors` map by setting the key
 *   `::p/process-error` in your environment to a function of [env, err] -> data.
 */
com.wsscode.pathom.core.error_handler_plugin = new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword("com.wsscode.pathom.core","wrap-read","com.wsscode.pathom.core/wrap-read",-904142261),com.wsscode.pathom.core.wrap_handle_exception,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793),com.wsscode.pathom.core.wrap_parser_exception,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-mutate","com.wsscode.pathom.core/wrap-mutate",989863202),com.wsscode.pathom.core.wrap_mutate_handle_exception], null);
com.wsscode.pathom.core.trace_plugin = com.wsscode.pathom.trace.trace_plugin;
/**
 * Reduces the error path to the last available nesting on the map m.
 */
com.wsscode.pathom.core.collapse_error_path = (function com$wsscode$pathom$core$collapse_error_path(m,path){
return cljs.core.vec((function (){var path_SINGLEQUOTE_ = path;
while(true){
if((cljs.core.count(path_SINGLEQUOTE_) === (0))){
return cljs.core.take.cljs$core$IFn$_invoke$arity$2((1),path);
} else {
if(cljs.core.truth_(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(m,path_SINGLEQUOTE_))){
return path_SINGLEQUOTE_;
} else {
var G__63922 = cljs.core.butlast(path_SINGLEQUOTE_);
path_SINGLEQUOTE_ = G__63922;
continue;
}
}
break;
}
})());
});
/**
 * Extract errors from the data root and inject those in the same level where
 * the error item is present. For example:
 * 
 * {:query {:item :com.wsscode.pathom/reader-error}
 *  :com.wsscode.pathom.core/errors
 *  {[:query :item] {:error "some error"}}}
 * 
 * Is turned into:
 * 
 * {:query {:item :com.wsscode.pathom/reader-error
 *          :com.wsscode.pathom.core/errors {:item {:error "some error"}}}
 * 
 * This makes easier to reach for the error when rendering the UI.
 * 
 * Use it e.g. via [[p/post-process-parser-plugin]], after the [[p/error-handler-plugin]].
 */
com.wsscode.pathom.core.raise_errors = (function com$wsscode$pathom$core$raise_errors(data){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (m,p__63187){
var vec__63188 = p__63187;
var path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__63188,(0),null);
var err = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__63188,(1),null);
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("com.wsscode.pathom.core","reader-error","com.wsscode.pathom.core/reader-error",-803587882),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(m,path))){
var path_SINGLEQUOTE_ = cljs.core.concat.cljs$core$IFn$_invoke$arity$2(cljs.core.butlast(path),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","errors","com.wsscode.pathom.core/errors",-1179549217),cljs.core.last(path)], null));
return cljs.core.assoc_in(m,path_SINGLEQUOTE_,err);
} else {
return m;
}
}),cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(data,new cljs.core.Keyword("com.wsscode.pathom.core","errors","com.wsscode.pathom.core/errors",-1179549217)),cljs.core.get.cljs$core$IFn$_invoke$arity$2(data,new cljs.core.Keyword("com.wsscode.pathom.core","errors","com.wsscode.pathom.core/errors",-1179549217)));
});
/**
 * Mutations running through a parser all come back in a map like this {'my/mutation {:result {...}}}. This function
 *   converts that to {'my/mutation {...}}. Copied from fulcro.server.
 */
com.wsscode.pathom.core.raise_response = (function com$wsscode$pathom$core$raise_response(resp){
return clojure.walk.prewalk((function (x){
if(cljs.core.map_QMARK_(x)){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (acc,p__63191){
var vec__63192 = p__63191;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__63192,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__63192,(1),null);
if((((k instanceof cljs.core.Symbol)) && ((!((new cljs.core.Keyword(null,"result","result",1415092211).cljs$core$IFn$_invoke$arity$1(v) == null)))))){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(acc,k,new cljs.core.Keyword(null,"result","result",1415092211).cljs$core$IFn$_invoke$arity$1(v));
} else {
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(acc,k,v);
}
}),cljs.core.PersistentArrayMap.EMPTY,x);
} else {
return x;
}
}),resp);
});
com.wsscode.pathom.core.raise_mutation_result_plugin = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793),(function com$wsscode$pathom$core$raise_mutation_result_wrap_parser(parser){
return (function com$wsscode$pathom$core$raise_mutation_result_wrap_parser_$_raise_mutation_result_wrap_internal(env,tx){
return com.wsscode.pathom.core.raise_response((parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(env,tx) : parser.call(null,env,tx)));
});
})], null);
com.wsscode.pathom.core.env_plugin = (function com$wsscode$pathom$core$env_plugin(extra_env){
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793),(function com$wsscode$pathom$core$env_plugin_$_env_plugin_wrap_parser(parser){
return (function com$wsscode$pathom$core$env_plugin_$_env_plugin_wrap_parser_$_env_plugin_wrap_internal(env,tx){
var G__63195 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([extra_env,env], 0));
var G__63196 = tx;
return (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(G__63195,G__63196) : parser.call(null,G__63195,G__63196));
});
})], null);
});
/**
 * This plugin receives a function that will be called to wrap the current
 *   enviroment each time the main parser is called (parser level).
 */
com.wsscode.pathom.core.env_wrap_plugin = (function com$wsscode$pathom$core$env_wrap_plugin(extra_env_wrapper){
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793),(function com$wsscode$pathom$core$env_wrap_plugin_$_env_wrap_wrap_parser(parser){
return (function com$wsscode$pathom$core$env_wrap_plugin_$_env_wrap_wrap_parser_$_env_wrap_wrap_internal(env,tx){
var G__63197 = (extra_env_wrapper.cljs$core$IFn$_invoke$arity$1 ? extra_env_wrapper.cljs$core$IFn$_invoke$arity$1(env) : extra_env_wrapper.call(null,env));
var G__63198 = tx;
return (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(G__63197,G__63198) : parser.call(null,G__63197,G__63198));
});
})], null);
});
/**
 * DEPRECATED not required anymore, this was integrated in the main engine.
 */
com.wsscode.pathom.core.request_cache_plugin = cljs.core.PersistentArrayMap.EMPTY;
com.wsscode.pathom.core.cached_STAR_ = (function com$wsscode$pathom$core$cached_STAR_(env,key,body_fn){
var temp__5802__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(env,new cljs.core.Keyword("com.wsscode.pathom.core","request-cache","com.wsscode.pathom.core/request-cache",1469884617));
if(cljs.core.truth_(temp__5802__auto__)){
var cache = temp__5802__auto__;
var temp__5802__auto____$1 = cljs.core.find(cljs.core.deref(cache),key);
if(cljs.core.truth_(temp__5802__auto____$1)){
var vec__63199 = temp__5802__auto____$1;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__63199,(0),null);
var hit = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__63199,(1),null);
com.wsscode.pathom.trace.trace(env,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword("com.wsscode.pathom.trace","event","com.wsscode.pathom.trace/event",1287398983),new cljs.core.Keyword("com.wsscode.pathom.core","cache-hit","com.wsscode.pathom.core/cache-hit",1851998374),new cljs.core.Keyword("com.wsscode.pathom.core","cache-key","com.wsscode.pathom.core/cache-key",246159991),key], null));

return com.wsscode.async.async_cljs.throw_err(hit);
} else {
com.wsscode.pathom.trace.trace(env,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword("com.wsscode.pathom.trace","event","com.wsscode.pathom.trace/event",1287398983),new cljs.core.Keyword("com.wsscode.pathom.core","cache-miss","com.wsscode.pathom.core/cache-miss",1311426337),new cljs.core.Keyword("com.wsscode.pathom.core","cache-key","com.wsscode.pathom.core/cache-key",246159991),key], null));

var res__53579__auto__ = (function (){try{return (body_fn.cljs$core$IFn$_invoke$arity$0 ? body_fn.cljs$core$IFn$_invoke$arity$0() : body_fn.call(null));
}catch (e63202){var e = e63202;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(cache,cljs.core.assoc,key,e);

throw e;
}})();
if(com.wsscode.async.async_cljs.chan_QMARK_(res__53579__auto__)){
var c__42152__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__42153__auto__ = (function (){var switch__42120__auto__ = (function (state_63217){
var state_val_63218 = (state_63217[(1)]);
if((state_val_63218 === (1))){
var state_63217__$1 = state_63217;
var statearr_63219_63923 = state_63217__$1;
(statearr_63219_63923[(2)] = null);

(statearr_63219_63923[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63218 === (2))){
var _ = (function (){var statearr_63220 = state_63217;
(statearr_63220[(4)] = cljs.core.cons((5),(state_63217[(4)])));

return statearr_63220;
})();
var state_63217__$1 = state_63217;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_63217__$1,(6),res__53579__auto__);
} else {
if((state_val_63218 === (3))){
var inst_63215 = (state_63217[(2)]);
var state_63217__$1 = state_63217;
return cljs.core.async.impl.ioc_helpers.return_chan(state_63217__$1,inst_63215);
} else {
if((state_val_63218 === (4))){
var inst_63203 = (state_63217[(2)]);
var state_63217__$1 = state_63217;
var statearr_63222_63924 = state_63217__$1;
(statearr_63222_63924[(2)] = inst_63203);

(statearr_63222_63924[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63218 === (5))){
var _ = (function (){var statearr_63223 = state_63217;
(statearr_63223[(4)] = cljs.core.rest((state_63217[(4)])));

return statearr_63223;
})();
var state_63217__$1 = state_63217;
var ex63221 = (state_63217__$1[(2)]);
var statearr_63224_63925 = state_63217__$1;
(statearr_63224_63925[(5)] = ex63221);


var statearr_63225_63926 = state_63217__$1;
(statearr_63225_63926[(1)] = (4));

(statearr_63225_63926[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63218 === (6))){
var inst_63210 = (state_63217[(2)]);
var inst_63211 = com.wsscode.async.async_cljs.throw_err(inst_63210);
var inst_63212 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(cache,cljs.core.assoc,key,inst_63211);
var _ = (function (){var statearr_63226 = state_63217;
(statearr_63226[(4)] = cljs.core.rest((state_63217[(4)])));

return statearr_63226;
})();
var state_63217__$1 = (function (){var statearr_63227 = state_63217;
(statearr_63227[(7)] = inst_63212);

return statearr_63227;
})();
var statearr_63228_63927 = state_63217__$1;
(statearr_63228_63927[(2)] = inst_63211);

(statearr_63228_63927[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$cached_STAR__$_state_machine__42121__auto__ = null;
var com$wsscode$pathom$core$cached_STAR__$_state_machine__42121__auto____0 = (function (){
var statearr_63229 = [null,null,null,null,null,null,null,null];
(statearr_63229[(0)] = com$wsscode$pathom$core$cached_STAR__$_state_machine__42121__auto__);

(statearr_63229[(1)] = (1));

return statearr_63229;
});
var com$wsscode$pathom$core$cached_STAR__$_state_machine__42121__auto____1 = (function (state_63217){
while(true){
var ret_value__42122__auto__ = (function (){try{while(true){
var result__42123__auto__ = switch__42120__auto__(state_63217);
if(cljs.core.keyword_identical_QMARK_(result__42123__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__42123__auto__;
}
break;
}
}catch (e63230){var ex__42124__auto__ = e63230;
var statearr_63231_63928 = state_63217;
(statearr_63231_63928[(2)] = ex__42124__auto__);


if(cljs.core.seq((state_63217[(4)]))){
var statearr_63232_63929 = state_63217;
(statearr_63232_63929[(1)] = cljs.core.first((state_63217[(4)])));

} else {
throw ex__42124__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__42122__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__63930 = state_63217;
state_63217 = G__63930;
continue;
} else {
return ret_value__42122__auto__;
}
break;
}
});
com$wsscode$pathom$core$cached_STAR__$_state_machine__42121__auto__ = function(state_63217){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$cached_STAR__$_state_machine__42121__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$cached_STAR__$_state_machine__42121__auto____1.call(this,state_63217);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$cached_STAR__$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$cached_STAR__$_state_machine__42121__auto____0;
com$wsscode$pathom$core$cached_STAR__$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$cached_STAR__$_state_machine__42121__auto____1;
return com$wsscode$pathom$core$cached_STAR__$_state_machine__42121__auto__;
})()
})();
var state__42154__auto__ = (function (){var statearr_63233 = f__42153__auto__();
(statearr_63233[(6)] = c__42152__auto__);

return statearr_63233;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__42154__auto__);
}));

return c__42152__auto__;
} else {
var hit = res__53579__auto__;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(cache,cljs.core.assoc,key,hit);

return hit;
}
}
} else {
return (body_fn.cljs$core$IFn$_invoke$arity$0 ? body_fn.cljs$core$IFn$_invoke$arity$0() : body_fn.call(null));
}
});
com.wsscode.pathom.core.cached_async_STAR_ = (function com$wsscode$pathom$core$cached_async_STAR_(env,key,f){
var temp__5802__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(env,new cljs.core.Keyword("com.wsscode.pathom.core","request-cache","com.wsscode.pathom.core/request-cache",1469884617));
if(cljs.core.truth_(temp__5802__auto__)){
var cache = temp__5802__auto__;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(cache,cljs.core.update,key,(function (x){
if(cljs.core.truth_(x)){
com.wsscode.pathom.trace.trace(env,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword("com.wsscode.pathom.trace","event","com.wsscode.pathom.trace/event",1287398983),new cljs.core.Keyword("com.wsscode.pathom.core","cache-hit","com.wsscode.pathom.core/cache-hit",1851998374),new cljs.core.Keyword("com.wsscode.pathom.core","cache-key","com.wsscode.pathom.core/cache-key",246159991),key], null));

return x;
} else {
com.wsscode.pathom.trace.trace(env,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword("com.wsscode.pathom.trace","event","com.wsscode.pathom.trace/event",1287398983),new cljs.core.Keyword("com.wsscode.pathom.core","cache-miss","com.wsscode.pathom.core/cache-miss",1311426337),new cljs.core.Keyword("com.wsscode.pathom.core","cache-key","com.wsscode.pathom.core/cache-key",246159991),key], null));

var ch__53544__auto__ = cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();
var c__42152__auto___63931 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__42153__auto__ = (function (){var switch__42120__auto__ = (function (state_63259){
var state_val_63260 = (state_63259[(1)]);
if((state_val_63260 === (7))){
var inst_63240 = (state_63259[(7)]);
var state_63259__$1 = state_63259;
var statearr_63261_63932 = state_63259__$1;
(statearr_63261_63932[(2)] = inst_63240);

(statearr_63261_63932[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63260 === (1))){
var state_63259__$1 = state_63259;
var statearr_63262_63933 = state_63259__$1;
(statearr_63262_63933[(2)] = null);

(statearr_63262_63933[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63260 === (4))){
var inst_63234 = (state_63259[(2)]);
var state_63259__$1 = state_63259;
var statearr_63263_63934 = state_63259__$1;
(statearr_63263_63934[(2)] = inst_63234);

(statearr_63263_63934[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63260 === (6))){
var inst_63240 = (state_63259[(7)]);
var state_63259__$1 = state_63259;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_63259__$1,(9),inst_63240);
} else {
if((state_val_63260 === (3))){
var inst_63250 = (state_63259[(8)]);
var inst_63250__$1 = (state_63259[(2)]);
var inst_63251 = (inst_63250__$1 == null);
var state_63259__$1 = (function (){var statearr_63264 = state_63259;
(statearr_63264[(8)] = inst_63250__$1);

return statearr_63264;
})();
if(cljs.core.truth_(inst_63251)){
var statearr_63265_63935 = state_63259__$1;
(statearr_63265_63935[(1)] = (10));

} else {
var statearr_63266_63936 = state_63259__$1;
(statearr_63266_63936[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63260 === (12))){
var inst_63257 = (state_63259[(2)]);
var state_63259__$1 = state_63259;
return cljs.core.async.impl.ioc_helpers.return_chan(state_63259__$1,inst_63257);
} else {
if((state_val_63260 === (2))){
var inst_63240 = (state_63259[(7)]);
var _ = (function (){var statearr_63267 = state_63259;
(statearr_63267[(4)] = cljs.core.cons((5),(state_63259[(4)])));

return statearr_63267;
})();
var inst_63240__$1 = (f.cljs$core$IFn$_invoke$arity$0 ? f.cljs$core$IFn$_invoke$arity$0() : f.call(null));
var inst_63241 = com.wsscode.async.async_cljs.chan_QMARK_(inst_63240__$1);
var state_63259__$1 = (function (){var statearr_63268 = state_63259;
(statearr_63268[(7)] = inst_63240__$1);

return statearr_63268;
})();
if(inst_63241){
var statearr_63269_63937 = state_63259__$1;
(statearr_63269_63937[(1)] = (6));

} else {
var statearr_63270_63938 = state_63259__$1;
(statearr_63270_63938[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63260 === (11))){
var inst_63250 = (state_63259[(8)]);
var inst_63255 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(ch__53544__auto__,inst_63250);
var state_63259__$1 = state_63259;
var statearr_63272_63939 = state_63259__$1;
(statearr_63272_63939[(2)] = inst_63255);

(statearr_63272_63939[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63260 === (9))){
var inst_63244 = (state_63259[(2)]);
var state_63259__$1 = state_63259;
var statearr_63273_63940 = state_63259__$1;
(statearr_63273_63940[(2)] = inst_63244);

(statearr_63273_63940[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63260 === (5))){
var _ = (function (){var statearr_63274 = state_63259;
(statearr_63274[(4)] = cljs.core.rest((state_63259[(4)])));

return statearr_63274;
})();
var state_63259__$1 = state_63259;
var ex63271 = (state_63259__$1[(2)]);
var statearr_63275_63941 = state_63259__$1;
(statearr_63275_63941[(5)] = ex63271);


var statearr_63276_63942 = state_63259__$1;
(statearr_63276_63942[(1)] = (4));

(statearr_63276_63942[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63260 === (10))){
var inst_63253 = cljs.core.async.close_BANG_(ch__53544__auto__);
var state_63259__$1 = state_63259;
var statearr_63277_63944 = state_63259__$1;
(statearr_63277_63944[(2)] = inst_63253);

(statearr_63277_63944[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63260 === (8))){
var inst_63247 = (state_63259[(2)]);
var _ = (function (){var statearr_63278 = state_63259;
(statearr_63278[(4)] = cljs.core.rest((state_63259[(4)])));

return statearr_63278;
})();
var state_63259__$1 = state_63259;
var statearr_63279_63945 = state_63259__$1;
(statearr_63279_63945[(2)] = inst_63247);

(statearr_63279_63945[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$cached_async_STAR__$_state_machine__42121__auto__ = null;
var com$wsscode$pathom$core$cached_async_STAR__$_state_machine__42121__auto____0 = (function (){
var statearr_63280 = [null,null,null,null,null,null,null,null,null];
(statearr_63280[(0)] = com$wsscode$pathom$core$cached_async_STAR__$_state_machine__42121__auto__);

(statearr_63280[(1)] = (1));

return statearr_63280;
});
var com$wsscode$pathom$core$cached_async_STAR__$_state_machine__42121__auto____1 = (function (state_63259){
while(true){
var ret_value__42122__auto__ = (function (){try{while(true){
var result__42123__auto__ = switch__42120__auto__(state_63259);
if(cljs.core.keyword_identical_QMARK_(result__42123__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__42123__auto__;
}
break;
}
}catch (e63281){var ex__42124__auto__ = e63281;
var statearr_63282_63946 = state_63259;
(statearr_63282_63946[(2)] = ex__42124__auto__);


if(cljs.core.seq((state_63259[(4)]))){
var statearr_63283_63947 = state_63259;
(statearr_63283_63947[(1)] = cljs.core.first((state_63259[(4)])));

} else {
throw ex__42124__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__42122__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__63948 = state_63259;
state_63259 = G__63948;
continue;
} else {
return ret_value__42122__auto__;
}
break;
}
});
com$wsscode$pathom$core$cached_async_STAR__$_state_machine__42121__auto__ = function(state_63259){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$cached_async_STAR__$_state_machine__42121__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$cached_async_STAR__$_state_machine__42121__auto____1.call(this,state_63259);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$cached_async_STAR__$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$cached_async_STAR__$_state_machine__42121__auto____0;
com$wsscode$pathom$core$cached_async_STAR__$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$cached_async_STAR__$_state_machine__42121__auto____1;
return com$wsscode$pathom$core$cached_async_STAR__$_state_machine__42121__auto__;
})()
})();
var state__42154__auto__ = (function (){var statearr_63284 = f__42153__auto__();
(statearr_63284[(6)] = c__42152__auto___63931);

return statearr_63284;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__42154__auto__);
}));


return ch__53544__auto__;
}
}));

return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(cache),key);
} else {
var ch__53544__auto__ = cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();
var c__42152__auto___63949 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__42153__auto__ = (function (){var switch__42120__auto__ = (function (state_63310){
var state_val_63311 = (state_63310[(1)]);
if((state_val_63311 === (7))){
var inst_63291 = (state_63310[(7)]);
var state_63310__$1 = state_63310;
var statearr_63312_63950 = state_63310__$1;
(statearr_63312_63950[(2)] = inst_63291);

(statearr_63312_63950[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63311 === (1))){
var state_63310__$1 = state_63310;
var statearr_63313_63951 = state_63310__$1;
(statearr_63313_63951[(2)] = null);

(statearr_63313_63951[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63311 === (4))){
var inst_63285 = (state_63310[(2)]);
var state_63310__$1 = state_63310;
var statearr_63314_63952 = state_63310__$1;
(statearr_63314_63952[(2)] = inst_63285);

(statearr_63314_63952[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63311 === (6))){
var inst_63291 = (state_63310[(7)]);
var state_63310__$1 = state_63310;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_63310__$1,(9),inst_63291);
} else {
if((state_val_63311 === (3))){
var inst_63301 = (state_63310[(8)]);
var inst_63301__$1 = (state_63310[(2)]);
var inst_63302 = (inst_63301__$1 == null);
var state_63310__$1 = (function (){var statearr_63315 = state_63310;
(statearr_63315[(8)] = inst_63301__$1);

return statearr_63315;
})();
if(cljs.core.truth_(inst_63302)){
var statearr_63316_63953 = state_63310__$1;
(statearr_63316_63953[(1)] = (10));

} else {
var statearr_63317_63954 = state_63310__$1;
(statearr_63317_63954[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63311 === (12))){
var inst_63308 = (state_63310[(2)]);
var state_63310__$1 = state_63310;
return cljs.core.async.impl.ioc_helpers.return_chan(state_63310__$1,inst_63308);
} else {
if((state_val_63311 === (2))){
var inst_63291 = (state_63310[(7)]);
var _ = (function (){var statearr_63318 = state_63310;
(statearr_63318[(4)] = cljs.core.cons((5),(state_63310[(4)])));

return statearr_63318;
})();
var inst_63291__$1 = (f.cljs$core$IFn$_invoke$arity$0 ? f.cljs$core$IFn$_invoke$arity$0() : f.call(null));
var inst_63292 = com.wsscode.async.async_cljs.chan_QMARK_(inst_63291__$1);
var state_63310__$1 = (function (){var statearr_63319 = state_63310;
(statearr_63319[(7)] = inst_63291__$1);

return statearr_63319;
})();
if(inst_63292){
var statearr_63320_63955 = state_63310__$1;
(statearr_63320_63955[(1)] = (6));

} else {
var statearr_63321_63956 = state_63310__$1;
(statearr_63321_63956[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63311 === (11))){
var inst_63301 = (state_63310[(8)]);
var inst_63306 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(ch__53544__auto__,inst_63301);
var state_63310__$1 = state_63310;
var statearr_63323_63957 = state_63310__$1;
(statearr_63323_63957[(2)] = inst_63306);

(statearr_63323_63957[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63311 === (9))){
var inst_63295 = (state_63310[(2)]);
var state_63310__$1 = state_63310;
var statearr_63324_63958 = state_63310__$1;
(statearr_63324_63958[(2)] = inst_63295);

(statearr_63324_63958[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63311 === (5))){
var _ = (function (){var statearr_63325 = state_63310;
(statearr_63325[(4)] = cljs.core.rest((state_63310[(4)])));

return statearr_63325;
})();
var state_63310__$1 = state_63310;
var ex63322 = (state_63310__$1[(2)]);
var statearr_63326_63959 = state_63310__$1;
(statearr_63326_63959[(5)] = ex63322);


var statearr_63327_63960 = state_63310__$1;
(statearr_63327_63960[(1)] = (4));

(statearr_63327_63960[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63311 === (10))){
var inst_63304 = cljs.core.async.close_BANG_(ch__53544__auto__);
var state_63310__$1 = state_63310;
var statearr_63328_63961 = state_63310__$1;
(statearr_63328_63961[(2)] = inst_63304);

(statearr_63328_63961[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63311 === (8))){
var inst_63298 = (state_63310[(2)]);
var _ = (function (){var statearr_63329 = state_63310;
(statearr_63329[(4)] = cljs.core.rest((state_63310[(4)])));

return statearr_63329;
})();
var state_63310__$1 = state_63310;
var statearr_63330_63962 = state_63310__$1;
(statearr_63330_63962[(2)] = inst_63298);

(statearr_63330_63962[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$cached_async_STAR__$_state_machine__42121__auto__ = null;
var com$wsscode$pathom$core$cached_async_STAR__$_state_machine__42121__auto____0 = (function (){
var statearr_63331 = [null,null,null,null,null,null,null,null,null];
(statearr_63331[(0)] = com$wsscode$pathom$core$cached_async_STAR__$_state_machine__42121__auto__);

(statearr_63331[(1)] = (1));

return statearr_63331;
});
var com$wsscode$pathom$core$cached_async_STAR__$_state_machine__42121__auto____1 = (function (state_63310){
while(true){
var ret_value__42122__auto__ = (function (){try{while(true){
var result__42123__auto__ = switch__42120__auto__(state_63310);
if(cljs.core.keyword_identical_QMARK_(result__42123__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__42123__auto__;
}
break;
}
}catch (e63332){var ex__42124__auto__ = e63332;
var statearr_63333_63964 = state_63310;
(statearr_63333_63964[(2)] = ex__42124__auto__);


if(cljs.core.seq((state_63310[(4)]))){
var statearr_63334_63965 = state_63310;
(statearr_63334_63965[(1)] = cljs.core.first((state_63310[(4)])));

} else {
throw ex__42124__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__42122__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__63966 = state_63310;
state_63310 = G__63966;
continue;
} else {
return ret_value__42122__auto__;
}
break;
}
});
com$wsscode$pathom$core$cached_async_STAR__$_state_machine__42121__auto__ = function(state_63310){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$cached_async_STAR__$_state_machine__42121__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$cached_async_STAR__$_state_machine__42121__auto____1.call(this,state_63310);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$cached_async_STAR__$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$cached_async_STAR__$_state_machine__42121__auto____0;
com$wsscode$pathom$core$cached_async_STAR__$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$cached_async_STAR__$_state_machine__42121__auto____1;
return com$wsscode$pathom$core$cached_async_STAR__$_state_machine__42121__auto__;
})()
})();
var state__42154__auto__ = (function (){var statearr_63335 = f__42153__auto__();
(statearr_63335[(6)] = c__42152__auto___63949);

return statearr_63335;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__42154__auto__);
}));


return ch__53544__auto__;
}
});
com.wsscode.pathom.core.cached_async = (function com$wsscode$pathom$core$cached_async(p__63336,key,f){
var map__63337 = p__63336;
var map__63337__$1 = cljs.core.__destructure_map(map__63337);
var env = map__63337__$1;
var async_request_cache_ch = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63337__$1,new cljs.core.Keyword("com.wsscode.pathom.core","async-request-cache-ch","com.wsscode.pathom.core/async-request-cache-ch",-1864666369));
var request_cache = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63337__$1,new cljs.core.Keyword("com.wsscode.pathom.core","request-cache","com.wsscode.pathom.core/request-cache",1469884617));
if(cljs.core.truth_(async_request_cache_ch)){
if(cljs.core.contains_QMARK_(cljs.core.deref(request_cache),key)){
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(request_cache),key);
} else {
var out = cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();
cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(async_request_cache_ch,new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [env,key,f,out], null));

var ch__53544__auto__ = cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();
var c__42152__auto___63967 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__42153__auto__ = (function (){var switch__42120__auto__ = (function (state_63359){
var state_val_63360 = (state_63359[(1)]);
if((state_val_63360 === (7))){
var inst_63345 = (state_63359[(2)]);
var state_63359__$1 = state_63359;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_63359__$1,(6),inst_63345);
} else {
if((state_val_63360 === (1))){
var state_63359__$1 = state_63359;
var statearr_63361_63968 = state_63359__$1;
(statearr_63361_63968[(2)] = null);

(statearr_63361_63968[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63360 === (4))){
var inst_63338 = (state_63359[(2)]);
var state_63359__$1 = state_63359;
var statearr_63362_63969 = state_63359__$1;
(statearr_63362_63969[(2)] = inst_63338);

(statearr_63362_63969[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63360 === (6))){
var inst_63347 = (state_63359[(2)]);
var _ = (function (){var statearr_63363 = state_63359;
(statearr_63363[(4)] = cljs.core.rest((state_63359[(4)])));

return statearr_63363;
})();
var state_63359__$1 = state_63359;
var statearr_63364_63970 = state_63359__$1;
(statearr_63364_63970[(2)] = inst_63347);

(statearr_63364_63970[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63360 === (3))){
var inst_63350 = (state_63359[(7)]);
var inst_63350__$1 = (state_63359[(2)]);
var inst_63351 = (inst_63350__$1 == null);
var state_63359__$1 = (function (){var statearr_63365 = state_63359;
(statearr_63365[(7)] = inst_63350__$1);

return statearr_63365;
})();
if(cljs.core.truth_(inst_63351)){
var statearr_63366_63971 = state_63359__$1;
(statearr_63366_63971[(1)] = (8));

} else {
var statearr_63367_63972 = state_63359__$1;
(statearr_63367_63972[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63360 === (2))){
var _ = (function (){var statearr_63369 = state_63359;
(statearr_63369[(4)] = cljs.core.cons((5),(state_63359[(4)])));

return statearr_63369;
})();
var state_63359__$1 = state_63359;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_63359__$1,(7),out);
} else {
if((state_val_63360 === (9))){
var inst_63350 = (state_63359[(7)]);
var inst_63355 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(ch__53544__auto__,inst_63350);
var state_63359__$1 = state_63359;
var statearr_63370_63973 = state_63359__$1;
(statearr_63370_63973[(2)] = inst_63355);

(statearr_63370_63973[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63360 === (5))){
var _ = (function (){var statearr_63371 = state_63359;
(statearr_63371[(4)] = cljs.core.rest((state_63359[(4)])));

return statearr_63371;
})();
var state_63359__$1 = state_63359;
var ex63368 = (state_63359__$1[(2)]);
var statearr_63372_63975 = state_63359__$1;
(statearr_63372_63975[(5)] = ex63368);


var statearr_63373_63976 = state_63359__$1;
(statearr_63373_63976[(1)] = (4));

(statearr_63373_63976[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63360 === (10))){
var inst_63357 = (state_63359[(2)]);
var state_63359__$1 = state_63359;
return cljs.core.async.impl.ioc_helpers.return_chan(state_63359__$1,inst_63357);
} else {
if((state_val_63360 === (8))){
var inst_63353 = cljs.core.async.close_BANG_(ch__53544__auto__);
var state_63359__$1 = state_63359;
var statearr_63374_63977 = state_63359__$1;
(statearr_63374_63977[(2)] = inst_63353);

(statearr_63374_63977[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$cached_async_$_state_machine__42121__auto__ = null;
var com$wsscode$pathom$core$cached_async_$_state_machine__42121__auto____0 = (function (){
var statearr_63375 = [null,null,null,null,null,null,null,null];
(statearr_63375[(0)] = com$wsscode$pathom$core$cached_async_$_state_machine__42121__auto__);

(statearr_63375[(1)] = (1));

return statearr_63375;
});
var com$wsscode$pathom$core$cached_async_$_state_machine__42121__auto____1 = (function (state_63359){
while(true){
var ret_value__42122__auto__ = (function (){try{while(true){
var result__42123__auto__ = switch__42120__auto__(state_63359);
if(cljs.core.keyword_identical_QMARK_(result__42123__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__42123__auto__;
}
break;
}
}catch (e63376){var ex__42124__auto__ = e63376;
var statearr_63377_63978 = state_63359;
(statearr_63377_63978[(2)] = ex__42124__auto__);


if(cljs.core.seq((state_63359[(4)]))){
var statearr_63378_63980 = state_63359;
(statearr_63378_63980[(1)] = cljs.core.first((state_63359[(4)])));

} else {
throw ex__42124__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__42122__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__63981 = state_63359;
state_63359 = G__63981;
continue;
} else {
return ret_value__42122__auto__;
}
break;
}
});
com$wsscode$pathom$core$cached_async_$_state_machine__42121__auto__ = function(state_63359){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$cached_async_$_state_machine__42121__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$cached_async_$_state_machine__42121__auto____1.call(this,state_63359);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$cached_async_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$cached_async_$_state_machine__42121__auto____0;
com$wsscode$pathom$core$cached_async_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$cached_async_$_state_machine__42121__auto____1;
return com$wsscode$pathom$core$cached_async_$_state_machine__42121__auto__;
})()
})();
var state__42154__auto__ = (function (){var statearr_63379 = f__42153__auto__();
(statearr_63379[(6)] = c__42152__auto___63967);

return statearr_63379;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__42154__auto__);
}));


return ch__53544__auto__;
}
} else {
return com.wsscode.pathom.core.cached_async_STAR_(env,key,f);
}
});
com.wsscode.pathom.core.request_cache_async_loop = (function com$wsscode$pathom$core$request_cache_async_loop(ch){
var c__42152__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__42153__auto__ = (function (){var switch__42120__auto__ = (function (state_63401){
var state_val_63402 = (state_63401[(1)]);
if((state_val_63402 === (1))){
var state_63401__$1 = state_63401;
var statearr_63403_63982 = state_63401__$1;
(statearr_63403_63982[(2)] = null);

(statearr_63403_63982[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63402 === (2))){
var state_63401__$1 = state_63401;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_63401__$1,(4),ch);
} else {
if((state_val_63402 === (3))){
var inst_63399 = (state_63401[(2)]);
var state_63401__$1 = state_63401;
return cljs.core.async.impl.ioc_helpers.return_chan(state_63401__$1,inst_63399);
} else {
if((state_val_63402 === (4))){
var inst_63382 = (state_63401[(7)]);
var inst_63382__$1 = (state_63401[(2)]);
var state_63401__$1 = (function (){var statearr_63404 = state_63401;
(statearr_63404[(7)] = inst_63382__$1);

return statearr_63404;
})();
if(cljs.core.truth_(inst_63382__$1)){
var statearr_63405_63983 = state_63401__$1;
(statearr_63405_63983[(1)] = (5));

} else {
var statearr_63406_63984 = state_63401__$1;
(statearr_63406_63984[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63402 === (5))){
var inst_63382 = (state_63401[(7)]);
var inst_63387 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_63382,(0),null);
var inst_63388 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_63382,(1),null);
var inst_63389 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_63382,(2),null);
var inst_63390 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_63382,(3),null);
var inst_63391 = com.wsscode.pathom.core.cached_async_STAR_(inst_63387,inst_63388,inst_63389);
var state_63401__$1 = state_63401;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_63401__$1,(8),inst_63390,inst_63391);
} else {
if((state_val_63402 === (6))){
var state_63401__$1 = state_63401;
var statearr_63407_63985 = state_63401__$1;
(statearr_63407_63985[(2)] = null);

(statearr_63407_63985[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63402 === (7))){
var inst_63397 = (state_63401[(2)]);
var state_63401__$1 = state_63401;
var statearr_63408_63986 = state_63401__$1;
(statearr_63408_63986[(2)] = inst_63397);

(statearr_63408_63986[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63402 === (8))){
var inst_63393 = (state_63401[(2)]);
var state_63401__$1 = (function (){var statearr_63409 = state_63401;
(statearr_63409[(8)] = inst_63393);

return statearr_63409;
})();
var statearr_63410_63987 = state_63401__$1;
(statearr_63410_63987[(2)] = null);

(statearr_63410_63987[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$request_cache_async_loop_$_state_machine__42121__auto__ = null;
var com$wsscode$pathom$core$request_cache_async_loop_$_state_machine__42121__auto____0 = (function (){
var statearr_63411 = [null,null,null,null,null,null,null,null,null];
(statearr_63411[(0)] = com$wsscode$pathom$core$request_cache_async_loop_$_state_machine__42121__auto__);

(statearr_63411[(1)] = (1));

return statearr_63411;
});
var com$wsscode$pathom$core$request_cache_async_loop_$_state_machine__42121__auto____1 = (function (state_63401){
while(true){
var ret_value__42122__auto__ = (function (){try{while(true){
var result__42123__auto__ = switch__42120__auto__(state_63401);
if(cljs.core.keyword_identical_QMARK_(result__42123__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__42123__auto__;
}
break;
}
}catch (e63412){var ex__42124__auto__ = e63412;
var statearr_63413_63988 = state_63401;
(statearr_63413_63988[(2)] = ex__42124__auto__);


if(cljs.core.seq((state_63401[(4)]))){
var statearr_63414_63989 = state_63401;
(statearr_63414_63989[(1)] = cljs.core.first((state_63401[(4)])));

} else {
throw ex__42124__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__42122__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__63990 = state_63401;
state_63401 = G__63990;
continue;
} else {
return ret_value__42122__auto__;
}
break;
}
});
com$wsscode$pathom$core$request_cache_async_loop_$_state_machine__42121__auto__ = function(state_63401){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$request_cache_async_loop_$_state_machine__42121__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$request_cache_async_loop_$_state_machine__42121__auto____1.call(this,state_63401);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$request_cache_async_loop_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$request_cache_async_loop_$_state_machine__42121__auto____0;
com$wsscode$pathom$core$request_cache_async_loop_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$request_cache_async_loop_$_state_machine__42121__auto____1;
return com$wsscode$pathom$core$request_cache_async_loop_$_state_machine__42121__auto__;
})()
})();
var state__42154__auto__ = (function (){var statearr_63415 = f__42153__auto__();
(statearr_63415[(6)] = c__42152__auto__);

return statearr_63415;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__42154__auto__);
}));

return c__42152__auto__;
});
com.wsscode.pathom.core.cache_hit = (function com$wsscode$pathom$core$cache_hit(p__63416,key,value){
var map__63417 = p__63416;
var map__63417__$1 = cljs.core.__destructure_map(map__63417);
var env = map__63417__$1;
var request_cache = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63417__$1,new cljs.core.Keyword("com.wsscode.pathom.core","request-cache","com.wsscode.pathom.core/request-cache",1469884617));
com.wsscode.pathom.trace.trace(env,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword("com.wsscode.pathom.trace","event","com.wsscode.pathom.trace/event",1287398983),new cljs.core.Keyword("com.wsscode.pathom.core","cache-miss","com.wsscode.pathom.core/cache-miss",1311426337),new cljs.core.Keyword("com.wsscode.pathom.core","cache-key","com.wsscode.pathom.core/cache-key",246159991),key], null));

cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(request_cache,cljs.core.assoc,key,value);

return value;
});
com.wsscode.pathom.core.cache_contains_QMARK_ = (function com$wsscode$pathom$core$cache_contains_QMARK_(p__63418,key){
var map__63419 = p__63418;
var map__63419__$1 = cljs.core.__destructure_map(map__63419);
var request_cache = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63419__$1,new cljs.core.Keyword("com.wsscode.pathom.core","request-cache","com.wsscode.pathom.core/request-cache",1469884617));
return cljs.core.contains_QMARK_(cljs.core.deref(request_cache),key);
});
com.wsscode.pathom.core.cache_read = (function com$wsscode$pathom$core$cache_read(p__63420,key){
var map__63421 = p__63420;
var map__63421__$1 = cljs.core.__destructure_map(map__63421);
var request_cache = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63421__$1,new cljs.core.Keyword("com.wsscode.pathom.core","request-cache","com.wsscode.pathom.core/request-cache",1469884617));
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(request_cache),key);
});
com.wsscode.pathom.core.wrap_add_path = (function com$wsscode$pathom$core$wrap_add_path(reader){
return (function (p__63422){
var map__63423 = p__63422;
var map__63423__$1 = cljs.core.__destructure_map(map__63423);
var env = map__63423__$1;
var ast = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63423__$1,new cljs.core.Keyword(null,"ast","ast",-860334068));
var G__63424 = cljs.core.update.cljs$core$IFn$_invoke$arity$4(env,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),cljs.core.fnil.cljs$core$IFn$_invoke$arity$2(cljs.core.conj,cljs.core.PersistentVector.EMPTY),new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(ast));
return (reader.cljs$core$IFn$_invoke$arity$1 ? reader.cljs$core$IFn$_invoke$arity$1(G__63424) : reader.call(null,G__63424));
});
});
com.wsscode.pathom.core.group_plugins_by_action = (function com$wsscode$pathom$core$group_plugins_by_action(plugins){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (g,p){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (g__$1,p__63425){
var vec__63426 = p__63425;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__63426,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__63426,(1),null);
return cljs.core.update.cljs$core$IFn$_invoke$arity$4(g__$1,k,cljs.core.fnil.cljs$core$IFn$_invoke$arity$2(cljs.core.conj,cljs.core.PersistentVector.EMPTY),v);
}),g,p);
}),cljs.core.PersistentArrayMap.EMPTY,plugins);
});
/**
 * Normalize a remote interface input. In case of vector it makes a map. Otherwise
 *   returns as is.
 */
com.wsscode.pathom.core.normalize_request = (function com$wsscode$pathom$core$normalize_request(input){
if(cljs.core.vector_QMARK_(input)){
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword("pathom","eql","pathom/eql",302093908),input,new cljs.core.Keyword("pathom","entity","pathom/entity",541245087),cljs.core.PersistentArrayMap.EMPTY], null);
} else {
return input;
}
});
com.wsscode.pathom.core.wrap_normalize_env = (function com$wsscode$pathom$core$wrap_normalize_env(var_args){
var G__63430 = arguments.length;
switch (G__63430) {
case 1:
return com.wsscode.pathom.core.wrap_normalize_env.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return com.wsscode.pathom.core.wrap_normalize_env.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(com.wsscode.pathom.core.wrap_normalize_env.cljs$core$IFn$_invoke$arity$1 = (function (parser){
return com.wsscode.pathom.core.wrap_normalize_env.cljs$core$IFn$_invoke$arity$2(parser,cljs.core.PersistentVector.EMPTY);
}));

(com.wsscode.pathom.core.wrap_normalize_env.cljs$core$IFn$_invoke$arity$2 = (function (parser,plugins){
return (function() {
var com$wsscode$pathom$core$wrap_normalize_env_internal = null;
var com$wsscode$pathom$core$wrap_normalize_env_internal__2 = (function (env,tx){
return com$wsscode$pathom$core$wrap_normalize_env_internal.cljs$core$IFn$_invoke$arity$3(env,tx,null);
});
var com$wsscode$pathom$core$wrap_normalize_env_internal__3 = (function (env,request,target){
var map__63431 = com.wsscode.pathom.core.normalize_request(request);
var map__63431__$1 = cljs.core.__destructure_map(map__63431);
var eql = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63431__$1,new cljs.core.Keyword("pathom","eql","pathom/eql",302093908));
var entity = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63431__$1,new cljs.core.Keyword("pathom","entity","pathom/entity",541245087));
var ast = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63431__$1,new cljs.core.Keyword("pathom","ast","pathom/ast",-211527857));
var tx = (function (){var or__5045__auto__ = eql;
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
return edn_query_language.core.ast__GT_query(ast);
}
})();
var env_SINGLEQUOTE_ = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 7, [new cljs.core.Keyword("com.wsscode.pathom.core","entity","com.wsscode.pathom.core/entity",884706031),cljs.core.atom.cljs$core$IFn$_invoke$arity$1((function (){var or__5045__auto__ = entity;
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
return cljs.core.PersistentArrayMap.EMPTY;
}
})()),new cljs.core.Keyword("com.wsscode.pathom.core","request-cache","com.wsscode.pathom.core/request-cache",1469884617),cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY),new cljs.core.Keyword("com.wsscode.pathom.core","entity-key","com.wsscode.pathom.core/entity-key",1494103249),new cljs.core.Keyword("com.wsscode.pathom.core","entity","com.wsscode.pathom.core/entity",884706031),new cljs.core.Keyword("com.wsscode.pathom.core","entity-path-cache","com.wsscode.pathom.core/entity-path-cache",-1017384397),cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY),new cljs.core.Keyword("com.wsscode.pathom.core","placeholder-prefixes","com.wsscode.pathom.core/placeholder-prefixes",-1362240644),new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 1, [">",null], null), null),new cljs.core.Keyword("com.wsscode.pathom.core","parent-query","com.wsscode.pathom.core/parent-query",1683392594),tx,new cljs.core.Keyword("com.wsscode.pathom.core","root-query","com.wsscode.pathom.core/root-query",-100266682),tx], null),env,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword("com.wsscode.pathom.core","plugin-actions","com.wsscode.pathom.core/plugin-actions",-876552970),com.wsscode.pathom.core.group_plugins_by_action(plugins),new cljs.core.Keyword("com.wsscode.pathom.core","plugins","com.wsscode.pathom.core/plugins",-2128476796),plugins,new cljs.core.Keyword(null,"target","target",253001721),target], null)], 0));
if(cljs.core.truth_(cljs.core.some(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Symbol(null,"*","*",345799209,null),null], null), null),tx))){
var res__53579__auto__ = (function (){var G__63432 = env_SINGLEQUOTE_;
var G__63433 = com.wsscode.pathom.core.remove_query_wildcard(tx);
return (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(G__63432,G__63433) : parser.call(null,G__63432,G__63433));
})();
if(com.wsscode.async.async_cljs.chan_QMARK_(res__53579__auto__)){
var c__42152__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__42153__auto__ = (function (){var switch__42120__auto__ = (function (state_63449){
var state_val_63450 = (state_63449[(1)]);
if((state_val_63450 === (1))){
var state_63449__$1 = state_63449;
var statearr_63451_63993 = state_63449__$1;
(statearr_63451_63993[(2)] = null);

(statearr_63451_63993[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63450 === (2))){
var _ = (function (){var statearr_63452 = state_63449;
(statearr_63452[(4)] = cljs.core.cons((5),(state_63449[(4)])));

return statearr_63452;
})();
var state_63449__$1 = state_63449;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_63449__$1,(6),res__53579__auto__);
} else {
if((state_val_63450 === (3))){
var inst_63447 = (state_63449[(2)]);
var state_63449__$1 = state_63449;
return cljs.core.async.impl.ioc_helpers.return_chan(state_63449__$1,inst_63447);
} else {
if((state_val_63450 === (4))){
var inst_63434 = (state_63449[(2)]);
var state_63449__$1 = state_63449;
var statearr_63454_63994 = state_63449__$1;
(statearr_63454_63994[(2)] = inst_63434);

(statearr_63454_63994[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63450 === (5))){
var _ = (function (){var statearr_63455 = state_63449;
(statearr_63455[(4)] = cljs.core.rest((state_63449[(4)])));

return statearr_63455;
})();
var state_63449__$1 = state_63449;
var ex63453 = (state_63449__$1[(2)]);
var statearr_63456_63995 = state_63449__$1;
(statearr_63456_63995[(5)] = ex63453);


var statearr_63457_63996 = state_63449__$1;
(statearr_63457_63996[(1)] = (4));

(statearr_63457_63996[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63450 === (6))){
var inst_63441 = (state_63449[(2)]);
var inst_63442 = com.wsscode.async.async_cljs.throw_err(inst_63441);
var inst_63443 = (entity.cljs$core$IFn$_invoke$arity$1 ? entity.cljs$core$IFn$_invoke$arity$1(env_SINGLEQUOTE_) : entity.call(null,env_SINGLEQUOTE_));
var inst_63444 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([inst_63443,inst_63442], 0));
var _ = (function (){var statearr_63458 = state_63449;
(statearr_63458[(4)] = cljs.core.rest((state_63449[(4)])));

return statearr_63458;
})();
var state_63449__$1 = state_63449;
var statearr_63459_63997 = state_63449__$1;
(statearr_63459_63997[(2)] = inst_63444);

(statearr_63459_63997[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$wrap_normalize_env_internal_$_state_machine__42121__auto__ = null;
var com$wsscode$pathom$core$wrap_normalize_env_internal_$_state_machine__42121__auto____0 = (function (){
var statearr_63460 = [null,null,null,null,null,null,null];
(statearr_63460[(0)] = com$wsscode$pathom$core$wrap_normalize_env_internal_$_state_machine__42121__auto__);

(statearr_63460[(1)] = (1));

return statearr_63460;
});
var com$wsscode$pathom$core$wrap_normalize_env_internal_$_state_machine__42121__auto____1 = (function (state_63449){
while(true){
var ret_value__42122__auto__ = (function (){try{while(true){
var result__42123__auto__ = switch__42120__auto__(state_63449);
if(cljs.core.keyword_identical_QMARK_(result__42123__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__42123__auto__;
}
break;
}
}catch (e63461){var ex__42124__auto__ = e63461;
var statearr_63462_63998 = state_63449;
(statearr_63462_63998[(2)] = ex__42124__auto__);


if(cljs.core.seq((state_63449[(4)]))){
var statearr_63463_63999 = state_63449;
(statearr_63463_63999[(1)] = cljs.core.first((state_63449[(4)])));

} else {
throw ex__42124__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__42122__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__64000 = state_63449;
state_63449 = G__64000;
continue;
} else {
return ret_value__42122__auto__;
}
break;
}
});
com$wsscode$pathom$core$wrap_normalize_env_internal_$_state_machine__42121__auto__ = function(state_63449){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$wrap_normalize_env_internal_$_state_machine__42121__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$wrap_normalize_env_internal_$_state_machine__42121__auto____1.call(this,state_63449);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$wrap_normalize_env_internal_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$wrap_normalize_env_internal_$_state_machine__42121__auto____0;
com$wsscode$pathom$core$wrap_normalize_env_internal_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$wrap_normalize_env_internal_$_state_machine__42121__auto____1;
return com$wsscode$pathom$core$wrap_normalize_env_internal_$_state_machine__42121__auto__;
})()
})();
var state__42154__auto__ = (function (){var statearr_63464 = f__42153__auto__();
(statearr_63464[(6)] = c__42152__auto__);

return statearr_63464;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__42154__auto__);
}));

return c__42152__auto__;
} else {
var res = res__53579__auto__;
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(entity.cljs$core$IFn$_invoke$arity$1 ? entity.cljs$core$IFn$_invoke$arity$1(env_SINGLEQUOTE_) : entity.call(null,env_SINGLEQUOTE_)),res], 0));
}
} else {
return (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(env_SINGLEQUOTE_,tx) : parser.call(null,env_SINGLEQUOTE_,tx));
}
});
com$wsscode$pathom$core$wrap_normalize_env_internal = function(env,request,target){
switch(arguments.length){
case 2:
return com$wsscode$pathom$core$wrap_normalize_env_internal__2.call(this,env,request);
case 3:
return com$wsscode$pathom$core$wrap_normalize_env_internal__3.call(this,env,request,target);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$wrap_normalize_env_internal.cljs$core$IFn$_invoke$arity$2 = com$wsscode$pathom$core$wrap_normalize_env_internal__2;
com$wsscode$pathom$core$wrap_normalize_env_internal.cljs$core$IFn$_invoke$arity$3 = com$wsscode$pathom$core$wrap_normalize_env_internal__3;
return com$wsscode$pathom$core$wrap_normalize_env_internal;
})()
}));

(com.wsscode.pathom.core.wrap_normalize_env.cljs$lang$maxFixedArity = 2);

com.wsscode.pathom.core.wrap_parallel_setup = (function com$wsscode$pathom$core$wrap_parallel_setup(parser){
return (function com$wsscode$pathom$core$wrap_parallel_setup_$_wrap_async_done_signal_internal(env,tx){
var signal = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(false);
var res__53579__auto__ = (function (){var G__63465 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(env,new cljs.core.Keyword("com.wsscode.pathom.parser","done-signal*","com.wsscode.pathom.parser/done-signal*",2069309538),signal,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword("com.wsscode.pathom.parser","active-paths","com.wsscode.pathom.parser/active-paths",457466204),cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentHashSet.EMPTY),new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),cljs.core.PersistentVector.EMPTY], 0));
var G__63466 = tx;
return (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(G__63465,G__63466) : parser.call(null,G__63465,G__63466));
})();
if(com.wsscode.async.async_cljs.chan_QMARK_(res__53579__auto__)){
var c__42152__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__42153__auto__ = (function (){var switch__42120__auto__ = (function (state_63481){
var state_val_63482 = (state_63481[(1)]);
if((state_val_63482 === (1))){
var state_63481__$1 = state_63481;
var statearr_63483_64001 = state_63481__$1;
(statearr_63483_64001[(2)] = null);

(statearr_63483_64001[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63482 === (2))){
var _ = (function (){var statearr_63484 = state_63481;
(statearr_63484[(4)] = cljs.core.cons((5),(state_63481[(4)])));

return statearr_63484;
})();
var state_63481__$1 = state_63481;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_63481__$1,(6),res__53579__auto__);
} else {
if((state_val_63482 === (3))){
var inst_63479 = (state_63481[(2)]);
var state_63481__$1 = state_63481;
return cljs.core.async.impl.ioc_helpers.return_chan(state_63481__$1,inst_63479);
} else {
if((state_val_63482 === (4))){
var inst_63467 = (state_63481[(2)]);
var state_63481__$1 = state_63481;
var statearr_63486_64002 = state_63481__$1;
(statearr_63486_64002[(2)] = inst_63467);

(statearr_63486_64002[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63482 === (5))){
var _ = (function (){var statearr_63487 = state_63481;
(statearr_63487[(4)] = cljs.core.rest((state_63481[(4)])));

return statearr_63487;
})();
var state_63481__$1 = state_63481;
var ex63485 = (state_63481__$1[(2)]);
var statearr_63488_64003 = state_63481__$1;
(statearr_63488_64003[(5)] = ex63485);


var statearr_63489_64008 = state_63481__$1;
(statearr_63489_64008[(1)] = (4));

(statearr_63489_64008[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63482 === (6))){
var inst_63474 = (state_63481[(2)]);
var inst_63475 = com.wsscode.async.async_cljs.throw_err(inst_63474);
var inst_63476 = cljs.core.reset_BANG_(signal,true);
var _ = (function (){var statearr_63490 = state_63481;
(statearr_63490[(4)] = cljs.core.rest((state_63481[(4)])));

return statearr_63490;
})();
var state_63481__$1 = (function (){var statearr_63491 = state_63481;
(statearr_63491[(7)] = inst_63476);

return statearr_63491;
})();
var statearr_63492_64009 = state_63481__$1;
(statearr_63492_64009[(2)] = inst_63475);

(statearr_63492_64009[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$wrap_parallel_setup_$_wrap_async_done_signal_internal_$_state_machine__42121__auto__ = null;
var com$wsscode$pathom$core$wrap_parallel_setup_$_wrap_async_done_signal_internal_$_state_machine__42121__auto____0 = (function (){
var statearr_63493 = [null,null,null,null,null,null,null,null];
(statearr_63493[(0)] = com$wsscode$pathom$core$wrap_parallel_setup_$_wrap_async_done_signal_internal_$_state_machine__42121__auto__);

(statearr_63493[(1)] = (1));

return statearr_63493;
});
var com$wsscode$pathom$core$wrap_parallel_setup_$_wrap_async_done_signal_internal_$_state_machine__42121__auto____1 = (function (state_63481){
while(true){
var ret_value__42122__auto__ = (function (){try{while(true){
var result__42123__auto__ = switch__42120__auto__(state_63481);
if(cljs.core.keyword_identical_QMARK_(result__42123__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__42123__auto__;
}
break;
}
}catch (e63494){var ex__42124__auto__ = e63494;
var statearr_63495_64011 = state_63481;
(statearr_63495_64011[(2)] = ex__42124__auto__);


if(cljs.core.seq((state_63481[(4)]))){
var statearr_63496_64012 = state_63481;
(statearr_63496_64012[(1)] = cljs.core.first((state_63481[(4)])));

} else {
throw ex__42124__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__42122__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__64013 = state_63481;
state_63481 = G__64013;
continue;
} else {
return ret_value__42122__auto__;
}
break;
}
});
com$wsscode$pathom$core$wrap_parallel_setup_$_wrap_async_done_signal_internal_$_state_machine__42121__auto__ = function(state_63481){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$wrap_parallel_setup_$_wrap_async_done_signal_internal_$_state_machine__42121__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$wrap_parallel_setup_$_wrap_async_done_signal_internal_$_state_machine__42121__auto____1.call(this,state_63481);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$wrap_parallel_setup_$_wrap_async_done_signal_internal_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$wrap_parallel_setup_$_wrap_async_done_signal_internal_$_state_machine__42121__auto____0;
com$wsscode$pathom$core$wrap_parallel_setup_$_wrap_async_done_signal_internal_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$wrap_parallel_setup_$_wrap_async_done_signal_internal_$_state_machine__42121__auto____1;
return com$wsscode$pathom$core$wrap_parallel_setup_$_wrap_async_done_signal_internal_$_state_machine__42121__auto__;
})()
})();
var state__42154__auto__ = (function (){var statearr_63497 = f__42153__auto__();
(statearr_63497[(6)] = c__42152__auto__);

return statearr_63497;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__42154__auto__);
}));

return c__42152__auto__;
} else {
var res = res__53579__auto__;
cljs.core.reset_BANG_(signal,true);

return res;
}
});
});
com.wsscode.pathom.core.wrap_setup_async_cache = (function com$wsscode$pathom$core$wrap_setup_async_cache(parser){
return (function com$wsscode$pathom$core$wrap_setup_async_cache_$_wrap_setup_async_cache_internal(env,tx){
var async_cache_ch = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(cljs.core.get.cljs$core$IFn$_invoke$arity$3(env,new cljs.core.Keyword("com.wsscode.pathom.core","async-request-cache-ch-size","com.wsscode.pathom.core/async-request-cache-ch-size",-437531159),(1024)));
com.wsscode.pathom.core.request_cache_async_loop(async_cache_ch);

var res__53579__auto__ = (function (){var G__63498 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(env,new cljs.core.Keyword("com.wsscode.pathom.core","async-request-cache-ch","com.wsscode.pathom.core/async-request-cache-ch",-1864666369),async_cache_ch);
var G__63499 = tx;
return (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(G__63498,G__63499) : parser.call(null,G__63498,G__63499));
})();
if(com.wsscode.async.async_cljs.chan_QMARK_(res__53579__auto__)){
var c__42152__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__42153__auto__ = (function (){var switch__42120__auto__ = (function (state_63514){
var state_val_63515 = (state_63514[(1)]);
if((state_val_63515 === (1))){
var state_63514__$1 = state_63514;
var statearr_63516_64014 = state_63514__$1;
(statearr_63516_64014[(2)] = null);

(statearr_63516_64014[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63515 === (2))){
var _ = (function (){var statearr_63517 = state_63514;
(statearr_63517[(4)] = cljs.core.cons((5),(state_63514[(4)])));

return statearr_63517;
})();
var state_63514__$1 = state_63514;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_63514__$1,(6),res__53579__auto__);
} else {
if((state_val_63515 === (3))){
var inst_63512 = (state_63514[(2)]);
var state_63514__$1 = state_63514;
return cljs.core.async.impl.ioc_helpers.return_chan(state_63514__$1,inst_63512);
} else {
if((state_val_63515 === (4))){
var inst_63500 = (state_63514[(2)]);
var state_63514__$1 = state_63514;
var statearr_63519_64015 = state_63514__$1;
(statearr_63519_64015[(2)] = inst_63500);

(statearr_63519_64015[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63515 === (5))){
var _ = (function (){var statearr_63520 = state_63514;
(statearr_63520[(4)] = cljs.core.rest((state_63514[(4)])));

return statearr_63520;
})();
var state_63514__$1 = state_63514;
var ex63518 = (state_63514__$1[(2)]);
var statearr_63521_64016 = state_63514__$1;
(statearr_63521_64016[(5)] = ex63518);


var statearr_63522_64017 = state_63514__$1;
(statearr_63522_64017[(1)] = (4));

(statearr_63522_64017[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63515 === (6))){
var inst_63507 = (state_63514[(2)]);
var inst_63508 = com.wsscode.async.async_cljs.throw_err(inst_63507);
var inst_63509 = cljs.core.async.close_BANG_(async_cache_ch);
var _ = (function (){var statearr_63523 = state_63514;
(statearr_63523[(4)] = cljs.core.rest((state_63514[(4)])));

return statearr_63523;
})();
var state_63514__$1 = (function (){var statearr_63524 = state_63514;
(statearr_63524[(7)] = inst_63509);

return statearr_63524;
})();
var statearr_63525_64018 = state_63514__$1;
(statearr_63525_64018[(2)] = inst_63508);

(statearr_63525_64018[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$wrap_setup_async_cache_$_wrap_setup_async_cache_internal_$_state_machine__42121__auto__ = null;
var com$wsscode$pathom$core$wrap_setup_async_cache_$_wrap_setup_async_cache_internal_$_state_machine__42121__auto____0 = (function (){
var statearr_63526 = [null,null,null,null,null,null,null,null];
(statearr_63526[(0)] = com$wsscode$pathom$core$wrap_setup_async_cache_$_wrap_setup_async_cache_internal_$_state_machine__42121__auto__);

(statearr_63526[(1)] = (1));

return statearr_63526;
});
var com$wsscode$pathom$core$wrap_setup_async_cache_$_wrap_setup_async_cache_internal_$_state_machine__42121__auto____1 = (function (state_63514){
while(true){
var ret_value__42122__auto__ = (function (){try{while(true){
var result__42123__auto__ = switch__42120__auto__(state_63514);
if(cljs.core.keyword_identical_QMARK_(result__42123__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__42123__auto__;
}
break;
}
}catch (e63527){var ex__42124__auto__ = e63527;
var statearr_63528_64019 = state_63514;
(statearr_63528_64019[(2)] = ex__42124__auto__);


if(cljs.core.seq((state_63514[(4)]))){
var statearr_63529_64020 = state_63514;
(statearr_63529_64020[(1)] = cljs.core.first((state_63514[(4)])));

} else {
throw ex__42124__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__42122__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__64021 = state_63514;
state_63514 = G__64021;
continue;
} else {
return ret_value__42122__auto__;
}
break;
}
});
com$wsscode$pathom$core$wrap_setup_async_cache_$_wrap_setup_async_cache_internal_$_state_machine__42121__auto__ = function(state_63514){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$wrap_setup_async_cache_$_wrap_setup_async_cache_internal_$_state_machine__42121__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$wrap_setup_async_cache_$_wrap_setup_async_cache_internal_$_state_machine__42121__auto____1.call(this,state_63514);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$wrap_setup_async_cache_$_wrap_setup_async_cache_internal_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$wrap_setup_async_cache_$_wrap_setup_async_cache_internal_$_state_machine__42121__auto____0;
com$wsscode$pathom$core$wrap_setup_async_cache_$_wrap_setup_async_cache_internal_$_state_machine__42121__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$wrap_setup_async_cache_$_wrap_setup_async_cache_internal_$_state_machine__42121__auto____1;
return com$wsscode$pathom$core$wrap_setup_async_cache_$_wrap_setup_async_cache_internal_$_state_machine__42121__auto__;
})()
})();
var state__42154__auto__ = (function (){var statearr_63530 = f__42153__auto__();
(statearr_63530[(6)] = c__42152__auto__);

return statearr_63530;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__42154__auto__);
}));

return c__42152__auto__;
} else {
var res = res__53579__auto__;
cljs.core.async.close_BANG_(async_cache_ch);

return res;
}
});
});
com.wsscode.pathom.core.wrap_reduce_params = (function com$wsscode$pathom$core$wrap_reduce_params(reader){
return (function() {
var G__64022 = null;
var G__64022__1 = (function (env){
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"value","value",305978217),(reader.cljs$core$IFn$_invoke$arity$1 ? reader.cljs$core$IFn$_invoke$arity$1(env) : reader.call(null,env))], null);
});
var G__64022__3 = (function (env,_,___$1){
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"value","value",305978217),(reader.cljs$core$IFn$_invoke$arity$1 ? reader.cljs$core$IFn$_invoke$arity$1(env) : reader.call(null,env))], null);
});
G__64022 = function(env,_,___$1){
switch(arguments.length){
case 1:
return G__64022__1.call(this,env);
case 3:
return G__64022__3.call(this,env,_,___$1);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
G__64022.cljs$core$IFn$_invoke$arity$1 = G__64022__1;
G__64022.cljs$core$IFn$_invoke$arity$3 = G__64022__3;
return G__64022;
})()
});
com.wsscode.pathom.core.pathom_read_SINGLEQUOTE_ = (function com$wsscode$pathom$core$pathom_read_SINGLEQUOTE_(p__63531){
var map__63532 = p__63531;
var map__63532__$1 = cljs.core.__destructure_map(map__63532);
var env = map__63532__$1;
var reader = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63532__$1,new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410));
return com.wsscode.pathom.core.read_from(env,reader);
});
com.wsscode.pathom.core.apply_plugins = (function com$wsscode$pathom$core$apply_plugins(var_args){
var args__5775__auto__ = [];
var len__5769__auto___64023 = arguments.length;
var i__5770__auto___64024 = (0);
while(true){
if((i__5770__auto___64024 < len__5769__auto___64023)){
args__5775__auto__.push((arguments[i__5770__auto___64024]));

var G__64025 = (i__5770__auto___64024 + (1));
i__5770__auto___64024 = G__64025;
continue;
} else {
}
break;
}

var argseq__5776__auto__ = ((((3) < args__5775__auto__.length))?(new cljs.core.IndexedSeq(args__5775__auto__.slice((3)),(0),null)):null);
return com.wsscode.pathom.core.apply_plugins.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__5776__auto__);
});

(com.wsscode.pathom.core.apply_plugins.cljs$core$IFn$_invoke$arity$variadic = (function (v,plugins,key,params){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (x,plugin){
var f = cljs.core.get.cljs$core$IFn$_invoke$arity$2(plugin,key);
if(cljs.core.truth_(f)){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(f,x,params);
} else {
return x;
}
}),v,plugins);
}));

(com.wsscode.pathom.core.apply_plugins.cljs$lang$maxFixedArity = (3));

/** @this {Function} */
(com.wsscode.pathom.core.apply_plugins.cljs$lang$applyTo = (function (seq63533){
var G__63534 = cljs.core.first(seq63533);
var seq63533__$1 = cljs.core.next(seq63533);
var G__63535 = cljs.core.first(seq63533__$1);
var seq63533__$2 = cljs.core.next(seq63533__$1);
var G__63536 = cljs.core.first(seq63533__$2);
var seq63533__$3 = cljs.core.next(seq63533__$2);
var self__5754__auto__ = this;
return self__5754__auto__.cljs$core$IFn$_invoke$arity$variadic(G__63534,G__63535,G__63536,seq63533__$3);
}));

com.wsscode.pathom.core.exec_plugin_actions = (function com$wsscode$pathom$core$exec_plugin_actions(var_args){
var args__5775__auto__ = [];
var len__5769__auto___64031 = arguments.length;
var i__5770__auto___64032 = (0);
while(true){
if((i__5770__auto___64032 < len__5769__auto___64031)){
args__5775__auto__.push((arguments[i__5770__auto___64032]));

var G__64033 = (i__5770__auto___64032 + (1));
i__5770__auto___64032 = G__64033;
continue;
} else {
}
break;
}

var argseq__5776__auto__ = ((((3) < args__5775__auto__.length))?(new cljs.core.IndexedSeq(args__5775__auto__.slice((3)),(0),null)):null);
return com.wsscode.pathom.core.exec_plugin_actions.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__5776__auto__);
});

(com.wsscode.pathom.core.exec_plugin_actions.cljs$core$IFn$_invoke$arity$variadic = (function (env,key,v,args){
var plugins = cljs.core.get_in.cljs$core$IFn$_invoke$arity$3(env,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","plugin-actions","com.wsscode.pathom.core/plugin-actions",-876552970),key], null),cljs.core.PersistentVector.EMPTY);
var augmented_v = cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (x,f){
return (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(x) : f.call(null,x));
}),v,plugins);
return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(augmented_v,args);
}));

(com.wsscode.pathom.core.exec_plugin_actions.cljs$lang$maxFixedArity = (3));

/** @this {Function} */
(com.wsscode.pathom.core.exec_plugin_actions.cljs$lang$applyTo = (function (seq63537){
var G__63538 = cljs.core.first(seq63537);
var seq63537__$1 = cljs.core.next(seq63537);
var G__63539 = cljs.core.first(seq63537__$1);
var seq63537__$2 = cljs.core.next(seq63537__$1);
var G__63540 = cljs.core.first(seq63537__$2);
var seq63537__$3 = cljs.core.next(seq63537__$2);
var self__5754__auto__ = this;
return self__5754__auto__.cljs$core$IFn$_invoke$arity$variadic(G__63538,G__63539,G__63540,seq63537__$3);
}));

com.wsscode.pathom.core.easy_plugins = (function com$wsscode$pathom$core$easy_plugins(p__63541){
var map__63542 = p__63541;
var map__63542__$1 = cljs.core.__destructure_map(map__63542);
var plugins = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63542__$1,new cljs.core.Keyword("com.wsscode.pathom.core","plugins","com.wsscode.pathom.core/plugins",-2128476796));
var env = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63542__$1,new cljs.core.Keyword("com.wsscode.pathom.core","env","com.wsscode.pathom.core/env",602275378));
var G__63543 = plugins;
var G__63543__$1 = ((cljs.core.fn_QMARK_(env))?cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [com.wsscode.pathom.core.env_wrap_plugin(env)], null),G__63543):G__63543);
if(cljs.core.map_QMARK_(env)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [com.wsscode.pathom.core.env_plugin(env)], null),G__63543__$1);
} else {
return G__63543__$1;
}
});
com.wsscode.pathom.core.settings_mutation = (function com$wsscode$pathom$core$settings_mutation(settings){
var or__5045__auto__ = new cljs.core.Keyword("com.wsscode.pathom.core","mutate","com.wsscode.pathom.core/mutate",-2086097173).cljs$core$IFn$_invoke$arity$1(settings);
if(cljs.core.truth_(or__5045__auto__)){
return or__5045__auto__;
} else {
return new cljs.core.Keyword(null,"mutate","mutate",1422419038).cljs$core$IFn$_invoke$arity$1(settings);
}
});
com.wsscode.pathom.core.wrap_setup_env = (function com$wsscode$pathom$core$wrap_setup_env(parser,env_SINGLEQUOTE_){
return (function com$wsscode$pathom$core$wrap_setup_env_$_wrap_setup_env_internal(env,tx){
var G__63544 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([env,env_SINGLEQUOTE_], 0));
var G__63545 = tx;
return (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(G__63544,G__63545) : parser.call(null,G__63544,G__63545));
});
});
/**
 * Create a new pathom serial parser, this parser is capable of waiting for core.async
 *   to continue processing, allowing async operations to happen during the parsing.
 * 
 *   Options to tune the parser:
 * 
 *   ::p/env - Use this key to provide a default environment for the parser. This is a sugar
 *   to use the p/env-plugin.
 * 
 *   ::p/mutate - A mutate function that will be called to run mutations, this function
 *   must have the signature: (mutate env key params)
 * 
 *   ::p/plugins - A vector with plugins.
 */
com.wsscode.pathom.core.parser = (function com$wsscode$pathom$core$parser(settings){
var plugins = com.wsscode.pathom.core.easy_plugins(settings);
var mutate = com.wsscode.pathom.core.settings_mutation(settings);
return com.wsscode.pathom.core.wrap_normalize_env.cljs$core$IFn$_invoke$arity$2(com.wsscode.pathom.core.wrap_setup_env(com.wsscode.pathom.core.apply_plugins.cljs$core$IFn$_invoke$arity$variadic(com.wsscode.pathom.core.apply_plugins(com.wsscode.pathom.parser.parser(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"read","read",1140058661),com.wsscode.pathom.core.wrap_add_path(com.wsscode.pathom.core.apply_plugins(com.wsscode.pathom.core.pathom_read_SINGLEQUOTE_,plugins,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-read","com.wsscode.pathom.core/wrap-read",-904142261))),new cljs.core.Keyword(null,"mutate","mutate",1422419038),(cljs.core.truth_(mutate)?com.wsscode.pathom.core.apply_plugins(mutate,plugins,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-mutate","com.wsscode.pathom.core/wrap-mutate",989863202)):null)], null)),plugins,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793)),plugins,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser2","com.wsscode.pathom.core/wrap-parser2",776559497),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([settings], 0)),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("com.wsscode.pathom.core","async-parser?","com.wsscode.pathom.core/async-parser?",920199905),false], null)),plugins);
});
/**
 * Create a new pathom async parser, this parser is serial and capable of waiting for core.async
 *   to continue processing, allowing async operations to happen during the parsing.
 * 
 *   Options to tune the parser:
 * 
 *   ::p/env - Use this key to provide a default environment for the parser. This is a sugar
 *   to use the p/env-plugin.
 * 
 *   ::p/mutate - A mutate function that will be called to run mutations, this function
 *   must have the signature: (mutate env key params)
 * 
 *   ::p/plugins - A vector with plugins.
 */
com.wsscode.pathom.core.async_parser = (function com$wsscode$pathom$core$async_parser(settings){
var plugins = com.wsscode.pathom.core.easy_plugins(settings);
var mutate = com.wsscode.pathom.core.settings_mutation(settings);
return com.wsscode.pathom.core.wrap_normalize_env.cljs$core$IFn$_invoke$arity$2(com.wsscode.pathom.core.wrap_setup_async_cache(com.wsscode.pathom.core.wrap_setup_env(com.wsscode.pathom.core.apply_plugins.cljs$core$IFn$_invoke$arity$variadic(com.wsscode.pathom.core.apply_plugins(com.wsscode.pathom.parser.async_parser(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"read","read",1140058661),com.wsscode.pathom.core.wrap_add_path(com.wsscode.pathom.core.apply_plugins(com.wsscode.pathom.core.pathom_read_SINGLEQUOTE_,plugins,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-read","com.wsscode.pathom.core/wrap-read",-904142261))),new cljs.core.Keyword(null,"mutate","mutate",1422419038),(cljs.core.truth_(mutate)?com.wsscode.pathom.core.apply_plugins(mutate,plugins,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-mutate","com.wsscode.pathom.core/wrap-mutate",989863202)):null)], null)),plugins,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793)),plugins,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser2","com.wsscode.pathom.core/wrap-parser2",776559497),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([settings], 0)),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("com.wsscode.pathom.core","async-parser?","com.wsscode.pathom.core/async-parser?",920199905),true], null))),plugins);
});
/**
 * If you are doing a recursive call to the parser inside a resolver and using parallel
 *   parser, you need to pass the env though this function to avoid issues.
 * 
 *   Example:
 * 
 *  (pc/defresolver internal-recursive [{:keys [parser] :as env} input]
 *    {::result (parser (p/reset-parallel-env env) [::sub-query])})
 */
com.wsscode.pathom.core.reset_parallel_env = (function com$wsscode$pathom$core$reset_parallel_env(env){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(env,new cljs.core.Keyword("com.wsscode.pathom.parser","active-paths","com.wsscode.pathom.parser/active-paths",457466204),cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentHashSet.EMPTY),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword("com.wsscode.pathom.parser","waiting","com.wsscode.pathom.parser/waiting",-798662278),cljs.core.PersistentHashSet.EMPTY,new cljs.core.Keyword("com.wsscode.pathom.parser","key-watchers","com.wsscode.pathom.parser/key-watchers",-1670257404),cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY),new cljs.core.Keyword("com.wsscode.pathom.core","entity-path-cache","com.wsscode.pathom.core/entity-path-cache",-1017384397),cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY)], 0));
});
/**
 * Create a new pathom parallel parser, this parser is capable of coordinating parallel
 *   data fetch. This also works as an async parser and will handle core async channels
 *   properly.
 * 
 *   Options to tune the parser:
 * 
 *   ::p/env - Use this key to provide a default environment for the parser. This is a sugar
 *   to use the p/env-plugin.
 * 
 *   ::p/mutate - A mutate function that will be called to run mutations, this function
 *   must have the signature: (mutate env key params)
 * 
 *   ::p/plugins - A vector with plugins.
 * 
 *   ::p/async-request-cache-ch-size - Pathom uses internally a queue to avoid concurrency
 *   issues with concurrency, each request gets its own channel, so you can consider this
 *   size needs to accommodate the max parallelism for a single query. Default: 1024
 * 
 *   ::pp/external-wait-ignore-timeout - Sometimes external waits get stuck because a concurrency
 *   problem, this timeout will ignore external waits after some time so the request can
 *   go on. Default: 3000
 * 
 *   ::pp/max-key-iterations - there is a loop that happens when processing attributes in
 *   parallel, this loop will cause multiple iterations to happen in order for a single
 *   attribute to be processed, but in some conditions this loop can go indefinitely, to
 *   prevent this situation this option allows to control the max number of iterations, after
 *   that it will give up on processing that attribute. Default: 10
 * 
 *   ::pp/key-process-timeout - Max time allowed to run the full query. This is a cascading
 *   timeout, the first level will have the total amount. Default: 60000
 * 
 *   ::pp/processing-recheck-timer - Periodic time to run a checker to verify no parts are
 *   stuck during the processing, when nil the feature is disabled. Default: nil
 */
com.wsscode.pathom.core.parallel_parser = (function com$wsscode$pathom$core$parallel_parser(settings){
var plugins = com.wsscode.pathom.core.easy_plugins(settings);
var mutate = com.wsscode.pathom.core.settings_mutation(settings);
return com.wsscode.pathom.core.wrap_normalize_env.cljs$core$IFn$_invoke$arity$2(com.wsscode.pathom.core.wrap_setup_async_cache(com.wsscode.pathom.core.wrap_parallel_setup(com.wsscode.pathom.core.wrap_setup_env(com.wsscode.pathom.core.apply_plugins.cljs$core$IFn$_invoke$arity$variadic(com.wsscode.pathom.core.apply_plugins(com.wsscode.pathom.parser.parallel_parser(new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"read","read",1140058661),com.wsscode.pathom.core.wrap_add_path(com.wsscode.pathom.core.apply_plugins(com.wsscode.pathom.core.pathom_read_SINGLEQUOTE_,plugins,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-read","com.wsscode.pathom.core/wrap-read",-904142261))),new cljs.core.Keyword(null,"mutate","mutate",1422419038),(cljs.core.truth_(mutate)?com.wsscode.pathom.core.apply_plugins(mutate,plugins,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-mutate","com.wsscode.pathom.core/wrap-mutate",989863202)):null),new cljs.core.Keyword(null,"add-error","add-error",-1195330235),com.wsscode.pathom.core.add_error], null)),plugins,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793)),plugins,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser2","com.wsscode.pathom.core/wrap-parser2",776559497),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([settings], 0)),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("com.wsscode.pathom.core","async-parser?","com.wsscode.pathom.core/async-parser?",920199905),true], null)))),plugins);
});
/**
 * Starting from a map, do a EQL selection on that map. Think of this function as
 *   a power up version of select-keys, but supporting nested selections and placeholders
 *   using the default `>` namespace.
 * 
 *   Example:
 *   (p/map-select {:foo "bar" :deep {:a 1 :b 2}} [{:deep [:a]}])
 *   => {:deep {:a 1}}
 */
com.wsscode.pathom.core.map_select = (function (){var parser = com.wsscode.pathom.core.parser(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword("com.wsscode.pathom.core","env","com.wsscode.pathom.core/env",602275378),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [com.wsscode.pathom.core.map_reader,com.wsscode.pathom.core.env_placeholder_reader], null),new cljs.core.Keyword("com.wsscode.pathom.core","placeholder-prefixes","com.wsscode.pathom.core/placeholder-prefixes",-1362240644),new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 1, [">",null], null), null)], null),new cljs.core.Keyword("com.wsscode.pathom.core","plugins","com.wsscode.pathom.core/plugins",-2128476796),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [com.wsscode.pathom.core.elide_special_outputs_plugin], null)], null));
return (function (map,selection){
return parser(new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("com.wsscode.pathom.core","entity","com.wsscode.pathom.core/entity",884706031),map], null),selection);
});
})();
com.wsscode.pathom.core.continue$ = com.wsscode.pathom.core.join;
com.wsscode.pathom.core.continue_seq = com.wsscode.pathom.core.join_seq;
/**
 * DEPRECATED: use env-placeholder-reader instead.
 * 
 *   Produces a reader that will respond to any keyword with the namespace ns. The join node logical level stays the same
 *   as the parent where the placeholder node is requested.
 */
com.wsscode.pathom.core.placeholder_reader = (function com$wsscode$pathom$core$placeholder_reader(var_args){
var G__63547 = arguments.length;
switch (G__63547) {
case 0:
return com.wsscode.pathom.core.placeholder_reader.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return com.wsscode.pathom.core.placeholder_reader.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(com.wsscode.pathom.core.placeholder_reader.cljs$core$IFn$_invoke$arity$0 = (function (){
return com.wsscode.pathom.core.placeholder_reader.cljs$core$IFn$_invoke$arity$1(">");
}));

(com.wsscode.pathom.core.placeholder_reader.cljs$core$IFn$_invoke$arity$1 = (function (ns){
return (function (p__63548){
var map__63549 = p__63548;
var map__63549__$1 = cljs.core.__destructure_map(map__63549);
var env = map__63549__$1;
var ast = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63549__$1,new cljs.core.Keyword(null,"ast","ast",-860334068));
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(ns,cljs.core.namespace(new cljs.core.Keyword(null,"dispatch-key","dispatch-key",733619510).cljs$core$IFn$_invoke$arity$1(ast)))){
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$1(env);
} else {
return new cljs.core.Keyword("com.wsscode.pathom.core","continue","com.wsscode.pathom.core/continue",591698194);
}
});
}));

(com.wsscode.pathom.core.placeholder_reader.cljs$lang$maxFixedArity = 1);

com.wsscode.pathom.core.placeholder_node = com.wsscode.pathom.core.placeholder_reader;
/**
 * DEPRECATED: use p/parser to create your parser
 */
com.wsscode.pathom.core.pathom_read = (function com$wsscode$pathom$core$pathom_read(p__63550,_,___$1){
var map__63551 = p__63550;
var map__63551__$1 = cljs.core.__destructure_map(map__63551);
var env = map__63551__$1;
var reader = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63551__$1,new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410));
var process_reader = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__63551__$1,new cljs.core.Keyword("com.wsscode.pathom.core","process-reader","com.wsscode.pathom.core/process-reader",348867871));
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"value","value",305978217),(function (){var env__$1 = com.wsscode.pathom.core.normalize_env(env);
return com.wsscode.pathom.core.read_from(env__$1,(cljs.core.truth_(process_reader)?(process_reader.cljs$core$IFn$_invoke$arity$1 ? process_reader.cljs$core$IFn$_invoke$arity$1(reader) : process_reader.call(null,reader)):reader));
})()], null);
});
/**
 * DEPRECATED: use ident-value instead
 */
com.wsscode.pathom.core.ast_key_id = (function com$wsscode$pathom$core$ast_key_id(ast){
var key = (function (){var G__63552 = ast;
if((G__63552 == null)){
return null;
} else {
return new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(G__63552);
}
})();
if(cljs.core.sequential_QMARK_(key)){
return cljs.core.second(key);
} else {
return null;
}
});
/**
 * DEPRECATED: use p/entity
 *   Runs the parser against current element to garantee that some fields are loaded.
 *   This is useful when you need to ensure some values are loaded in order to fetch some
 *   more complex data.
 */
com.wsscode.pathom.core.ensure_attrs = (function com$wsscode$pathom$core$ensure_attrs(env,attributes){
return com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$2(env,attributes);
});

//# sourceMappingURL=com.wsscode.pathom.core.js.map
