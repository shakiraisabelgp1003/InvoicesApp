goog.provide('com.fulcrologic.rad.report_render_options');
com.fulcrologic.rad.report_render_options.style = new cljs.core.Keyword("com.fulcrologic.rad.report-render","style","com.fulcrologic.rad.report-render/style",-785561802);
com.fulcrologic.rad.report_render_options.header_style = new cljs.core.Keyword("com.fulcrologic.rad.report-render","header-style","com.fulcrologic.rad.report-render/header-style",-1505890219);
com.fulcrologic.rad.report_render_options.footer_style = new cljs.core.Keyword("com.fulcrologic.rad.report-render","footer-style","com.fulcrologic.rad.report-render/footer-style",-120832326);
com.fulcrologic.rad.report_render_options.control_style = new cljs.core.Keyword("com.fulcrologic.rad.report-render","control-style","com.fulcrologic.rad.report-render/control-style",-266544951);
com.fulcrologic.rad.report_render_options.body_style = new cljs.core.Keyword("com.fulcrologic.rad.report-render","body-style","com.fulcrologic.rad.report-render/body-style",-547083342);
com.fulcrologic.rad.report_render_options.row_style = new cljs.core.Keyword("com.fulcrologic.rad.report-render","row-style","com.fulcrologic.rad.report-render/row-style",2003404158);

//# sourceMappingURL=com.fulcrologic.rad.report_render_options.js.map
