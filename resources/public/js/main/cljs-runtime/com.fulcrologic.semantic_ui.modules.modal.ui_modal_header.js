goog.provide('com.fulcrologic.semantic_ui.modules.modal.ui_modal_header');
/**
 * A modal can have a header.
 * 
 *   Props:
 *  - as (elementType): An element type to render as (string or function).
 *  - children (node): Primary content.
 *  - className (string): Additional classes.
 *  - content (custom): Shorthand for primary content.
 */
com.fulcrologic.semantic_ui.modules.modal.ui_modal_header.ui_modal_header = com.fulcrologic.semantic_ui.factory_helpers.factory_apply(shadow.js.shim.module$semantic_ui_react$ModalHeader);

//# sourceMappingURL=com.fulcrologic.semantic_ui.modules.modal.ui_modal_header.js.map
