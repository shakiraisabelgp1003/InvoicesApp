goog.provide('com.fulcrologic.rad.rendering.semantic_ui.int_field');
com.fulcrologic.rad.rendering.semantic_ui.int_field.render_field = com.fulcrologic.rad.rendering.semantic_ui.field.render_field_factory.cljs$core$IFn$_invoke$arity$1(com.fulcrologic.fulcro.dom.inputs.ui_int_input);

//# sourceMappingURL=com.fulcrologic.rad.rendering.semantic_ui.int_field.js.map
