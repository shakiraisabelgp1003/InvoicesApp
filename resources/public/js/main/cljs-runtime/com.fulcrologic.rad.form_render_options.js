goog.provide('com.fulcrologic.rad.form_render_options');
com.fulcrologic.rad.form_render_options.style = new cljs.core.Keyword("com.fulcrologic.rad.form-render","style","com.fulcrologic.rad.form-render/style",-76545338);
com.fulcrologic.rad.form_render_options.header_style = new cljs.core.Keyword("com.fulcrologic.rad.form-render","header-style","com.fulcrologic.rad.form-render/header-style",-1735248635);
com.fulcrologic.rad.form_render_options.footer_style = new cljs.core.Keyword("com.fulcrologic.rad.form-render","footer-style","com.fulcrologic.rad.form-render/footer-style",-1559198806);
com.fulcrologic.rad.form_render_options.fields_style = new cljs.core.Keyword("com.fulcrologic.rad.form-render","fields-style","com.fulcrologic.rad.form-render/fields-style",-1333311484);

//# sourceMappingURL=com.fulcrologic.rad.form_render_options.js.map
