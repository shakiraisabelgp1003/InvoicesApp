goog.provide('com.fulcrologic.rad.errors');
if((typeof com !== 'undefined') && (typeof com.fulcrologic !== 'undefined') && (typeof com.fulcrologic.rad !== 'undefined') && (typeof com.fulcrologic.rad.errors !== 'undefined') && (typeof com.fulcrologic.rad.errors.prior_warnings !== 'undefined')){
} else {
com.fulcrologic.rad.errors.prior_warnings = cljs.core.volatile_BANG_(cljs.core.PersistentHashSet.EMPTY);
}

//# sourceMappingURL=com.fulcrologic.rad.errors.js.map
