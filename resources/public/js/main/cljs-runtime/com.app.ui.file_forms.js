goog.provide('com.app.ui.file_forms');

var options__57920__auto___60682 = (function (){var get_class__57913__auto__ = (function (){
return com.app.ui.file_forms.FileForm;
});
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(com.fulcrologic.rad.form.convert_options(get_class__57913__auto__,"com.app.ui.file-forms.FileForm",new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword("com.fulcrologic.rad.form","id","com.fulcrologic.rad.form/id",-668012937),com.app.model.file.id,new cljs.core.Keyword("com.fulcrologic.rad.form","layout-styles","com.fulcrologic.rad.form/layout-styles",958249624),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"form-container","form-container",-1768135707),new cljs.core.Keyword(null,"file-as-icon","file-as-icon",-1441299278)], null),new cljs.core.Keyword("com.fulcrologic.rad.form","attributes","com.fulcrologic.rad.form/attributes",1227286591),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [com.app.model.file.uploaded_on,com.app.model.file.sha,com.app.model.file.filename], null),new cljs.core.Keyword("com.fulcrologic.rad.form","field-labels","com.fulcrologic.rad.form/field-labels",1559200440),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword("file.sha","filename","file.sha/filename",1999511899),"",new cljs.core.Keyword("file","uploaded-on","file/uploaded-on",-1367781993),"",new cljs.core.Keyword("file","sha","file/sha",-945072191),""], null)], null)),new cljs.core.Keyword(null,"render","render",-1408033454),(function com$app$ui$file_forms$render_FileForm(this$){
return com.fulcrologic.fulcro.components.wrapped_render(this$,(function (){
var props = (com.fulcrologic.fulcro.components.props.cljs$core$IFn$_invoke$arity$1 ? com.fulcrologic.fulcro.components.props.cljs$core$IFn$_invoke$arity$1(this$) : com.fulcrologic.fulcro.components.props.call(null,this$));
return com.fulcrologic.rad.form.render_layout(this$,props);
}));
}),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"componentName","componentName",-2103437555),new cljs.core.Keyword("com.app.ui.file-forms","FileForm","com.app.ui.file-forms/FileForm",1614654496)], 0));
})();
if((typeof com !== 'undefined') && (typeof com.app !== 'undefined') && (typeof com.app.ui !== 'undefined') && (typeof com.app.ui.file_forms !== 'undefined') && (typeof com.app.ui.file_forms.FileForm !== 'undefined')){
} else {
/**
 * @constructor
 */
com.app.ui.file_forms.FileForm = com.fulcrologic.fulcro.components.react_constructor(new cljs.core.Keyword(null,"initLocalState","initLocalState",-46503876).cljs$core$IFn$_invoke$arity$1(options__57920__auto___60682));
}

com.fulcrologic.fulcro.components.configure_component_BANG_(com.app.ui.file_forms.FileForm,new cljs.core.Keyword("com.app.ui.file-forms","FileForm","com.app.ui.file-forms/FileForm",1614654496),options__57920__auto___60682);

//# sourceMappingURL=com.app.ui.file_forms.js.map
