goog.provide('com.fulcrologic.semantic_ui.modules.modal.ui_modal_content');
/**
 * A modal can contain content.
 * 
 *   Props:
 *  - as (elementType): An element type to render as (string or function).
 *  - children (node): Primary content.
 *  - className (string): Additional classes.
 *  - content (custom): Shorthand for primary content.
 *  - image (bool): A modal can contain image content.
 *  - scrolling (bool): A modal can use the entire size of the screen.
 */
com.fulcrologic.semantic_ui.modules.modal.ui_modal_content.ui_modal_content = com.fulcrologic.semantic_ui.factory_helpers.factory_apply(shadow.js.shim.module$semantic_ui_react$ModalContent);

//# sourceMappingURL=com.fulcrologic.semantic_ui.modules.modal.ui_modal_content.js.map
