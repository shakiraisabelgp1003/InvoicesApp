goog.provide('com.fulcrologic.semantic_ui.modules.popup.ui_popup_content');
/**
 * A PopupContent displays the content body of a Popover.
 * 
 *   Props:
 *  - as (elementType): An element type to render as (string or function).
 *  - children (node): The content of the Popup
 *  - className (string): Classes to add to the Popup content className.
 *  - content (custom): Shorthand for primary content.
 */
com.fulcrologic.semantic_ui.modules.popup.ui_popup_content.ui_popup_content = com.fulcrologic.semantic_ui.factory_helpers.factory_apply(shadow.js.shim.module$semantic_ui_react$PopupContent);

//# sourceMappingURL=com.fulcrologic.semantic_ui.modules.popup.ui_popup_content.js.map
