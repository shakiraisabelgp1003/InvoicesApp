goog.provide('com.fulcrologic.semantic_ui.modules.modal.ui_modal_actions');
/**
 * A modal can contain a row of actions.
 * 
 *   Props:
 *  - actions (custom): Array of shorthand buttons.
 *  - as (elementType): An element type to render as (string or function).
 *  - children (node): Primary content.
 *  - className (string): Additional classes.
 *  - content (custom): Shorthand for primary content.
 *  - onActionClick (func): Action onClick handler when using shorthand `actions`. ()
 */
com.fulcrologic.semantic_ui.modules.modal.ui_modal_actions.ui_modal_actions = com.fulcrologic.semantic_ui.factory_helpers.factory_apply(shadow.js.shim.module$semantic_ui_react$ModalActions);

//# sourceMappingURL=com.fulcrologic.semantic_ui.modules.modal.ui_modal_actions.js.map
