goog.provide('com.fulcrologic.rad.attributes');
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.fulcrologic.rad.attributes","qualified-key","com.fulcrologic.rad.attributes/qualified-key",-649597949),new cljs.core.Symbol("cljs.core","qualified-keyword?","cljs.core/qualified-keyword?",-308091478,null),cljs.core.qualified_keyword_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.fulcrologic.rad.attributes","type","com.fulcrologic.rad.attributes/type",-1756868804),new cljs.core.Symbol("cljs.core","keyword?","cljs.core/keyword?",713156450,null),cljs.core.keyword_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.fulcrologic.rad.attributes","target","com.fulcrologic.rad.attributes/target",-650174039),new cljs.core.Symbol("cljs.core","qualified-keyword?","cljs.core/qualified-keyword?",-308091478,null),cljs.core.qualified_keyword_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null),new cljs.core.Keyword(null,"req","req",-326448303),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","type","com.fulcrologic.rad.attributes/type",-1756868804),new cljs.core.Keyword("com.fulcrologic.rad.attributes","qualified-key","com.fulcrologic.rad.attributes/qualified-key",-649597949)], null),new cljs.core.Keyword(null,"opt","opt",-794706369),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","target","com.fulcrologic.rad.attributes/target",-650174039)], null)),cljs.spec.alpha.map_spec_impl(cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"req-un","req-un",1074571008),new cljs.core.Keyword(null,"opt-un","opt-un",883442496),new cljs.core.Keyword(null,"gfn","gfn",791517474),new cljs.core.Keyword(null,"pred-exprs","pred-exprs",1792271395),new cljs.core.Keyword(null,"keys-pred","keys-pred",858984739),new cljs.core.Keyword(null,"opt-keys","opt-keys",1262688261),new cljs.core.Keyword(null,"req-specs","req-specs",553962313),new cljs.core.Keyword(null,"req","req",-326448303),new cljs.core.Keyword(null,"req-keys","req-keys",514319221),new cljs.core.Keyword(null,"opt-specs","opt-specs",-384905450),new cljs.core.Keyword(null,"pred-forms","pred-forms",172611832),new cljs.core.Keyword(null,"opt","opt",-794706369)],[null,null,null,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (G__85652){
return cljs.core.map_QMARK_(G__85652);
}),(function (G__85652){
return cljs.core.contains_QMARK_(G__85652,new cljs.core.Keyword("com.fulcrologic.rad.attributes","type","com.fulcrologic.rad.attributes/type",-1756868804));
}),(function (G__85652){
return cljs.core.contains_QMARK_(G__85652,new cljs.core.Keyword("com.fulcrologic.rad.attributes","qualified-key","com.fulcrologic.rad.attributes/qualified-key",-649597949));
})], null),(function (G__85652){
return ((cljs.core.map_QMARK_(G__85652)) && (((cljs.core.contains_QMARK_(G__85652,new cljs.core.Keyword("com.fulcrologic.rad.attributes","type","com.fulcrologic.rad.attributes/type",-1756868804))) && (cljs.core.contains_QMARK_(G__85652,new cljs.core.Keyword("com.fulcrologic.rad.attributes","qualified-key","com.fulcrologic.rad.attributes/qualified-key",-649597949))))));
}),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","target","com.fulcrologic.rad.attributes/target",-650174039)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","type","com.fulcrologic.rad.attributes/type",-1756868804),new cljs.core.Keyword("com.fulcrologic.rad.attributes","qualified-key","com.fulcrologic.rad.attributes/qualified-key",-649597949)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","type","com.fulcrologic.rad.attributes/type",-1756868804),new cljs.core.Keyword("com.fulcrologic.rad.attributes","qualified-key","com.fulcrologic.rad.attributes/qualified-key",-649597949)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","type","com.fulcrologic.rad.attributes/type",-1756868804),new cljs.core.Keyword("com.fulcrologic.rad.attributes","qualified-key","com.fulcrologic.rad.attributes/qualified-key",-649597949)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","target","com.fulcrologic.rad.attributes/target",-650174039)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.list(new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"%","%",-950237169,null)], null),cljs.core.list(new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Symbol(null,"%","%",-950237169,null))),cljs.core.list(new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"%","%",-950237169,null)], null),cljs.core.list(new cljs.core.Symbol("cljs.core","contains?","cljs.core/contains?",-976526835,null),new cljs.core.Symbol(null,"%","%",-950237169,null),new cljs.core.Keyword("com.fulcrologic.rad.attributes","type","com.fulcrologic.rad.attributes/type",-1756868804))),cljs.core.list(new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"%","%",-950237169,null)], null),cljs.core.list(new cljs.core.Symbol("cljs.core","contains?","cljs.core/contains?",-976526835,null),new cljs.core.Symbol(null,"%","%",-950237169,null),new cljs.core.Keyword("com.fulcrologic.rad.attributes","qualified-key","com.fulcrologic.rad.attributes/qualified-key",-649597949)))], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","target","com.fulcrologic.rad.attributes/target",-650174039)], null)])));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","every","cljs.spec.alpha/every",123912744,null),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681)),cljs.spec.alpha.every_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword("cljs.spec.alpha","kind-form","cljs.spec.alpha/kind-form",-1047104697),null,new cljs.core.Keyword("cljs.spec.alpha","describe","cljs.spec.alpha/describe",1883026911),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","every","cljs.spec.alpha/every",123912744,null),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681)),new cljs.core.Keyword("cljs.spec.alpha","cpred","cljs.spec.alpha/cpred",-693471218),(function (G__85653){
return cljs.core.coll_QMARK_(G__85653);
})], null),null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute-map","com.fulcrologic.rad.attributes/attribute-map",313776322),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","map-of","cljs.spec.alpha/map-of",153715093,null),new cljs.core.Symbol("cljs.core","keyword?","cljs.core/keyword?",713156450,null),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681)),cljs.spec.alpha.every_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","tuple","cljs.spec.alpha/tuple",-415901908,null),new cljs.core.Symbol(null,"keyword?","keyword?",1917797069,null),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681)),cljs.spec.alpha.tuple_impl.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","keyword?","cljs.core/keyword?",713156450,null),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.keyword_QMARK_,new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681)], null)),new cljs.core.PersistentArrayMap(null, 7, [new cljs.core.Keyword("cljs.spec.alpha","kfn","cljs.spec.alpha/kfn",672643897),(function (i__20422__auto__,v__20423__auto__){
return cljs.core.nth.cljs$core$IFn$_invoke$arity$2(v__20423__auto__,(0));
}),new cljs.core.Keyword(null,"into","into",-150836029),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword("cljs.spec.alpha","conform-all","cljs.spec.alpha/conform-all",45201917),true,new cljs.core.Keyword(null,"kind","kind",-717265803),cljs.core.map_QMARK_,new cljs.core.Keyword("cljs.spec.alpha","kind-form","cljs.spec.alpha/kind-form",-1047104697),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Keyword("cljs.spec.alpha","describe","cljs.spec.alpha/describe",1883026911),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","map-of","cljs.spec.alpha/map-of",153715093,null),new cljs.core.Symbol("cljs.core","keyword?","cljs.core/keyword?",713156450,null),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681)),new cljs.core.Keyword("cljs.spec.alpha","cpred","cljs.spec.alpha/cpred",-693471218),(function (G__85654){
return cljs.core.map_QMARK_(G__85654);
})], null),null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.fulcrologic.rad.attributes","id-keys","com.fulcrologic.rad.attributes/id-keys",432883187),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","every","cljs.spec.alpha/every",123912744,null),new cljs.core.Symbol("cljs.core","qualified-keyword?","cljs.core/qualified-keyword?",-308091478,null),new cljs.core.Keyword(null,"kind","kind",-717265803),new cljs.core.Symbol("cljs.core","set?","cljs.core/set?",-1176684971,null)),cljs.spec.alpha.every_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Symbol(null,"qualified-keyword?","qualified-keyword?",375456001,null),cljs.core.qualified_keyword_QMARK_,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"kind","kind",-717265803),cljs.core.set_QMARK_,new cljs.core.Keyword("cljs.spec.alpha","kind-form","cljs.spec.alpha/kind-form",-1047104697),new cljs.core.Symbol("cljs.core","set?","cljs.core/set?",-1176684971,null),new cljs.core.Keyword("cljs.spec.alpha","describe","cljs.spec.alpha/describe",1883026911),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","every","cljs.spec.alpha/every",123912744,null),new cljs.core.Symbol("cljs.core","qualified-keyword?","cljs.core/qualified-keyword?",-308091478,null),new cljs.core.Keyword(null,"kind","kind",-717265803),new cljs.core.Symbol("cljs.core","set?","cljs.core/set?",-1176684971,null)),new cljs.core.Keyword("cljs.spec.alpha","cpred","cljs.spec.alpha/cpred",-693471218),(function (G__85655){
return cljs.core.set_QMARK_(G__85655);
})], null),null));
cljs.spec.alpha.def_impl(new cljs.core.Symbol("com.fulcrologic.rad.attributes","new-attribute","com.fulcrologic.rad.attributes/new-attribute",-1570997305,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","fspec","cljs.spec.alpha/fspec",-1289128341,null),new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"kw","kw",1158308175),new cljs.core.Symbol("cljs.core","qualified-keyword?","cljs.core/qualified-keyword?",-308091478,null),new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Symbol("cljs.core","keyword?","cljs.core/keyword?",713156450,null),new cljs.core.Keyword(null,"m","m",1632677161),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681)),cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"kw","kw",1158308175),new cljs.core.Symbol("cljs.core","qualified-keyword?","cljs.core/qualified-keyword?",-308091478,null),new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Symbol("cljs.core","keyword?","cljs.core/keyword?",713156450,null),new cljs.core.Keyword(null,"m","m",1632677161),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"kw","kw",1158308175),new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"m","m",1632677161)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.qualified_keyword_QMARK_,cljs.core.keyword_QMARK_,cljs.core.map_QMARK_], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","qualified-keyword?","cljs.core/qualified-keyword?",-308091478,null),new cljs.core.Symbol("cljs.core","keyword?","cljs.core/keyword?",713156450,null),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"kw","kw",1158308175),new cljs.core.Symbol("cljs.core","qualified-keyword?","cljs.core/qualified-keyword?",-308091478,null),new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Symbol("cljs.core","keyword?","cljs.core/keyword?",713156450,null),new cljs.core.Keyword(null,"m","m",1632677161),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681),null,null),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681),null,null,null));


/**
 * Create a new attribute, which is represented as an Attribute record.
 * 
 *   Type can be one of :string, :int, :uuid, etc. (more types are added over time,
 *   so see main documentation and your database adapter for more information).
 * 
 *   The remaining argument is an open map of additional things that any subsystem can
 *   use to describe facets of this attribute that are important to your system.
 * 
 *   If `:ref` is used as the type then the ultimate ID of the target entity should be listed in `m`
 *   under the ::target key.
 *   
 * @type {function(*, !cljs.core.Keyword, !cljs.core.IMap): *}
 */
com.fulcrologic.rad.attributes.new_attribute = (function com$fulcrologic$rad$attributes$new_attribute(kw,type,m){
var map__85660 = cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"kw","kw",1158308175),new cljs.core.Symbol("cljs.core","qualified-keyword?","cljs.core/qualified-keyword?",-308091478,null),new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Symbol("cljs.core","keyword?","cljs.core/keyword?",713156450,null),new cljs.core.Keyword(null,"m","m",1632677161),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"kw","kw",1158308175),new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"m","m",1632677161)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.qualified_keyword_QMARK_,cljs.core.keyword_QMARK_,cljs.core.map_QMARK_], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","qualified-keyword?","cljs.core/qualified-keyword?",-308091478,null),new cljs.core.Symbol("cljs.core","keyword?","cljs.core/keyword?",713156450,null),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"kw","kw",1158308175),new cljs.core.Symbol("cljs.core","qualified-keyword?","cljs.core/qualified-keyword?",-308091478,null),new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Symbol("cljs.core","keyword?","cljs.core/keyword?",713156450,null),new cljs.core.Keyword(null,"m","m",1632677161),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681),null,null),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681),null,null,null);
var map__85660__$1 = cljs.core.__destructure_map(map__85660);
var argspec85656 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85660__$1,new cljs.core.Keyword(null,"args","args",1315556576));
var retspec85657 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85660__$1,new cljs.core.Keyword(null,"ret","ret",-468222814));
if(cljs.core.truth_(argspec85656)){
com.fulcrologic.guardrails.core.run_check(new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/fulcrologic/rad/attributes.cljc:29 new-attribute's",new cljs.core.Keyword(null,"tap>?","tap>?",212454486),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false,new cljs.core.Keyword(null,"expound-opts","expound-opts",623087481),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"args?","args?",-1963723548),true], null),argspec85656,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [kw,type,m], null));
} else {
}

var f85659 = (function (kw__$1,type__$1,m__$1){
var v = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(m__$1,new cljs.core.Keyword("com.fulcrologic.rad.attributes","type","com.fulcrologic.rad.attributes/type",-1756868804),type__$1),new cljs.core.Keyword("com.fulcrologic.rad.attributes","qualified-key","com.fulcrologic.rad.attributes/qualified-key",-649597949),kw__$1);
if(((cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"ref","ref",1289896967),type__$1)) && (((cljs.core.contains_QMARK_(m__$1,new cljs.core.Keyword("com.fulcrologic.rad.attributes","targets","com.fulcrologic.rad.attributes/targets",626018558))) || (cljs.core.contains_QMARK_(m__$1,new cljs.core.Keyword("com.fulcrologic.rad.attributes","target","com.fulcrologic.rad.attributes/target",-650174039))))))){
taoensso.timbre._log_BANG_.cljs$core$IFn$_invoke$arity$11(taoensso.timbre._STAR_config_STAR_,new cljs.core.Keyword(null,"warn","warn",-436710552),"com.fulcrologic.rad.attributes",null,null,new cljs.core.Keyword(null,"p","p",151049309),new cljs.core.Keyword(null,"auto","auto",-566279492),(new cljs.core.Delay((function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, ["NON-Reference attribute",kw__$1,"was given referential target(s). This could cause errors in code that generates code from the attribute."], null);
}),null)),null,1224291797,null);
} else {
}

if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"ref","ref",1289896967),type__$1)) && ((((!(cljs.core.contains_QMARK_(m__$1,new cljs.core.Keyword("com.fulcrologic.rad.attributes","targets","com.fulcrologic.rad.attributes/targets",626018558))))) && ((!(cljs.core.contains_QMARK_(m__$1,new cljs.core.Keyword("com.fulcrologic.rad.attributes","target","com.fulcrologic.rad.attributes/target",-650174039))))))))){
taoensso.timbre._log_BANG_.cljs$core$IFn$_invoke$arity$11(taoensso.timbre._STAR_config_STAR_,new cljs.core.Keyword(null,"warn","warn",-436710552),"com.fulcrologic.rad.attributes",null,null,new cljs.core.Keyword(null,"p","p",151049309),new cljs.core.Keyword(null,"auto","auto",-566279492),(new cljs.core.Delay((function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, ["Reference attribute",kw__$1,"does not list target(s). Resolver generation will not be accurate."], null);
}),null)),null,-1303883165,null);
} else {
}

return v;
});
var ret85658 = f85659(kw,type,m);
if(cljs.core.truth_(retspec85657)){
com.fulcrologic.guardrails.core.run_check(new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/fulcrologic/rad/attributes.cljc:29 new-attribute's",new cljs.core.Keyword(null,"tap>?","tap>?",212454486),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false,new cljs.core.Keyword(null,"expound-opts","expound-opts",623087481),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"args?","args?",-1963723548),false], null),retspec85657,ret85658);
} else {
}

return ret85658;
});
cljs.spec.alpha.def_impl(new cljs.core.Symbol("com.fulcrologic.rad.attributes","to-many?","com.fulcrologic.rad.attributes/to-many?",-245615363,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","fspec","cljs.spec.alpha/fspec",-1289128341,null),new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attr","attr",-604132353),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681)),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Symbol("cljs.core","boolean?","cljs.core/boolean?",1400713761,null)),cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attr","attr",-604132353),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"attr","attr",-604132353)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attr","attr",-604132353),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Symbol("cljs.core","boolean?","cljs.core/boolean?",1400713761,null),cljs.core.boolean_QMARK_,null,null),new cljs.core.Symbol("cljs.core","boolean?","cljs.core/boolean?",1400713761,null),null,null,null));


/**
 * Returns true if the attribute with the given key is a to-many.
 * @type {function(*): !boolean}
 */
com.fulcrologic.rad.attributes.to_many_QMARK_ = (function com$fulcrologic$rad$attributes$to_many_QMARK_(attr){
var map__85665 = cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attr","attr",-604132353),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"attr","attr",-604132353)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attr","attr",-604132353),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Symbol("cljs.core","boolean?","cljs.core/boolean?",1400713761,null),cljs.core.boolean_QMARK_,null,null),new cljs.core.Symbol("cljs.core","boolean?","cljs.core/boolean?",1400713761,null),null,null,null);
var map__85665__$1 = cljs.core.__destructure_map(map__85665);
var argspec85661 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85665__$1,new cljs.core.Keyword(null,"args","args",1315556576));
var retspec85662 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85665__$1,new cljs.core.Keyword(null,"ret","ret",-468222814));
if(cljs.core.truth_(argspec85661)){
com.fulcrologic.guardrails.core.run_check(new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/fulcrologic/rad/attributes.cljc:94 to-many?'s",new cljs.core.Keyword(null,"tap>?","tap>?",212454486),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false,new cljs.core.Keyword(null,"expound-opts","expound-opts",623087481),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"args?","args?",-1963723548),true], null),argspec85661,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [attr], null));
} else {
}

var f85664 = (function (attr__$1){
return cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"many","many",1092119164),new cljs.core.Keyword("com.fulcrologic.rad.attributes","cardinality","com.fulcrologic.rad.attributes/cardinality",-1073230325).cljs$core$IFn$_invoke$arity$1(attr__$1));
});
var ret85663 = f85664(attr);
if(cljs.core.truth_(retspec85662)){
com.fulcrologic.guardrails.core.run_check(new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/fulcrologic/rad/attributes.cljc:94 to-many?'s",new cljs.core.Keyword(null,"tap>?","tap>?",212454486),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false,new cljs.core.Keyword(null,"expound-opts","expound-opts",623087481),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"args?","args?",-1963723548),false], null),retspec85662,ret85663);
} else {
}

return ret85663;
});
cljs.spec.alpha.def_impl(new cljs.core.Symbol("com.fulcrologic.rad.attributes","to-one?","com.fulcrologic.rad.attributes/to-one?",-999397141,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","fspec","cljs.spec.alpha/fspec",-1289128341,null),new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attr","attr",-604132353),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681)),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Symbol("cljs.core","boolean?","cljs.core/boolean?",1400713761,null)),cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attr","attr",-604132353),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"attr","attr",-604132353)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attr","attr",-604132353),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Symbol("cljs.core","boolean?","cljs.core/boolean?",1400713761,null),cljs.core.boolean_QMARK_,null,null),new cljs.core.Symbol("cljs.core","boolean?","cljs.core/boolean?",1400713761,null),null,null,null));


/**
 * Returns true if the attribute with the given key is a to-one.
 * @type {function(*): !boolean}
 */
com.fulcrologic.rad.attributes.to_one_QMARK_ = (function com$fulcrologic$rad$attributes$to_one_QMARK_(attr){
var map__85670 = cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attr","attr",-604132353),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"attr","attr",-604132353)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attr","attr",-604132353),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute","com.fulcrologic.rad.attributes/attribute",1984853681)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Symbol("cljs.core","boolean?","cljs.core/boolean?",1400713761,null),cljs.core.boolean_QMARK_,null,null),new cljs.core.Symbol("cljs.core","boolean?","cljs.core/boolean?",1400713761,null),null,null,null);
var map__85670__$1 = cljs.core.__destructure_map(map__85670);
var retspec85667 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85670__$1,new cljs.core.Keyword(null,"ret","ret",-468222814));
var argspec85666 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85670__$1,new cljs.core.Keyword(null,"args","args",1315556576));
if(cljs.core.truth_(argspec85666)){
com.fulcrologic.guardrails.core.run_check(new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/fulcrologic/rad/attributes.cljc:100 to-one?'s",new cljs.core.Keyword(null,"tap>?","tap>?",212454486),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false,new cljs.core.Keyword(null,"expound-opts","expound-opts",623087481),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"args?","args?",-1963723548),true], null),argspec85666,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [attr], null));
} else {
}

var f85669 = (function (attr__$1){
return cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"many","many",1092119164),new cljs.core.Keyword("com.fulcrologic.rad.attributes","cardinality","com.fulcrologic.rad.attributes/cardinality",-1073230325).cljs$core$IFn$_invoke$arity$1(attr__$1));
});
var ret85668 = f85669(attr);
if(cljs.core.truth_(retspec85667)){
com.fulcrologic.guardrails.core.run_check(new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/fulcrologic/rad/attributes.cljc:100 to-one?'s",new cljs.core.Keyword(null,"tap>?","tap>?",212454486),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false,new cljs.core.Keyword(null,"expound-opts","expound-opts",623087481),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"args?","args?",-1963723548),false], null),retspec85667,ret85668);
} else {
}

return ret85668;
});
cljs.spec.alpha.def_impl(new cljs.core.Symbol("com.fulcrologic.rad.attributes","to-int","com.fulcrologic.rad.attributes/to-int",-1059443256,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","fspec","cljs.spec.alpha/fspec",-1289128341,null),new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"str","str",1089608819),new cljs.core.Symbol("cljs.core","string?","cljs.core/string?",-2072921719,null)),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Symbol("cljs.core","int?","cljs.core/int?",50730120,null)),cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"str","str",1089608819),new cljs.core.Symbol("cljs.core","string?","cljs.core/string?",-2072921719,null)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"str","str",1089608819)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.string_QMARK_], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","string?","cljs.core/string?",-2072921719,null)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"str","str",1089608819),new cljs.core.Symbol("cljs.core","string?","cljs.core/string?",-2072921719,null)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Symbol("cljs.core","int?","cljs.core/int?",50730120,null),cljs.core.int_QMARK_,null,null),new cljs.core.Symbol("cljs.core","int?","cljs.core/int?",50730120,null),null,null,null));


/**
 * @type {function(!string): !number}
 */
com.fulcrologic.rad.attributes.to_int = (function com$fulcrologic$rad$attributes$to_int(str){
var map__85675 = cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"str","str",1089608819),new cljs.core.Symbol("cljs.core","string?","cljs.core/string?",-2072921719,null)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"str","str",1089608819)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.string_QMARK_], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","string?","cljs.core/string?",-2072921719,null)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"str","str",1089608819),new cljs.core.Symbol("cljs.core","string?","cljs.core/string?",-2072921719,null)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Symbol("cljs.core","int?","cljs.core/int?",50730120,null),cljs.core.int_QMARK_,null,null),new cljs.core.Symbol("cljs.core","int?","cljs.core/int?",50730120,null),null,null,null);
var map__85675__$1 = cljs.core.__destructure_map(map__85675);
var argspec85671 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85675__$1,new cljs.core.Keyword(null,"args","args",1315556576));
var retspec85672 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85675__$1,new cljs.core.Keyword(null,"ret","ret",-468222814));
if(cljs.core.truth_(argspec85671)){
com.fulcrologic.guardrails.core.run_check(new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/fulcrologic/rad/attributes.cljc:106 to-int's",new cljs.core.Keyword(null,"tap>?","tap>?",212454486),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false,new cljs.core.Keyword(null,"expound-opts","expound-opts",623087481),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"args?","args?",-1963723548),true], null),argspec85671,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [str], null));
} else {
}

var f85674 = (function (str__$1){
if((str__$1 == null)){
return (0);
} else {
try{return parseInt(str__$1);
}catch (e85676){var e = e85676;
return (0);
}}
});
var ret85673 = f85674(str);
if(cljs.core.truth_(retspec85672)){
com.fulcrologic.guardrails.core.run_check(new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/fulcrologic/rad/attributes.cljc:106 to-int's",new cljs.core.Keyword(null,"tap>?","tap>?",212454486),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false,new cljs.core.Keyword(null,"expound-opts","expound-opts",623087481),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"args?","args?",-1963723548),false], null),retspec85672,ret85673);
} else {
}

return ret85673;
});
cljs.spec.alpha.def_impl(new cljs.core.Symbol("com.fulcrologic.rad.attributes","attributes->eql","com.fulcrologic.rad.attributes/attributes->eql",-98405318,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","fspec","cljs.spec.alpha/fspec",-1289128341,null),new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attrs","attrs",-2090668713),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null)),cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attrs","attrs",-2090668713),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"attrs","attrs",-2090668713)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attrs","attrs",-2090668713),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null),cljs.core.vector_QMARK_,null,null),new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null),null,null,null));


/**
 * Returns an EQL query for all of the attributes that are available for the given database-id
 * @type {function(*): !cljs.core.IVector}
 */
com.fulcrologic.rad.attributes.attributes__GT_eql = (function com$fulcrologic$rad$attributes$attributes__GT_eql(attrs){
var map__85681 = cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attrs","attrs",-2090668713),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"attrs","attrs",-2090668713)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attrs","attrs",-2090668713),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null),cljs.core.vector_QMARK_,null,null),new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null),null,null,null);
var map__85681__$1 = cljs.core.__destructure_map(map__85681);
var retspec85678 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85681__$1,new cljs.core.Keyword(null,"ret","ret",-468222814));
var argspec85677 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85681__$1,new cljs.core.Keyword(null,"args","args",1315556576));
if(cljs.core.truth_(argspec85677)){
com.fulcrologic.guardrails.core.run_check(new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/fulcrologic/rad/attributes.cljc:116 attributes->eql's",new cljs.core.Keyword(null,"tap>?","tap>?",212454486),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false,new cljs.core.Keyword(null,"expound-opts","expound-opts",623087481),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"args?","args?",-1963723548),true], null),argspec85677,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [attrs], null));
} else {
}

var f85680 = (function (attrs__$1){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (outs,p__85682){
var map__85683 = p__85682;
var map__85683__$1 = cljs.core.__destructure_map(map__85683);
var qualified_key = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85683__$1,new cljs.core.Keyword("com.fulcrologic.rad.attributes","qualified-key","com.fulcrologic.rad.attributes/qualified-key",-649597949));
var type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85683__$1,new cljs.core.Keyword("com.fulcrologic.rad.attributes","type","com.fulcrologic.rad.attributes/type",-1756868804));
var target = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85683__$1,new cljs.core.Keyword("com.fulcrologic.rad.attributes","target","com.fulcrologic.rad.attributes/target",-650174039));
var targets = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85683__$1,new cljs.core.Keyword("com.fulcrologic.rad.attributes","targets","com.fulcrologic.rad.attributes/targets",626018558));
if(((cljs.core.seq(targets)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"ref","ref",1289896967),type)))){
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(outs,cljs.core.PersistentArrayMap.createAsIfByAssoc([qualified_key,cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentArrayMap.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$1((function (t){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [t,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [t], null)], null);
})),targets)]));
} else {
if(cljs.core.truth_((function (){var and__5043__auto__ = target;
if(cljs.core.truth_(and__5043__auto__)){
return cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"ref","ref",1289896967),type);
} else {
return and__5043__auto__;
}
})())){
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(outs,cljs.core.PersistentArrayMap.createAsIfByAssoc([qualified_key,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [target], null)]));
} else {
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(outs,qualified_key);

}
}
}),cljs.core.PersistentVector.EMPTY,attrs__$1);
});
var ret85679 = f85680(attrs);
if(cljs.core.truth_(retspec85678)){
com.fulcrologic.guardrails.core.run_check(new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/fulcrologic/rad/attributes.cljc:116 attributes->eql's",new cljs.core.Keyword(null,"tap>?","tap>?",212454486),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false,new cljs.core.Keyword(null,"expound-opts","expound-opts",623087481),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"args?","args?",-1963723548),false], null),retspec85678,ret85679);
} else {
}

return ret85679;
});
cljs.spec.alpha.def_impl(new cljs.core.Symbol("com.fulcrologic.rad.attributes","attribute?","com.fulcrologic.rad.attributes/attribute?",-1663834643,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","fspec","cljs.spec.alpha/fspec",-1289128341,null),new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"v","v",21465059),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Symbol("cljs.core","boolean?","cljs.core/boolean?",1400713761,null)),cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"v","v",21465059),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"v","v",21465059)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.any_QMARK_], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"v","v",21465059),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Symbol("cljs.core","boolean?","cljs.core/boolean?",1400713761,null),cljs.core.boolean_QMARK_,null,null),new cljs.core.Symbol("cljs.core","boolean?","cljs.core/boolean?",1400713761,null),null,null,null));


/**
 * @type {function(*): !boolean}
 */
com.fulcrologic.rad.attributes.attribute_QMARK_ = (function com$fulcrologic$rad$attributes$attribute_QMARK_(v){
var map__85688 = cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"v","v",21465059),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"v","v",21465059)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.any_QMARK_], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"v","v",21465059),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Symbol("cljs.core","boolean?","cljs.core/boolean?",1400713761,null),cljs.core.boolean_QMARK_,null,null),new cljs.core.Symbol("cljs.core","boolean?","cljs.core/boolean?",1400713761,null),null,null,null);
var map__85688__$1 = cljs.core.__destructure_map(map__85688);
var retspec85685 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85688__$1,new cljs.core.Keyword(null,"ret","ret",-468222814));
var argspec85684 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85688__$1,new cljs.core.Keyword(null,"args","args",1315556576));
if(cljs.core.truth_(argspec85684)){
com.fulcrologic.guardrails.core.run_check(new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/fulcrologic/rad/attributes.cljc:156 attribute?'s",new cljs.core.Keyword(null,"tap>?","tap>?",212454486),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false,new cljs.core.Keyword(null,"expound-opts","expound-opts",623087481),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"args?","args?",-1963723548),true], null),argspec85684,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [v], null));
} else {
}

var f85687 = (function (v__$1){
return cljs.core.contains_QMARK_(v__$1,new cljs.core.Keyword("com.fulcrologic.rad.attributes","qualified-key","com.fulcrologic.rad.attributes/qualified-key",-649597949));
});
var ret85686 = f85687(v);
if(cljs.core.truth_(retspec85685)){
com.fulcrologic.guardrails.core.run_check(new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/fulcrologic/rad/attributes.cljc:156 attribute?'s",new cljs.core.Keyword(null,"tap>?","tap>?",212454486),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false,new cljs.core.Keyword(null,"expound-opts","expound-opts",623087481),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"args?","args?",-1963723548),false], null),retspec85685,ret85686);
} else {
}

return ret85686;
});
cljs.spec.alpha.def_impl(new cljs.core.Symbol("com.fulcrologic.rad.attributes","eql-query","com.fulcrologic.rad.attributes/eql-query",1972629947,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","fspec","cljs.spec.alpha/fspec",-1289128341,null),new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attr-query","attr-query",-865662860),new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null)),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null)),cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attr-query","attr-query",-865662860),new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"attr-query","attr-query",-865662860)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.vector_QMARK_], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attr-query","attr-query",-865662860),new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null),cljs.core.vector_QMARK_,null,null),new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null),null,null,null));


/**
 * Convert a query that uses attributes (records) as keys into the proper EQL query. I.e. (eql-query [account/id]) => [::account/id]
 * Honors metadata and join nesting.
 * @type {function(!cljs.core.IVector): !cljs.core.IVector}
 */
com.fulcrologic.rad.attributes.eql_query = (function com$fulcrologic$rad$attributes$eql_query(attr_query){
var map__85693 = cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attr-query","attr-query",-865662860),new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"attr-query","attr-query",-865662860)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.vector_QMARK_], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attr-query","attr-query",-865662860),new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null),cljs.core.vector_QMARK_,null,null),new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null),null,null,null);
var map__85693__$1 = cljs.core.__destructure_map(map__85693);
var retspec85690 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85693__$1,new cljs.core.Keyword(null,"ret","ret",-468222814));
var argspec85689 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85693__$1,new cljs.core.Keyword(null,"args","args",1315556576));
if(cljs.core.truth_(argspec85689)){
com.fulcrologic.guardrails.core.run_check(new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/fulcrologic/rad/attributes.cljc:161 eql-query's",new cljs.core.Keyword(null,"tap>?","tap>?",212454486),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false,new cljs.core.Keyword(null,"expound-opts","expound-opts",623087481),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"args?","args?",-1963723548),true], null),argspec85689,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [attr_query], null));
} else {
}

var f85692 = (function (attr_query__$1){
return clojure.walk.prewalk((function (ele){
if(com.fulcrologic.rad.attributes.attribute_QMARK_(ele)){
return new cljs.core.Keyword("com.fulcrologic.rad.attributes","qualified-key","com.fulcrologic.rad.attributes/qualified-key",-649597949).cljs$core$IFn$_invoke$arity$1(ele);
} else {
return ele;
}
}),attr_query__$1);
});
var ret85691 = f85692(attr_query);
if(cljs.core.truth_(retspec85690)){
com.fulcrologic.guardrails.core.run_check(new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/fulcrologic/rad/attributes.cljc:161 eql-query's",new cljs.core.Keyword(null,"tap>?","tap>?",212454486),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false,new cljs.core.Keyword(null,"expound-opts","expound-opts",623087481),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"args?","args?",-1963723548),false], null),retspec85690,ret85691);
} else {
}

return ret85691;
});
/**
 * Checks if the value looks to be a valid value based on the ::attr/required? and ::attr/valid? options of the
 * given attribute.
 * 
 * Returns true if:
 * 
 * * The value is completely missing (nil), and not marked `ao/required?`
 * * The attribute defines a `ao/valid?` predicate that returns true.
 * * The attribute has NO `ao/valid?` option but is marked `ao/required?`
 *   but the value is non-nil (and if a string, non-blank).
 * 
 * Otherwise returns false.
 * 
 */
com.fulcrologic.rad.attributes.valid_value_QMARK_ = (function com$fulcrologic$rad$attributes$valid_value_QMARK_(p__85694,value,props,k){
var map__85695 = p__85694;
var map__85695__$1 = cljs.core.__destructure_map(map__85695);
var attribute = map__85695__$1;
var required_QMARK_ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85695__$1,new cljs.core.Keyword("com.fulcrologic.rad.attributes","required?","com.fulcrologic.rad.attributes/required?",96541330));
var type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85695__$1,new cljs.core.Keyword("com.fulcrologic.rad.attributes","type","com.fulcrologic.rad.attributes/type",-1756868804));
var valid_QMARK_ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85695__$1,new cljs.core.Keyword("com.fulcrologic.rad.attributes","valid?","com.fulcrologic.rad.attributes/valid?",237057525));
var ref_QMARK_ = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"ref","ref",1289896967),type);
var non_empty_value_QMARK_ = (((!((value == null)))) && ((((((!(ref_QMARK_))) || ((!(cljs.core.empty_QMARK_(value)))))) && ((((!(typeof value === 'string'))) || ((((clojure.string.trim(value)).length) > (0))))))));
var or__5045__auto__ = (((value == null)) && (cljs.core.not(required_QMARK_)));
if(or__5045__auto__){
return or__5045__auto__;
} else {
if(cljs.core.truth_(valid_QMARK_)){
return (valid_QMARK_.cljs$core$IFn$_invoke$arity$3 ? valid_QMARK_.cljs$core$IFn$_invoke$arity$3(value,props,k) : valid_QMARK_.call(null,value,props,k));
} else {
return ((cljs.core.not(required_QMARK_)) || (non_empty_value_QMARK_));
}
}
});
cljs.spec.alpha.def_impl(new cljs.core.Symbol("com.fulcrologic.rad.attributes","attribute-map","com.fulcrologic.rad.attributes/attribute-map",1954307849,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","fspec","cljs.spec.alpha/fspec",-1289128341,null),new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attributes","attributes",-74013604),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute-map","com.fulcrologic.rad.attributes/attribute-map",313776322)),cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attributes","attributes",-74013604),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"attributes","attributes",-74013604)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attributes","attributes",-74013604),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute-map","com.fulcrologic.rad.attributes/attribute-map",313776322),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute-map","com.fulcrologic.rad.attributes/attribute-map",313776322),null,null),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute-map","com.fulcrologic.rad.attributes/attribute-map",313776322),null,null,null));


/**
 * Returns a map of qualified key -> attribute for the given attributes
 * @type {function(*): *}
 */
com.fulcrologic.rad.attributes.attribute_map = (function com$fulcrologic$rad$attributes$attribute_map(attributes){
var map__85700 = cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attributes","attributes",-74013604),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"attributes","attributes",-74013604)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attributes","attributes",-74013604),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute-map","com.fulcrologic.rad.attributes/attribute-map",313776322),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute-map","com.fulcrologic.rad.attributes/attribute-map",313776322),null,null),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attribute-map","com.fulcrologic.rad.attributes/attribute-map",313776322),null,null,null);
var map__85700__$1 = cljs.core.__destructure_map(map__85700);
var retspec85697 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85700__$1,new cljs.core.Keyword(null,"ret","ret",-468222814));
var argspec85696 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85700__$1,new cljs.core.Keyword(null,"args","args",1315556576));
if(cljs.core.truth_(argspec85696)){
com.fulcrologic.guardrails.core.run_check(new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/fulcrologic/rad/attributes.cljc:199 attribute-map's",new cljs.core.Keyword(null,"tap>?","tap>?",212454486),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false,new cljs.core.Keyword(null,"expound-opts","expound-opts",623087481),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"args?","args?",-1963723548),true], null),argspec85696,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [attributes], null));
} else {
}

var f85699 = (function (attributes__$1){
return cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentArrayMap.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$1((function (p__85701){
var map__85702 = p__85701;
var map__85702__$1 = cljs.core.__destructure_map(map__85702);
var a = map__85702__$1;
var qualified_key = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85702__$1,new cljs.core.Keyword("com.fulcrologic.rad.attributes","qualified-key","com.fulcrologic.rad.attributes/qualified-key",-649597949));
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [qualified_key,a], null);
})),attributes__$1);
});
var ret85698 = f85699(attributes);
if(cljs.core.truth_(retspec85697)){
com.fulcrologic.guardrails.core.run_check(new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/fulcrologic/rad/attributes.cljc:199 attribute-map's",new cljs.core.Keyword(null,"tap>?","tap>?",212454486),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false,new cljs.core.Keyword(null,"expound-opts","expound-opts",623087481),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"args?","args?",-1963723548),false], null),retspec85697,ret85698);
} else {
}

return ret85698;
});
cljs.spec.alpha.def_impl(new cljs.core.Symbol("com.fulcrologic.rad.attributes","entity-map","com.fulcrologic.rad.attributes/entity-map",632220915,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","fspec","cljs.spec.alpha/fspec",-1289128341,null),new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attributes","attributes",-74013604),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)),new cljs.core.Keyword(null,"ret","ret",-468222814),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","map-of","cljs.spec.alpha/map-of",153715093,null),new cljs.core.Symbol("cljs.core","qualified-keyword?","cljs.core/qualified-keyword?",-308091478,null),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932))),cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attributes","attributes",-74013604),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"attributes","attributes",-74013604)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attributes","attributes",-74013604),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","map-of","cljs.spec.alpha/map-of",153715093,null),new cljs.core.Symbol("cljs.core","qualified-keyword?","cljs.core/qualified-keyword?",-308091478,null),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)),cljs.spec.alpha.every_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","tuple","cljs.spec.alpha/tuple",-415901908,null),new cljs.core.Symbol(null,"qualified-keyword?","qualified-keyword?",375456001,null),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)),cljs.spec.alpha.tuple_impl.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","qualified-keyword?","cljs.core/qualified-keyword?",-308091478,null),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.qualified_keyword_QMARK_,new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)], null)),new cljs.core.PersistentArrayMap(null, 7, [new cljs.core.Keyword("cljs.spec.alpha","kfn","cljs.spec.alpha/kfn",672643897),(function (i__20422__auto__,v__20423__auto__){
return cljs.core.nth.cljs$core$IFn$_invoke$arity$2(v__20423__auto__,(0));
}),new cljs.core.Keyword(null,"into","into",-150836029),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword("cljs.spec.alpha","conform-all","cljs.spec.alpha/conform-all",45201917),true,new cljs.core.Keyword(null,"kind","kind",-717265803),cljs.core.map_QMARK_,new cljs.core.Keyword("cljs.spec.alpha","kind-form","cljs.spec.alpha/kind-form",-1047104697),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Keyword("cljs.spec.alpha","describe","cljs.spec.alpha/describe",1883026911),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","map-of","cljs.spec.alpha/map-of",153715093,null),new cljs.core.Symbol("cljs.core","qualified-keyword?","cljs.core/qualified-keyword?",-308091478,null),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)),new cljs.core.Keyword("cljs.spec.alpha","cpred","cljs.spec.alpha/cpred",-693471218),(function (G__85707){
return cljs.core.map_QMARK_(G__85707);
})], null),null),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","map-of","cljs.spec.alpha/map-of",153715093,null),new cljs.core.Symbol("cljs.core","qualified-keyword?","cljs.core/qualified-keyword?",-308091478,null),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)),null,null,null));


/**
 * Returns a map of qualified ID key -> the collection of attributes on entities that have that ID.
 * @type {function(*): !cljs.core.IMap}
 */
com.fulcrologic.rad.attributes.entity_map = (function com$fulcrologic$rad$attributes$entity_map(attributes){
var map__85708 = cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attributes","attributes",-74013604),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"attributes","attributes",-74013604)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"attributes","attributes",-74013604),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","map-of","cljs.spec.alpha/map-of",153715093,null),new cljs.core.Symbol("cljs.core","qualified-keyword?","cljs.core/qualified-keyword?",-308091478,null),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)),cljs.spec.alpha.every_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","tuple","cljs.spec.alpha/tuple",-415901908,null),new cljs.core.Symbol(null,"qualified-keyword?","qualified-keyword?",375456001,null),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)),cljs.spec.alpha.tuple_impl.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","qualified-keyword?","cljs.core/qualified-keyword?",-308091478,null),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.qualified_keyword_QMARK_,new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)], null)),new cljs.core.PersistentArrayMap(null, 7, [new cljs.core.Keyword("cljs.spec.alpha","kfn","cljs.spec.alpha/kfn",672643897),(function (i__20422__auto__,v__20423__auto__){
return cljs.core.nth.cljs$core$IFn$_invoke$arity$2(v__20423__auto__,(0));
}),new cljs.core.Keyword(null,"into","into",-150836029),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword("cljs.spec.alpha","conform-all","cljs.spec.alpha/conform-all",45201917),true,new cljs.core.Keyword(null,"kind","kind",-717265803),cljs.core.map_QMARK_,new cljs.core.Keyword("cljs.spec.alpha","kind-form","cljs.spec.alpha/kind-form",-1047104697),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Keyword("cljs.spec.alpha","describe","cljs.spec.alpha/describe",1883026911),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","map-of","cljs.spec.alpha/map-of",153715093,null),new cljs.core.Symbol("cljs.core","qualified-keyword?","cljs.core/qualified-keyword?",-308091478,null),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)),new cljs.core.Keyword("cljs.spec.alpha","cpred","cljs.spec.alpha/cpred",-693471218),(function (G__85709){
return cljs.core.map_QMARK_(G__85709);
})], null),null),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","map-of","cljs.spec.alpha/map-of",153715093,null),new cljs.core.Symbol("cljs.core","qualified-keyword?","cljs.core/qualified-keyword?",-308091478,null),new cljs.core.Keyword("com.fulcrologic.rad.attributes","attributes","com.fulcrologic.rad.attributes/attributes",826081932)),null,null,null);
var map__85708__$1 = cljs.core.__destructure_map(map__85708);
var retspec85704 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85708__$1,new cljs.core.Keyword(null,"ret","ret",-468222814));
var argspec85703 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__85708__$1,new cljs.core.Keyword(null,"args","args",1315556576));
if(cljs.core.truth_(argspec85703)){
com.fulcrologic.guardrails.core.run_check(new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/fulcrologic/rad/attributes.cljc:207 entity-map's",new cljs.core.Keyword(null,"tap>?","tap>?",212454486),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false,new cljs.core.Keyword(null,"expound-opts","expound-opts",623087481),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"args?","args?",-1963723548),true], null),argspec85703,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [attributes], null));
} else {
}

var f85706 = (function (attributes__$1){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (acc,attr){
var identities = com.fulcrologic.rad.attributes_options.identities.cljs$core$IFn$_invoke$arity$1(attr);
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (acc2,k){
return cljs.core.update.cljs$core$IFn$_invoke$arity$4(acc2,k,cljs.core.fnil.cljs$core$IFn$_invoke$arity$2(cljs.core.conj,cljs.core.PersistentVector.EMPTY),attr);
}),acc,identities);
}),cljs.core.PersistentArrayMap.EMPTY,attributes__$1);
});
var ret85705 = f85706(attributes);
if(cljs.core.truth_(retspec85704)){
com.fulcrologic.guardrails.core.run_check(new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/fulcrologic/rad/attributes.cljc:207 entity-map's",new cljs.core.Keyword(null,"tap>?","tap>?",212454486),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false,new cljs.core.Keyword(null,"expound-opts","expound-opts",623087481),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"args?","args?",-1963723548),false], null),retspec85704,ret85705);
} else {
}

return ret85705;
});
/**
 * Creates a Fulcro form-state validator function that can be used as a form validator for any form that contains
 * the given `attributes`.
 * 
 * A field is considered valid in this validator IF AND ONLY IF `attr/valid-value` returns true. See that
 * function's docstring for how that interacts with the `ao/valid?` option of attributes.
 * 
 * If `include-refs?` is true (default false) then references will be included in the validation.
 * 
 */
com.fulcrologic.rad.attributes.make_attribute_validator = (function com$fulcrologic$rad$attributes$make_attribute_validator(var_args){
var G__85711 = arguments.length;
switch (G__85711) {
case 1:
return com.fulcrologic.rad.attributes.make_attribute_validator.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return com.fulcrologic.rad.attributes.make_attribute_validator.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(com.fulcrologic.rad.attributes.make_attribute_validator.cljs$core$IFn$_invoke$arity$1 = (function (attributes){
return com.fulcrologic.rad.attributes.make_attribute_validator.cljs$core$IFn$_invoke$arity$2(attributes,false);
}));

(com.fulcrologic.rad.attributes.make_attribute_validator.cljs$core$IFn$_invoke$arity$2 = (function (attributes,include_refs_QMARK_){
var attribute_map = com.fulcrologic.rad.attributes.attribute_map(attributes);
return com.fulcrologic.fulcro.algorithms.form_state.make_validator.cljs$core$IFn$_invoke$arity$2((function (form,k){
return com.fulcrologic.rad.attributes.valid_value_QMARK_(cljs.core.get.cljs$core$IFn$_invoke$arity$2(attribute_map,k),cljs.core.get.cljs$core$IFn$_invoke$arity$2(form,k),form,k);
}),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"validate-edges?","validate-edges?",-981926542),include_refs_QMARK_], null));
}));

(com.fulcrologic.rad.attributes.make_attribute_validator.cljs$lang$maxFixedArity = 2);

/**
 * Build a (fn [env] env') that adds RAD attribute data to an env. If `base-wrapper` is supplied, then it will be called
 * as part of the evaluation, allowing you to build up a chain of environment middleware.
 * 
 * ```
 * (def build-env
 *   (-> (wrap-env all-attributes)
 *      ...))
 * 
 * ;; Pathom 2
 * (def env-plugin (p/env-wrap-plugin build-env))
 * 
 * ;; Pathom 3
 * (let [base-env (pci/register [...])
 *       env (build-env base-env)]
 *    (process env eql))
 * ```
 * 
 * similar to Ring middleware.
 * 
 */
com.fulcrologic.rad.attributes.wrap_env = (function com$fulcrologic$rad$attributes$wrap_env(var_args){
var G__85713 = arguments.length;
switch (G__85713) {
case 1:
return com.fulcrologic.rad.attributes.wrap_env.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return com.fulcrologic.rad.attributes.wrap_env.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(com.fulcrologic.rad.attributes.wrap_env.cljs$core$IFn$_invoke$arity$1 = (function (all_attributes){
return com.fulcrologic.rad.attributes.wrap_env.cljs$core$IFn$_invoke$arity$2(null,all_attributes);
}));

(com.fulcrologic.rad.attributes.wrap_env.cljs$core$IFn$_invoke$arity$2 = (function (base_wrapper,all_attributes){
var key__GT_attribute = com.fulcrologic.rad.attributes.attribute_map(all_attributes);
var id_keys = cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentHashSet.EMPTY,cljs.core.comp.cljs$core$IFn$_invoke$arity$2(cljs.core.filter.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword("com.fulcrologic.rad.attributes","identity?","com.fulcrologic.rad.attributes/identity?",-576130258)),cljs.core.map.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword("com.fulcrologic.rad.attributes","qualified-key","com.fulcrologic.rad.attributes/qualified-key",-649597949))),all_attributes);
return (function (env){
var G__85714 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(env,new cljs.core.Keyword("com.fulcrologic.rad.attributes","key->attribute","com.fulcrologic.rad.attributes/key->attribute",1058115384),key__GT_attribute,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword("com.fulcrologic.rad.attributes","id-keys","com.fulcrologic.rad.attributes/id-keys",432883187),id_keys], 0));
if(cljs.core.truth_(base_wrapper)){
return (base_wrapper.cljs$core$IFn$_invoke$arity$1 ? base_wrapper.cljs$core$IFn$_invoke$arity$1(G__85714) : base_wrapper.call(null,G__85714));
} else {
return G__85714;
}
});
}));

(com.fulcrologic.rad.attributes.wrap_env.cljs$lang$maxFixedArity = 2);

/**
 * Pathom 2 plugin. See also `wrap-env`.
 */
com.fulcrologic.rad.attributes.pathom_plugin = (function com$fulcrologic$rad$attributes$pathom_plugin(all_attributes){
var augment = com.fulcrologic.rad.attributes.wrap_env.cljs$core$IFn$_invoke$arity$1(all_attributes);
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793),(function com$fulcrologic$rad$attributes$pathom_plugin_$_env_wrap_wrap_parser(parser){
return (function com$fulcrologic$rad$attributes$pathom_plugin_$_env_wrap_wrap_parser_$_env_wrap_wrap_internal(env,tx){
var G__85715 = (augment.cljs$core$IFn$_invoke$arity$1 ? augment.cljs$core$IFn$_invoke$arity$1(env) : augment.call(null,env));
var G__85716 = tx;
return (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(G__85715,G__85716) : parser.call(null,G__85715,G__85716));
});
})], null);
});

//# sourceMappingURL=com.fulcrologic.rad.attributes.js.map
