(ns com.app.invoice-specs
  (:require
    [malli.core :as m]
    [malli.generator :as mg]
    [malli.error :as me]
    [malli.util :as mu]))

(def items
  [:map
   [:item/id :int]
   [:item/name :string]
   [:item/price :int]
   [:item/quantity :int]])

(def Customer
  [:map
   [:id :int]
   [:name :string]
   [:email {:optional true} :string]
   [:role [:enum "User" "Admin"]]])

(def invoice-schema
  (m/schema
    [:and
     [:map
      [:invoice/id :int]
      [:invoice/customer #'Customer]
      [:invoice/items #'items]
      [:invoice/total int?]
      [:invoice/date inst?]]]))

(mg/generate invoice-schema)

(m/explain
  invoice-schema
  {:invoice/id       2
   :invoice/total    40
   :invoice/date     #inst"2024-02-14T21:27:00.034-00:00"
   :invoice/customer {:id   1
                      :name 4545
                      :role "User"}
   :invoice/items    {:item/name  "Hammer"
                      :item/price 20
                      :item/id 2
                      :item/quantity 2}})

(def invoice1-customer-wrong-role
  {:invoice/id       3
   :invoice/total    30
   :invoice/date     #inst"2024-02-14T21:27:00.034-00:00"
   :invoice/customer {:id   3
                      :name "Lucy"
                      :role "non"}
   :invoice/items    {:item/name "Doll"
                      :item/price 15
                      :item/id 5
                      :item/quantity 2}})

(me/humanize (m/explain invoice-schema invoice1-customer-wrong-role))

(m/validate invoice-schema {:invoice/id       1
                            :invoice/total    10
                            :invoice/date     #inst"2024-02-20T21:27:00.034-00:00"
                            :invoice/customer {:id   2
                                               :name "Bob"
                                               :role "User"}
                            :invoice/items    {:item/name  "Cookies"
                                               :item/price 5
                                               :item/id 1
                                               :item/quantity 2}})