(ns com.app.ui.inventory
  (:require
    [com.app.model.item :as item]
    [com.app.model.statemachine :refer [ItemFormSM]]
    [com.fulcrologic.rad.report :as report]
    [com.fulcrologic.rad.report-options :as ro]
    [com.fulcrologic.fulcro.components :as comp :refer [defsc]]
    [com.fulcrologic.rad.control :as control]
    [com.fulcrologic.rad.form :as form]
    [clojure.string :as str]
    [com.fulcrologic.rad.semantic-ui-options :as suo]
    [com.app.ui.toggle :refer [buttonRowRenderer]]
    [com.fulcrologic.fulcro.ui-state-machines :as uism]
    [com.fulcrologic.fulcro.mutations :as m :refer [defmutation]]
    [com.fulcrologic.fulcro.dom :as dom :refer [div i p tr button label input]]
    [com.fulcrologic.semantic-ui.modules.modal.ui-modal :refer [ui-modal]]
    [com.fulcrologic.semantic-ui.modules.modal.ui-modal-header :refer [ui-modal-header]]
    [com.fulcrologic.semantic-ui.modules.modal.ui-modal-content :refer [ui-modal-content]]
    [com.fulcrologic.semantic-ui.modules.modal.ui-modal-actions :refer [ui-modal-actions]]
    [com.fulcrologic.rad.form-options :as fo]))

(def categories {:all-categories
                 {:category/services "Supplies & Services"
                  :category/tool     "Tool"
                  :category/food     "Food"
                  :category/item     "Item"
                  :category/other    "Other"}})

(def new-item-button
  {:type   :button
   :local? true
   :label  "New Item"
   :action (fn [_ {::form/keys [props computed-props form-instance master-form] :as env}]
             (js/console.log form-instance)
             (form/save! env))})


(form/defsc-form ItemForm [this props]
                 {fo/id            item/id
                  fo/machine ItemFormSM
                  fo/attributes    [item/item-name
                                    item/category
                                    item/description
                                    item/in-stock
                                    item/price]
                  fo/cancel-route :back
                  suo/rendering-options  {suo/report-row-button-renderer buttonRowRenderer}
                  fo/route-prefix  "item"
                  fo/title         "Edit Item"
                  fo/triggers     {:confirmed (fn [uism-env ident]
                                                #?(:cljs (js/alert "CONFIRMED"))
                                                uism-env)}
                  }
                 (div
                   (form/render-layout this props)

                   (ui-modal {:open (= (uism/get-active-state this (comp/get-ident this)) :state/saving-opt) :dimmer true}
                             (ui-modal-content {}
                                               (div :.ui.segment
                                                    (p "Are you sure you want to save this?"))
                                               (ui-modal-actions {}
                                                                 (button :.positive.ui.button
                                                                         {:onClick (fn [] (uism/trigger! this (comp/get-ident this) :event/save))}
                                                                         "Yes")
                                                                 (button :.ui.button
                                                                         {:onClick (fn [] (uism/trigger! this (comp/get-ident this) :event/cancel))}
                                                                         "Cancel"))))))

(report/defsc-report InventoryReport [this props]
                     {ro/title               "Inventory Report"
                      ro/source-attribute    :item/all-items
                      ro/row-pk              item/id
                      ro/columns             [item/item-name item/category item/price item/in-stock]
                      ro/initial-sort-params {:sort-by          :item/name
                                              :ascending?       false
                                              :sortable-columns #{:item/name}}
                      ro/row-visible?        (fn [{::keys [filter-name]} {:item/keys [name]}]
                                               (let [nm     (some-> name (str/lower-case))
                                                     target (some-> filter-name (str/trim) (str/lower-case))]
                                                 (or
                                                   (nil? target)
                                                   (empty? target)
                                                   (and nm (str/includes? nm target)))))
                      ro/controls            {::new-item   {:type   :button
                                                            :local? true
                                                            :label  "New Item"
                                                            :action (fn [this _] (form/create! this ItemForm))}
                                              ::search!       {:type   :button
                                                               :local? true
                                                               :label  "Filter"
                                                               :class  "ui basic compact mini red button"
                                                               :action (fn [this _] (report/filter-rows! this))}
                                              ::filter-name   {:type        :string
                                                               :local?      true
                                                               :placeholder "Type a partial name and press enter."
                                                               :onChange    (fn [this _] (report/filter-rows! this))}}


                      ro/control-layout      {:action-buttons [::new-item]
                                              :inputs         [[::filter-name ::search! :_]]}

                      ro/row-actions         [{:label  "Delete"
                                               :action (fn [this {:item/keys [id] :as row}] (form/delete! this :item/id id))}
                                              {:label  "Edit"
                                               :action (fn [this {:item/keys [id] :as row}] (form/edit! this ItemForm id))}]

                      ro/form-links          {item/item-name ItemForm}

                      ro/links               {:category/label (fn [this {:category/keys [label]}]
                                                                (control/set-parameter! this ::category label)
                                                                (report/filter-rows! this))}

                      ro/run-on-mount?       true
                      ro/route               "item-inventory-report"})