(ns com.app.ui
  (:require
    #?@(:cljs [[com.fulcrologic.semantic-ui.modules.dropdown.ui-dropdown :refer [ui-dropdown]]
               [com.fulcrologic.semantic-ui.modules.dropdown.ui-dropdown-menu :refer [ui-dropdown-menu]]
               [com.fulcrologic.semantic-ui.modules.dropdown.ui-dropdown-item :refer [ui-dropdown-item]]])
    #?(:clj  [com.fulcrologic.fulcro.dom-server :as dom :refer [div label input]]
       :cljs [com.fulcrologic.fulcro.dom :as dom :refer [div label input]])
    [com.app.ui.login-dialog :refer [LoginForm]]
    [com.app.ui.inventory :refer [ItemForm InventoryReport]]
    [com.app.ui.account-forms :refer [AccountForm BriefAccountForm AccountList]]
    [com.app.ui.file-forms :refer [FileForm]]
    [com.app.ui.invoice-forms :refer [LineItemForm InvoiceForm InvoiceList AccountInvoices]]
    [com.fulcrologic.fulcro.application :as app]
    [com.fulcrologic.fulcro.components :as comp :refer [defsc]]
    [com.fulcrologic.fulcro.dom.html-entities :as ent]
    [com.fulcrologic.fulcro.routing.dynamic-routing :refer [defrouter]]
    [com.fulcrologic.rad.authorization :as auth]
    [com.fulcrologic.rad.form :as form]
    [com.fulcrologic.rad.ids :refer [new-uuid]]
    [com.fulcrologic.rad.routing :as rroute]))

(defsc LandingPage [this {:ui/keys [open? selected-account edit-id] :as props}]
  {:query         [:ui/open? :ui/selected-account :ui/edit-id]
   :ident         (fn [] [:component/id ::LandingPage])
   :initial-state {}
   :route-segment ["landing-page"]
   :use-hooks?    true}
  (dom/div "Welcome"))

(defrouter MainRouter [this {:keys [current-state route-factory route-props]}]
  {:always-render-body? true
   :router-targets      [LandingPage ItemForm AccountInvoices InventoryReport AccountForm BriefAccountForm AccountList FileForm LineItemForm InvoiceForm InvoiceList]}
  (dom/div
    (dom/div :.ui.loader {:classes [(when-not (= :routed current-state) "active")]})
    (when route-factory
      (route-factory route-props))))

(def ui-main-router (comp/factory MainRouter))

(auth/defauthenticator Authenticator {:local LoginForm})

(def ui-authenticator (comp/factory Authenticator))

(defsc Root [this {::auth/keys [authorization]
                   ::app/keys  [active-remotes]
                   :keys       [authenticator router]}]
  {:query         [{:authenticator (comp/get-query Authenticator)}
                   {:router (comp/get-query MainRouter)}
                   ::app/active-remotes
                   ::auth/authorization]
   :initial-state {:router        {}
                   :authenticator {}}}
  (let [logged-in? (= :success (some-> authorization :local ::auth/status))
        busy?      (seq active-remotes)
        username   (some-> authorization :local :account/name)]
    (dom/div
      (div :.ui.top.menu
           (div :.ui.item "Demo")
           (when logged-in?
             #?(:cljs
                (comp/fragment
                  (ui-dropdown {:className "item" :text "Inventory"}
                               (ui-dropdown-menu {}
                                                 (ui-dropdown-item {:onClick (fn [] (rroute/route-to! this InventoryReport {}))} "View All")
                                                 (ui-dropdown-item {:onClick (fn [] (form/create! this ItemForm))} "New")))
                  (ui-dropdown {:className "item" :text "Invoices"}
                               (ui-dropdown-menu {}
                                                 (ui-dropdown-item {:onClick (fn [] (rroute/route-to! this InvoiceList {}))} "View All")
                                                 (ui-dropdown-item {:onClick (fn [] (form/create! this InvoiceForm))} "New")))
                  (ui-dropdown {:className "item" :text "Accounts"}
                               (ui-dropdown-menu {}
                                                 (ui-dropdown-item {:onClick (fn [] (rroute/route-to! this AccountList {}))} "View All")
                                                 (ui-dropdown-item {:onClick (fn [] (form/create! this AccountForm))} "New"))))))
           (div :.right.menu
                (div :.item
                     (div :.ui.tiny.loader {:classes [(when busy? "active")]})
                     ent/nbsp ent/nbsp ent/nbsp ent/nbsp)
                (if logged-in?
                  (comp/fragment
                    (div :.ui.item
                         (str "Logged in as " username))
                    (div :.ui.item
                         (dom/button :.ui.button {:onClick (fn []
                                                             (rroute/route-to! this LandingPage {})
                                                             (auth/logout! this :local))}
                                     "Logout")))
                  (div :.ui.item
                       (dom/button :.ui.primary.button {:onClick #(auth/authenticate! this :local nil)}
                                   "Login")))))
      (div :.ui.container.segment
           (ui-authenticator authenticator)
           (ui-main-router router)))))

(def ui-root (comp/factory Root))

